﻿using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace Do.Scripts.Tools.Tween
{
    public class MyTween : MonoBehaviour
    {
        public static MyTween Instance { get; private set; }

        [SerializeField] private List<MyTweenStep> _myTweenSteps = new List<MyTweenStep>();
        private float _tweenFloat;

        private void Awake()
        {
            if (Instance != null)
                return;
            Instance = this;
        }

        public void MyTween_Float(float Duration, Callback Complete)
        {
            GetTweenStep().My_Tween_Float(Duration, 0, Complete);
        }

        public void MyTween_Float(float Duration, float Delay, Callback Complete)
        {
            GetTweenStep().My_Tween_Float(Duration, Delay, Complete);
        }

        public DG.Tweening.Tween DoTween_Float(float Duration, float Delay = 0, Callback Complete = null)
        {
            _tweenFloat = 0;
            var tween = DOTween.To(() => _tweenFloat, x => _tweenFloat = x, 1, Duration).SetDelay(Delay).OnComplete(delegate
                {
                    Complete?.Invoke();
                });
            return tween;
        }

        private MyTweenStep GetTweenStep()
        {
            foreach (var myTweenStep in _myTweenSteps)
            {
                if (!myTweenStep.IsTween)
                    return myTweenStep;
            }
            var newTween = Instantiate(_myTweenSteps[0], transform);
            _myTweenSteps.Add(newTween);
            return newTween;
        }
    }
}
