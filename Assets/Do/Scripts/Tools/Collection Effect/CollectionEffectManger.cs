﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Do.Scripts.Tools.Collection_Effect
{
    public class CollectionEffectManger : MonoBehaviour
    {
        public static CollectionEffectManger Instance { get; private set; }
        
        public float speed;
        [SerializeField] private float range = 1f;
        [SerializeField] private CollectionEffect prefabCollectionEffect;
        [SerializeField] private List<CollectionEffect> listEffectCoin;

        private CollectionEffect _collectionEffect;
        private readonly List<Vector2> _between = new List<Vector2>();

        private void Awake()
        {
            if (Instance != null)
                return;
            Instance = this;
            // DontDestroyOnLoad(gameObject);
        }

        public void PlayEffectCoin(Vector2 origin, Vector2 target, Callback callbackStep)
        {
            _collectionEffect = GetEffect();
            _collectionEffect.transform.position = origin;
            GetBetween(origin);
            _collectionEffect.ShowEffectCoin(origin, target, _between, callbackStep);
        }

        private CollectionEffect GetEffect()
        {
            foreach (var effectCoin in listEffectCoin)
            {
                if (!effectCoin.IsActive)
                    return effectCoin;
            }

            var newEffectCoin = Instantiate(prefabCollectionEffect, transform);
            newEffectCoin.gameObject.name = "Effect " + newEffectCoin.name;
            newEffectCoin.transform.SetSiblingIndex(listEffectCoin.Count + 1);
            listEffectCoin.Add(newEffectCoin);
            return newEffectCoin;
        }

        private void GetBetween(Vector2 origin)
        {
            _between.Clear();
            var minX = origin.x - range / 2;
            var maxX = origin.x + range / 2;
            var minY = origin.y - range;
            var maxY = origin.y - range / 2;
            var count = _collectionEffect.transform.childCount;
            for (var i = 0; i < count; i++)
            {
                _between.Add(new Vector2(Random.Range(minX, maxX), Random.Range(minY, maxY)));
            }
        }
    }
}
