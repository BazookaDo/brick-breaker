﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Do.Scripts.Tools.Audio;
using Do.Scripts.Tools.Other;
using UnityEngine;

namespace Do.Scripts.Tools.Collection_Effect
{
    public class CollectionEffect : MonoBehaviour
    {
        public new string name;
        [Range(0.02f, 0.2f)] [SerializeField] private float duration = 0.1f;
        [SerializeField] private CollectionEffectManger collectionEffectManger;
        [SerializeField] private List<Animator> animators;

        private List<Vector2> _between = new List<Vector2>();
        private float _range;
        private Vector2 _target;
        private IEnumerator _routine;
        private readonly int _play = Animator.StringToHash("Play");
        private Callback _callback;
        private WaitForSeconds _waitForSeconds;
        public bool IsActive { get; private set; }

        private void Awake()
        {
            _waitForSeconds = Yield.GetTime(duration);
        }

        public void ShowEffectCoin(Vector2 origin, Vector2 target, List<Vector2> between, Callback callback)
        {
            transform.position = origin;
            if (_between.Count <= 0)
            {
                _range = transform.childCount;
                _target = target;
            }
            _between = between;
            _callback = callback;
            IsActive = true;
            gameObject.SetActive(true);
            _routine = InstanceAllObject();
            StartCoroutine(_routine);
        }

        private IEnumerator InstanceAllObject()
        {
            var current = 0;
            while (current < _range)
            {
                AudioManager.Instance.Play(SoundType.CoinCollect);
                var sort = _range - current;
                var child = transform.GetChild(current);
                child.gameObject.SetActive(true);
                child.DOMove(_between[current], collectionEffectManger.speed / 2).SetEase(Ease.OutSine).OnComplete(delegate
                {
                    child.DOMove(_target, collectionEffectManger.speed).SetEase(Ease.InSine).OnComplete(delegate
                    {
                        AudioManager.Instance.Play(SoundType.CoinCollect);
                        _callback?.Invoke();
                        if (sort <= 1)
                            DisableAllObject();
                    });
                });
                if (animators.Count > 0)
                {
                    animators[current].SetTrigger(_play);
                    animators[current].speed = 1 / collectionEffectManger.speed;
                }
                current++;
                if (current < _range)
                    yield return _waitForSeconds;
            }
        }

        private void DisableAllObject()
        {
            gameObject.SetActive(false);
            for (var i = 0; i < _range; i++)
            {
                var child = transform.GetChild(i);
                child.localPosition = Vector2.zero;
                child.gameObject.SetActive(false);
            }
            IsActive = false;
            _routine = null;
        }
    }
}
