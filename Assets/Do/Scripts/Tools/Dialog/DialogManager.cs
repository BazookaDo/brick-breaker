﻿using System.Collections.Generic;
using Dialog;
using Do.Scripts.Tools.Audio;
using Do.Scripts.Tools.Language;
using Home.Gift;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Do.Scripts.Tools.Dialog
{
    public class DialogManager : MonoBehaviour
    {
        public static DialogManager Instance { private set; get; }

        [Header("Setting")] 
        [SerializeField] private DialogSetting settingDialog;
        [SerializeField] private Button btnCloseSetting;
        private Callback _settingCallback;

        [Header("Language")]
        [SerializeField] private DialogLanguage languageDialog;
        [SerializeField] private Button btnConfirmLanguage;
        private Callback _languageCallback;
        
        [Header("Reward")] 
        [SerializeField] private DialogReward rewardDialog;
        [SerializeField] private Button btnRewardClaim;
        private Callback _reward;

        [Header("Play")] 
        [SerializeField] private DialogPlay playDialog;
        [SerializeField] private DialogPreviewMap previewMapDialog;
        [SerializeField] private RectTransform previewRoot;
        [SerializeField] private Button btnPlay, btnPlayAds, btnClosePlay;

        [Header("Base")]
        [SerializeField] private GameObject dialogCamera;
        [SerializeField] private GameObject canvas;
        [SerializeField] private GameObject messageDialog, yesNoDialog;
        [SerializeField] private TextMeshProUGUI messageText, messageButtonText, yesNoText, yesNoTitleText;
        [SerializeField] private Button btnAccept, btnIgnore, btnYes, btnNo;

        private void Awake()
        {
            if (Instance != null)
                return;
            Instance = this;
        }

        #region Setting

        public void Setting_Dialog(Callback callback = null)
        {
            dialogCamera.SetActive(true);
            canvas.SetActive(true);
            settingDialog.gameObject.SetActive(true);
            btnCloseSetting.onClick.AddListener(() =>
            {
                AudioManager.Instance.Play(SoundType.Click);
                DisableSettingDialog();
            });
            _settingCallback = callback;
        }

        private void DisableSettingDialog()
        {
            dialogCamera.SetActive(false);
            canvas.SetActive(false);
            settingDialog.gameObject.SetActive(false);
            _settingCallback?.Invoke();
            btnCloseSetting.onClick.RemoveAllListeners();
        }

        #endregion
        
        /// <summary>
        /// /////////
        /// </summary>

        #region Language

        public void Language_Dialog(Callback callback = null)
        {
            languageDialog.gameObject.SetActive(true);
            _languageCallback = callback;
            InitLanguageOnEnable();
            btnConfirmLanguage.onClick.AddListener(() =>
            {
                AudioManager.Instance.Play(SoundType.Click);
                DisableLanguageDialog();
            });
        }

        public void Add_Event_OnChange_Language(Callback callback)
        {
            LanguageManager.Instance.Add_Event_OnChange(callback);
        }

        private void DisableLanguageDialog()
        {
            languageDialog.gameObject.SetActive(false);
            _languageCallback?.Invoke();
            btnConfirmLanguage.onClick.RemoveAllListeners();
            LanguageManager.Instance.Clear_Event_OnChange();
        }

        private void InitLanguageOnEnable()
        {
            if (LanguageManager.Instance.Language == Language.Language.Eng)
                languageDialog.EngOn();
            else
                languageDialog.VieOn();
        }

        #endregion
        
        /// <summary>
        /// /////////
        /// </summary>

        #region Reward

        public void Reward_Dialog(Gift gift, Callback callback = null)
        {
            AudioManager.Instance.Play(SoundType.Reward);
            dialogCamera.SetActive(true);
            canvas.SetActive(true);
            rewardDialog.Active(gift);
            rewardDialog.gameObject.SetActive(true);
            _reward = callback;
            btnRewardClaim.onClick.AddListener(() =>
            {
                AudioManager.Instance.Play(SoundType.Click);
                Disable_Reward_Dialog();
            });
        }
        public void Reward_Dialog(List<Gift> gifts, Callback callback = null)
        {
            AudioManager.Instance.Play(SoundType.Reward);
            dialogCamera.SetActive(true);
            canvas.SetActive(true);
            rewardDialog.Active(gifts);
            rewardDialog.gameObject.SetActive(true);
            _reward = callback;
            btnRewardClaim.onClick.AddListener(() =>
            {
                AudioManager.Instance.Play(SoundType.Click);
                Disable_Reward_Dialog();
            });
        }

        private void Disable_Reward_Dialog()
        {
            dialogCamera.SetActive(false);
            canvas.SetActive(false);
            rewardDialog.gameObject.SetActive(false);
            btnRewardClaim.onClick.RemoveAllListeners();
            _reward?.Invoke();
            _reward = null;
        }

        #endregion

        /// <summary>
        /// /////////
        /// </summary>

        #region Play

        public void Play_Dialog(string title, Callback play, Callback playAds, Callback close)
        {
            dialogCamera.SetActive(true);
            canvas.SetActive(true);
            playDialog.Active(title);
            playDialog.gameObject.SetActive(true);
            previewMapDialog.Initialized();
            previewMapDialog.Show(previewRoot.anchoredPosition);
            btnPlay.onClick.AddListener(delegate
            {
                AudioManager.Instance.Play(SoundType.Click);
                Disable_Play_Dialog();
                play?.Invoke();
            });
            btnPlayAds.onClick.AddListener(delegate
            {
                AudioManager.Instance.Play(SoundType.Click);
                Disable_Play_Dialog();
                playAds?.Invoke();
            });
            btnClosePlay.onClick.AddListener(() =>
            {
                AudioManager.Instance.Play(SoundType.Click);
                Disable_Play_Dialog();
            });
        }

        private void Disable_Play_Dialog()
        {
            dialogCamera.SetActive(false);
            canvas.SetActive(false);
            playDialog.gameObject.SetActive(false);
            previewMapDialog.gameObject.SetActive(false);
            btnPlay.onClick.RemoveAllListeners();
            btnPlayAds.onClick.RemoveAllListeners();
            btnClosePlay.onClick.RemoveAllListeners();
        }

        public void Preview_Map(Vector2 anchoredPosition)
        {
            dialogCamera.SetActive(true);
            canvas.SetActive(true);
            previewMapDialog.Show(anchoredPosition);
        }

        public void Disable_Preview_Map()
        {
            dialogCamera.SetActive(false);
            canvas.SetActive(false);
            previewMapDialog.gameObject.SetActive(false);
        }

        #endregion
        
        /// <summary>
        /// /////////
        /// </summary>
        
        #region Base

        public void Message_Dialog(string message, string buttonMessage = "", Callback callbackAccept = null, Callback callbackIgnore = null)
        {
            dialogCamera.SetActive(true);
            canvas.SetActive(true);
            messageDialog.SetActive(true);
            messageText.text = message;
            messageButtonText.text = buttonMessage != "" ? buttonMessage : "OK";
            btnAccept.onClick.AddListener(() =>
            {
                AudioManager.Instance.Play(SoundType.Click);
                callbackAccept?.Invoke();
                DisableCanvas();
            });
            btnIgnore.onClick.AddListener(() =>
            {
                AudioManager.Instance.Play(SoundType.Click);
                callbackIgnore?.Invoke();
                DisableCanvas();
            });
        }

        public void Yes_No_Dialog(string message, string titleMessage, Callback callbackYes = null,
            Callback callbackNo = null, string buttonYesMessage = "YES", string buttonNoMessage = "NO")
        {
            dialogCamera.SetActive(true);
            canvas.SetActive(true);
            yesNoDialog.SetActive(true);
            yesNoText.text = message;
            yesNoTitleText.text = titleMessage;
            btnYes.onClick.AddListener(() =>
            {
                AudioManager.Instance.Play(SoundType.Click);
                callbackYes?.Invoke();
                DisableCanvas();
            });
            btnNo.onClick.AddListener(() =>
            {
                AudioManager.Instance.Play(SoundType.Click);
                callbackNo?.Invoke();
                DisableCanvas();
            });
        }

        private void DisableCanvas()
        {
            if (messageDialog.activeSelf)
            {
                messageDialog.SetActive(false);
                btnAccept.onClick.RemoveAllListeners();
                btnIgnore.onClick.RemoveAllListeners();
            }
            if (yesNoDialog.activeSelf)
            {
                yesNoDialog.SetActive(false);
                btnYes.onClick.RemoveAllListeners();
                btnNo.onClick.RemoveAllListeners();
            }
            dialogCamera.SetActive(false);
            canvas.SetActive(false);
        }

        #endregion
    }
}
