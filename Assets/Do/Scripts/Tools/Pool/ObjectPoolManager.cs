﻿using System.Collections.Generic;
using UnityEngine;

namespace Do.Scripts.Tools.Pool
{
    public class ObjectPoolManager : MonoBehaviour
    {
        private readonly List<ObjectPool> _objectPools = new List<ObjectPool>();
        
        [Header("Controller")]
        [SerializeField] private int poolLenght = 10;
        private readonly List<GameObject> _listPool = new List<GameObject>();

        [HideInInspector] 
        public ObjectPool ballPools;

        [HideInInspector] 
        public ObjectPool brickSquarePools, brickTrianglePools, brickStonePools;

        [HideInInspector] public ObjectPool itemAcousticWavesPools, itemBarDirectionPools, itemGunDirectionPools,
            itemIncreasePools, itemLaze2Pools, itemLaze4Pools, itemUpSideAutoPools, itemWormHoldPools;

        [HideInInspector] public ObjectPool boosterBombPools;

        [HideInInspector] public ObjectPool effectBrickBreakPools,
            effectLightingPools,
            effectLazePools,
            effectCannonPools,
            effectRocketPools,
            effectFreezePools,
            effectFreeze2Pools,
            effectFirePools,
            effectPoisonPools,
            effectMiniBombPools,
            effectNuclearBombPools,
            effectAcousticWavesPools;

        [HideInInspector] public ObjectPool trialRocketPools, trialFreezePools, trialFirePools, trialPoisonPools;

        [HideInInspector] public ObjectPool targetRocketPools;

        [Header("Object")]
        [SerializeField] 
        private GameObject ballPrefab;

        [SerializeField]
        private GameObject brickSquarePrefab, brickTrianglePrefab, brickStonePrefab;

        [SerializeField] private GameObject itemAcousticWavePrefab, itemBarDirectionPrefab, itemGunDirectionPrefab,
            itemIncreasePrefab, itemLaze2Prefab, itemLaze4Prefab, itemUpSideAutoPrefab, itemWormHoldPrefab;

        [SerializeField] private GameObject boosterBombPrefab;

        [SerializeField] private GameObject effectBrickBreakPrefab,
            effectLightingPrefab,
            effectLazePrefab,
            effectCannonPrefab,
            effectRocketPrefab,
            effectFreezePrefab,
            effectFreeze2Prefab,
            effectFirePrefab,
            effectPoisonPrefab,
            effectMiniBombPrefab,
            effectNuclearBombPrefab,
            effectAcousticWavesPrefab;

        [SerializeField] private GameObject trialRocketPrefab, trialFreezePrefab, trialFirePrefab, trialPoisonPrefab;

        [SerializeField] private GameObject targetRocketPrefab;
        
        public static ObjectPoolManager Instance { private set; get; }

        private void Initialized()
        {
            ballPools = InstancePool(ballPrefab, 50);
            
            brickSquarePools = InstancePool(brickSquarePrefab, 50);
            brickTrianglePools = InstancePool(brickTrianglePrefab, 20);
            brickStonePools = InstancePool(brickStonePrefab, 50);
            
            itemAcousticWavesPools = InstancePool(itemAcousticWavePrefab, 5);
            itemBarDirectionPools = InstancePool(itemBarDirectionPrefab, 5);
            itemGunDirectionPools = InstancePool(itemGunDirectionPrefab, 5);
            itemIncreasePools = InstancePool(itemIncreasePrefab, 5);
            itemLaze2Pools = InstancePool(itemLaze2Prefab, 5);
            itemLaze4Pools = InstancePool(itemLaze4Prefab, 5);
            itemUpSideAutoPools = InstancePool(itemUpSideAutoPrefab, 5);
            itemWormHoldPools = InstancePool(itemWormHoldPrefab, 5);

            boosterBombPools = InstancePool(boosterBombPrefab, 1);
            
            effectBrickBreakPools = InstancePool(effectBrickBreakPrefab, 20);
            effectLightingPools = InstancePool(effectLightingPrefab, 4);
            effectLazePools = InstancePool(effectLazePrefab, 4);
            effectCannonPools = InstancePool(effectCannonPrefab, 4);
            effectRocketPools = InstancePool(effectRocketPrefab, 5);
            effectFreezePools = InstancePool(effectFreezePrefab, 5);
            effectFreeze2Pools = InstancePool(effectFreeze2Prefab, 4);
            effectFirePools = InstancePool(effectFirePrefab, 5);
            effectPoisonPools = InstancePool(effectPoisonPrefab, 6);
            effectMiniBombPools = InstancePool(effectMiniBombPrefab, 3);
            effectNuclearBombPools = InstancePool(effectNuclearBombPrefab, 1);
            effectAcousticWavesPools = InstancePool(effectAcousticWavesPrefab, 3);
            
            trialRocketPools = InstancePool(trialRocketPrefab, 4);
            trialFreezePools = InstancePool(trialFreezePrefab, 4);
            trialFirePools = InstancePool(trialFirePrefab, 4);
            trialPoisonPools = InstancePool(trialPoisonPrefab, 5);
            
            targetRocketPools = InstancePool(targetRocketPrefab, 4);
        }

        private void Awake()
        {
            if (Instance != null)
                return;
            Instance = this;
            Initialized();
        }

        public void DisableAllObject()
        {
            foreach (var objectPool in _objectPools)
            {
                if (objectPool != null)
                    objectPool.DisableAllObject();
            }
            if(_listPool.Count <= 0)
                return;
            foreach (var pool in _listPool)
            {
                if (pool != null)
                    pool.GetComponent<BasePoolManager>().DisableAllObject();
            }
        }

    
        private ObjectPool InstancePool(GameObject prefab, int length)
        {
            if (prefab == null) 
                return null;
            var go = new GameObject(prefab.name);
            var objectPool = go.AddComponent<ObjectPool>();
            objectPool.PooledObject = prefab;
            go.transform.parent = gameObject.transform;
            objectPool.Initialize(length);
            _objectPools.Add(objectPool);
            return objectPool;
        }

        public void AddPool(GameObject addPool)
        {
            if (_listPool.Count > 0)
            {
                var isExist = false;
                foreach (var pool in _listPool)
                {
                    if (addPool == pool)
                        isExist = true;
                }
                if (isExist)
                {
                    _listPool.Remove(addPool);
                }
                else
                {
                    if (_listPool.Count >= poolLenght)
                    {
                        var old = _listPool[0];
                        _listPool.Remove(old);
                        Destroy(old);
                    }
                }
            }
            _listPool.Add(addPool);
        }

        public void RemovePool(GameObject removePool)
        {
            _listPool.Remove(removePool);
            Destroy(removePool);
        }
    }
}
