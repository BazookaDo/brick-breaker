﻿using System.Collections.Generic;
using UnityEngine;

namespace Do.Scripts.Tools.Pool
{
    public class BasePoolManager : MonoBehaviour
    {
        public float timeDelay = 600;
        private readonly List<ObjectPool> _objectPools = new List<ObjectPool>();
    
        public void LoadData()
        {
            ObjectPoolManager.Instance.AddPool(gameObject);
            if(IsInvoking(nameof(DestroyGameObject)))
                CancelInvoke(nameof(DestroyGameObject));
            Invoke(nameof(DestroyGameObject), timeDelay);
        }

        private void DestroyGameObject()
        {
            ObjectPoolManager.Instance.RemovePool(gameObject);
        }

        public void DisableAllObject()
        {
            foreach (var objectPool in _objectPools)
            {
                objectPool.DisableAllObject();
            }
        }

        protected ObjectPool InstancePool(GameObject prefab, int length)
        {
            if (prefab == null)
                return null;
            var go = new GameObject(prefab.name);
            var objectPool = go.AddComponent<ObjectPool>();
            objectPool.PooledObject = prefab;
            go.transform.parent = gameObject.transform;
            objectPool.Initialize(length);
            _objectPools.Add(objectPool);
            return objectPool;
        }
    }
}
