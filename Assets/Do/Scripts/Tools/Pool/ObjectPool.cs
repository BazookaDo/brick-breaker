﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Do.Scripts.Tools.Pool
{
    public class ObjectPool : MonoBehaviour
    {
        public Transform Parent;
        public GameObject PooledObject;
        private List<GameObject> PooledObjects;
        private int _poolLength = 1;

        private Type[] componentsToAdd;
        public void Initialize(int length)
        {
            if (length > 0)
                _poolLength = length;
            PooledObjects = new List<GameObject>();
            for (var i = 0; i < _poolLength; i++)
            {
                CreateObjectInPool();
            }
        }

        public void DisableAllObject()
        {
            foreach (var go in PooledObjects)
            {
                if (go.activeSelf)
                    go.SetActive(false);
            }
        }

        public void Initialize(Type[] components, int length)
        {
            this.componentsToAdd = components;
            Initialize(length);
        }

        public GameObject GetPooledObject()
        {
            foreach (var go in PooledObjects)
            {
                if (!go.activeSelf)
                {
                    return go;
                }
            }
            var indexToReturn = PooledObjects.Count;
            CreateObjectInPool();
            return PooledObjects[indexToReturn];
        }

        public bool CheckPoolObjectActive()
        {
            foreach (var go in PooledObjects)
            {
                if (go.activeSelf) 
                    return true;
            }
            return false;
        }

        private void CreateObjectInPool()
        {
            GameObject go;
            if (PooledObject == null)
                go = new GameObject(this.name + " PooledObject");
            else
            {
                go = Instantiate(PooledObject) as GameObject;
            }
            go.SetActive(false);
            PooledObjects.Add(go);
            
            if (componentsToAdd != null)
                foreach (var itemType in componentsToAdd)
                {
                    go.AddComponent(itemType);
                }
            go.transform.parent = Parent != null ? Parent : transform;
        }
    }
}