﻿using InGame;
using Loading;
using Other;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Do.Scripts.Tools.Control
{
    public class GameConfig : MonoBehaviour
    {
        public static GameConfig Instance { get; private set; }

        private static Vector2 screenSize = Vector2.zero;
        private static float ratioScaleScreen = 0f;

        public static Vector2 ScreenSize
        {
            get
            {
                if (screenSize.x > 0)
                    return screenSize;
                screenSize.x = Screen.width;
                screenSize.y = Screen.height;
                return screenSize;
            }
            private set => screenSize = value;
        }

        public static float RatioScaleScreen
        {
            get
            {
                if (ratioScaleScreen > 0) 
                    return ratioScaleScreen;
                var deviceRatio = ScreenSize.x / ScreenSize.y;
                var deviceWidth = 1080 / deviceRatio;
                ratioScaleScreen = deviceWidth / 1920;
                return ratioScaleScreen;
            }
            private set => ratioScaleScreen = value;
        }
        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);
                Initialize();
            }
            else 
                Destroy(gameObject);
        }

        private void Start()
        {
            if (SceneManager.GetActiveScene().name == LoadingManager.SCENE_LOADING)
            {
                if (RootManager.Instance.IsTutorial(0))
                {
                    Utils.GameMode = GameMode.Normal;
                    Utils.CurrentLevel = 0;
                    LoadingManager.Instance.LoadScene(LoadingManager.SCENE_DATA, LoadingManager.SCENE_GAME);
                }
                else
                    LoadingManager.Instance.LoadScene(LoadingManager.SCENE_DATA, LoadingManager.SCENE_HOME);
            }
        }

        private void Initialize()
        {
#if UNITY_EDITOR || UNITY_ANDROID
            Application.targetFrameRate = 60;
#endif
            var deviceRatio = ScreenSize.x / ScreenSize.y;
            var deviceWidth = 1080 / deviceRatio;
            RatioScaleScreen = deviceWidth / 1920;
            Debug.Log("Screen Size -- " + ScreenSize + " -- " + RatioScaleScreen);
        }
    }
}
