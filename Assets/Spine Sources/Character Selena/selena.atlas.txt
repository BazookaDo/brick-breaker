
selena.png
size: 1736,532
format: RGBA8888
filter: Linear,Linear
repeat: none
basic/L-chan1
  rotate: false
  xy: 992, 50
  size: 105, 128
  orig: 107, 130
  offset: 1, 1
  index: -1
basic/R-chan1
  rotate: false
  xy: 992, 50
  size: 105, 128
  orig: 107, 130
  offset: 1, 1
  index: -1
basic/L-chan2
  rotate: false
  xy: 812, 9
  size: 49, 162
  orig: 51, 164
  offset: 1, 1
  index: -1
basic/L-chan3
  rotate: false
  xy: 370, 105
  size: 69, 51
  orig: 71, 53
  offset: 1, 1
  index: -1
basic/L-tay1
  rotate: false
  xy: 441, 5
  size: 77, 151
  orig: 79, 153
  offset: 1, 1
  index: -1
basic/L-tay2
  rotate: true
  xy: 1238, 4
  size: 81, 110
  orig: 83, 112
  offset: 1, 1
  index: -1
basic/L-tay3
  rotate: false
  xy: 1413, 53
  size: 56, 77
  orig: 58, 79
  offset: 1, 1
  index: -1
basic/R-chan2
  rotate: false
  xy: 712, 8
  size: 48, 163
  orig: 50, 165
  offset: 1, 1
  index: -1
basic/R-chan3
  rotate: true
  xy: 1551, 8
  size: 68, 51
  orig: 70, 53
  offset: 1, 1
  index: -1
basic/co
  rotate: false
  xy: 1350, 2
  size: 61, 95
  orig: 63, 97
  offset: 1, 1
  index: -1
basic/head
  rotate: true
  xy: 2, 103
  size: 427, 366
  orig: 429, 368
  offset: 1, 1
  index: -1
basic/long may 1
  rotate: false
  xy: 1482, 5
  size: 67, 46
  orig: 69, 48
  offset: 1, 1
  index: -1
basic/long may 2
  rotate: false
  xy: 988, 180
  size: 76, 28
  orig: 78, 30
  offset: 1, 1
  index: -1
basic/tay 1
  rotate: false
  xy: 1551, 78
  size: 59, 59
  orig: 62, 61
  offset: 2, 1
  index: -1
basic/tay duoi 1
  rotate: false
  xy: 1206, 87
  size: 46, 122
  orig: 48, 124
  offset: 1, 1
  index: -1
basic/tay tren 1
  rotate: false
  xy: 914, 41
  size: 76, 137
  orig: 78, 139
  offset: 1, 1
  index: -1
basic/vay
  rotate: false
  xy: 897, 210
  size: 215, 320
  orig: 215, 320
  offset: 0, 0
  index: -1
connguoi1
  rotate: false
  xy: 397, 74
  size: 35, 29
  orig: 37, 31
  offset: 1, 1
  index: -1
connguoi2
  rotate: false
  xy: 397, 43
  size: 32, 29
  orig: 34, 31
  offset: 1, 1
  index: -1
daytreo
  rotate: true
  xy: 991, 15
  size: 24, 119
  orig: 24, 119
  offset: 0, 0
  index: -1
daytroi
  rotate: false
  xy: 897, 180
  size: 89, 28
  orig: 89, 28
  offset: 0, 0
  index: -1
hoang_hot
  rotate: false
  xy: 1628, 359
  size: 94, 60
  orig: 96, 62
  offset: 1, 1
  index: -1
mat 1
  rotate: true
  xy: 2, 4
  size: 97, 85
  orig: 99, 87
  offset: 1, 1
  index: -1
mat 2
  rotate: false
  xy: 1628, 214
  size: 91, 80
  orig: 93, 83
  offset: 1, 1
  index: -1
mat1-nham
  rotate: true
  xy: 89, 4
  size: 97, 85
  orig: 99, 87
  offset: 1, 1
  index: -1
mat2-nham
  rotate: false
  xy: 1614, 132
  size: 91, 80
  orig: 93, 83
  offset: 1, 1
  index: -1
mieng
  rotate: false
  xy: 1628, 421
  size: 106, 42
  orig: 108, 44
  offset: 1, 1
  index: -1
mom_ha_to
  rotate: false
  xy: 293, 5
  size: 102, 96
  orig: 105, 98
  offset: 2, 1
  index: -1
mui
  rotate: true
  xy: 176, 5
  size: 96, 115
  orig: 98, 117
  offset: 1, 1
  index: -1
nuocmat
  rotate: false
  xy: 1385, 139
  size: 227, 89
  orig: 229, 91
  offset: 1, 1
  index: -1
selena/L-chan1
  rotate: false
  xy: 1099, 80
  size: 105, 128
  orig: 107, 130
  offset: 1, 1
  index: -1
selena/R-chan1
  rotate: false
  xy: 1099, 80
  size: 105, 128
  orig: 107, 130
  offset: 1, 1
  index: -1
selena/L-chan2
  rotate: false
  xy: 863, 9
  size: 49, 162
  orig: 51, 164
  offset: 1, 1
  index: -1
selena/L-chan3
  rotate: true
  xy: 1112, 2
  size: 76, 61
  orig: 78, 63
  offset: 1, 1
  index: -1
selena/L-tay1
  rotate: false
  xy: 520, 7
  size: 85, 149
  orig: 87, 151
  offset: 1, 1
  index: -1
selena/L-tay2
  rotate: false
  xy: 1302, 99
  size: 81, 110
  orig: 83, 112
  offset: 1, 1
  index: -1
selena/L-tay3
  rotate: false
  xy: 1471, 53
  size: 56, 77
  orig: 58, 79
  offset: 1, 1
  index: -1
selena/R-chan2
  rotate: false
  xy: 762, 8
  size: 48, 163
  orig: 50, 165
  offset: 1, 1
  index: -1
selena/R-chan3
  rotate: true
  xy: 1175, 2
  size: 76, 61
  orig: 78, 63
  offset: 1, 1
  index: -1
selena/R-tay1
  rotate: false
  xy: 607, 15
  size: 103, 141
  orig: 105, 143
  offset: 1, 1
  index: -1
selena/R-tay2
  rotate: false
  xy: 1254, 87
  size: 46, 122
  orig: 48, 124
  offset: 1, 1
  index: -1
selena/R-tay3
  rotate: false
  xy: 1664, 71
  size: 59, 59
  orig: 62, 61
  offset: 2, 1
  index: -1
selena/co
  rotate: true
  xy: 1628, 296
  size: 61, 92
  orig: 63, 94
  offset: 1, 1
  index: -1
selena/dead
  rotate: true
  xy: 1330, 230
  size: 300, 296
  orig: 302, 298
  offset: 1, 1
  index: -1
selena/long may 1
  rotate: false
  xy: 1413, 4
  size: 67, 47
  orig: 69, 49
  offset: 1, 1
  index: -1
selena/long may 2
  rotate: false
  xy: 914, 12
  size: 75, 27
  orig: 77, 29
  offset: 1, 1
  index: -1
selena/toc truco
  rotate: true
  xy: 370, 158
  size: 372, 256
  orig: 374, 258
  offset: 1, 1
  index: -1
selena/toc2
  rotate: true
  xy: 628, 173
  size: 357, 267
  orig: 359, 269
  offset: 1, 1
  index: -1
selena/vay
  rotate: false
  xy: 1114, 211
  size: 214, 319
  orig: 214, 319
  offset: 0, 0
  index: -1
traitim
  rotate: true
  xy: 1604, 9
  size: 67, 58
  orig: 67, 59
  offset: 0, 1
  index: -1
vui_ve
  rotate: false
  xy: 1628, 465
  size: 106, 65
  orig: 108, 67
  offset: 1, 1
  index: -1
