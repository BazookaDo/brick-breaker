
ball.png
size: 1812,240
format: RGBA8888
filter: Linear,Linear
repeat: none
basic/1
  rotate: true
  xy: 430, 114
  size: 57, 61
  orig: 57, 61
  offset: 0, 0
  index: -1
basic/10
  rotate: true
  xy: 898, 2
  size: 48, 41
  orig: 48, 41
  offset: 0, 0
  index: -1
basic/11
  rotate: false
  xy: 545, 2
  size: 36, 62
  orig: 36, 62
  offset: 0, 0
  index: -1
basic/12
  rotate: true
  xy: 1763, 181
  size: 57, 47
  orig: 57, 47
  offset: 0, 0
  index: -1
basic/2
  rotate: false
  xy: 556, 66
  size: 59, 50
  orig: 59, 50
  offset: 0, 0
  index: -1
basic/3
  rotate: false
  xy: 1239, 143
  size: 55, 45
  orig: 55, 45
  offset: 0, 0
  index: -1
basic/4
  rotate: false
  xy: 203, 35
  size: 58, 62
  orig: 58, 63
  offset: 0, 1
  index: -1
basic/5
  rotate: true
  xy: 889, 146
  size: 39, 68
  orig: 39, 68
  offset: 0, 0
  index: -1
basic/6
  rotate: true
  xy: 583, 3
  size: 61, 40
  orig: 61, 40
  offset: 0, 0
  index: -1
basic/7
  rotate: false
  xy: 1675, 24
  size: 35, 30
  orig: 35, 30
  offset: 0, 0
  index: -1
basic/8
  rotate: false
  xy: 330, 101
  size: 37, 67
  orig: 37, 67
  offset: 0, 0
  index: -1
basic/9
  rotate: false
  xy: 2, 2
  size: 47, 71
  orig: 47, 71
  offset: 0, 0
  index: -1
bia/1
  rotate: false
  xy: 938, 52
  size: 43, 43
  orig: 43, 43
  offset: 0, 0
  index: -1
bia/10
  rotate: false
  xy: 1037, 2
  size: 45, 45
  orig: 45, 45
  offset: 0, 0
  index: -1
bia/11
  rotate: false
  xy: 1675, 95
  size: 39, 39
  orig: 39, 39
  offset: 0, 0
  index: -1
bia/12
  rotate: false
  xy: 1409, 86
  size: 43, 43
  orig: 43, 43
  offset: 0, 0
  index: -1
bia/13
  rotate: false
  xy: 1506, 48
  size: 46, 46
  orig: 46, 46
  offset: 0, 0
  index: -1
bia/2
  rotate: false
  xy: 941, 4
  size: 46, 46
  orig: 46, 46
  offset: 0, 0
  index: -1
bia/3
  rotate: false
  xy: 1749, 27
  size: 31, 31
  orig: 31, 31
  offset: 0, 0
  index: -1
bia/4
  rotate: false
  xy: 1561, 7
  size: 39, 39
  orig: 39, 39
  offset: 0, 0
  index: -1
bia/5
  rotate: false
  xy: 1714, 60
  size: 33, 33
  orig: 33, 33
  offset: 0, 0
  index: -1
bia/6
  rotate: false
  xy: 983, 52
  size: 43, 43
  orig: 43, 43
  offset: 0, 0
  index: -1
bia/7
  rotate: false
  xy: 1716, 97
  size: 37, 37
  orig: 37, 37
  offset: 0, 0
  index: -1
bia/8
  rotate: false
  xy: 1084, 2
  size: 43, 45
  orig: 43, 45
  offset: 0, 0
  index: -1
bia/9
  rotate: false
  xy: 989, 4
  size: 46, 46
  orig: 46, 46
  offset: 0, 0
  index: -1
bong-ro/1
  rotate: true
  xy: 51, 4
  size: 69, 41
  orig: 71, 45
  offset: 1, 1
  index: -1
bong-ro/10
  rotate: true
  xy: 678, 73
  size: 56, 48
  orig: 58, 50
  offset: 1, 1
  index: -1
bong-ro/11
  rotate: false
  xy: 1368, 2
  size: 27, 44
  orig: 30, 46
  offset: 2, 1
  index: -1
bong-ro/12
  rotate: true
  xy: 1050, 149
  size: 39, 61
  orig: 41, 63
  offset: 1, 1
  index: -1
bong-ro/13
  rotate: true
  xy: 960, 97
  size: 51, 54
  orig: 54, 56
  offset: 2, 1
  index: -1
bong-ro/2
  rotate: false
  xy: 1329, 48
  size: 46, 39
  orig: 48, 41
  offset: 1, 1
  index: -1
bong-ro/3
  rotate: false
  xy: 1182, 3
  size: 52, 35
  orig: 55, 37
  offset: 2, 1
  index: -1
bong-ro/4
  rotate: true
  xy: 341, 173
  size: 65, 79
  orig: 67, 81
  offset: 1, 1
  index: -1
bong-ro/5
  rotate: true
  xy: 733, 142
  size: 38, 68
  orig: 40, 70
  offset: 1, 1
  index: -1
bong-ro/6
  rotate: true
  xy: 1614, 194
  size: 44, 73
  orig: 46, 75
  offset: 1, 1
  index: -1
bong-ro/7
  rotate: true
  xy: 1640, 50
  size: 35, 33
  orig: 38, 36
  offset: 2, 1
  index: -1
bong-ro/8
  rotate: true
  xy: 199, 99
  size: 65, 62
  orig: 68, 64
  offset: 2, 1
  index: -1
bong-ro/9
  rotate: true
  xy: 1130, 190
  size: 48, 78
  orig: 50, 80
  offset: 1, 1
  index: -1
boomerang/1
  rotate: false
  xy: 1290, 2
  size: 40, 44
  orig: 42, 47
  offset: 1, 2
  index: -1
boomerang/10
  rotate: false
  xy: 1503, 2
  size: 27, 44
  orig: 29, 46
  offset: 1, 1
  index: -1
boomerang/11
  rotate: false
  xy: 863, 52
  size: 41, 42
  orig: 43, 44
  offset: 1, 1
  index: -1
boomerang/12
  rotate: false
  xy: 1014, 150
  size: 34, 36
  orig: 36, 38
  offset: 1, 1
  index: -1
boomerang/13
  rotate: false
  xy: 1602, 2
  size: 30, 47
  orig: 32, 49
  offset: 1, 1
  index: -1
boomerang/2
  rotate: false
  xy: 1749, 60
  size: 31, 33
  orig: 36, 35
  offset: 4, 1
  index: -1
boomerang/3
  rotate: true
  xy: 1070, 49
  size: 41, 55
  orig: 43, 57
  offset: 1, 1
  index: -1
boomerang/4
  rotate: true
  xy: 1028, 52
  size: 43, 40
  orig: 45, 42
  offset: 1, 1
  index: -1
boomerang/5
  rotate: true
  xy: 1506, 96
  size: 38, 53
  orig: 40, 55
  offset: 1, 1
  index: -1
boomerang/6
  rotate: true
  xy: 1289, 48
  size: 40, 38
  orig: 42, 40
  offset: 1, 1
  index: -1
boomerang/7
  rotate: true
  xy: 728, 113
  size: 27, 68
  orig: 29, 70
  offset: 1, 1
  index: -1
boomerang/8
  rotate: true
  xy: 561, 118
  size: 46, 68
  orig: 48, 70
  offset: 1, 1
  index: -1
boomerang/9
  rotate: false
  xy: 1332, 2
  size: 34, 44
  orig: 36, 46
  offset: 1, 1
  index: -1
coc-cuoc/1
  rotate: true
  xy: 1452, 17
  size: 33, 49
  orig: 35, 51
  offset: 1, 1
  index: -1
coc-cuoc/10
  rotate: true
  xy: 1176, 151
  size: 37, 61
  orig: 39, 63
  offset: 1, 1
  index: -1
coc-cuoc/11
  rotate: false
  xy: 1049, 190
  size: 79, 48
  orig: 82, 50
  offset: 1, 1
  index: -1
coc-cuoc/12
  rotate: true
  xy: 680, 10
  size: 61, 46
  orig: 63, 48
  offset: 1, 1
  index: -1
coc-cuoc/13
  rotate: false
  xy: 959, 150
  size: 53, 35
  orig: 55, 38
  offset: 1, 2
  index: -1
coc-cuoc/2
  rotate: false
  xy: 328, 47
  size: 58, 52
  orig: 60, 54
  offset: 1, 1
  index: -1
coc-cuoc/3
  rotate: false
  xy: 1371, 203
  size: 86, 35
  orig: 88, 38
  offset: 1, 2
  index: -1
coc-cuoc/4
  rotate: false
  xy: 179, 166
  size: 80, 72
  orig: 82, 74
  offset: 1, 1
  index: -1
coc-cuoc/5
  rotate: true
  xy: 443, 47
  size: 65, 49
  orig: 67, 51
  offset: 1, 1
  index: -1
coc-cuoc/6
  rotate: true
  xy: 1445, 150
  size: 48, 69
  orig: 50, 71
  offset: 1, 1
  index: -1
coc-cuoc/7
  rotate: true
  xy: 617, 66
  size: 50, 59
  orig: 52, 61
  offset: 1, 1
  index: -1
coc-cuoc/8
  rotate: true
  xy: 435, 2
  size: 43, 47
  orig: 45, 49
  offset: 1, 1
  index: -1
coc-cuoc/9
  rotate: false
  xy: 1181, 95
  size: 53, 54
  orig: 55, 56
  offset: 1, 1
  index: -1
donut/1
  rotate: false
  xy: 144, 33
  size: 57, 64
  orig: 59, 66
  offset: 1, 1
  index: -1
donut/10
  rotate: true
  xy: 1763, 130
  size: 49, 47
  orig: 51, 49
  offset: 1, 1
  index: -1
donut/11
  rotate: true
  xy: 906, 52
  size: 42, 30
  orig: 44, 32
  offset: 1, 1
  index: -1
donut/12
  rotate: false
  xy: 733, 182
  size: 78, 56
  orig: 80, 58
  offset: 1, 1
  index: -1
donut/13
  rotate: false
  xy: 131, 99
  size: 66, 65
  orig: 68, 68
  offset: 1, 1
  index: -1
donut/2
  rotate: false
  xy: 1445, 200
  size: 1, 1
  orig: 1, 1
  offset: 0, 0
  index: -1
donut/3
  rotate: false
  xy: 2, 157
  size: 92, 81
  orig: 94, 83
  offset: 1, 1
  index: -1
donut/4
  rotate: true
  xy: 1353, 89
  size: 40, 54
  orig: 42, 56
  offset: 1, 1
  index: -1
donut/5
  rotate: true
  xy: 798, 96
  size: 42, 66
  orig: 44, 68
  offset: 1, 1
  index: -1
donut/6
  rotate: true
  xy: 1657, 136
  size: 55, 67
  orig: 57, 69
  offset: 1, 1
  index: -1
donut/7
  rotate: false
  xy: 1537, 199
  size: 75, 39
  orig: 77, 41
  offset: 1, 1
  index: -1
donut/8
  rotate: false
  xy: 1516, 136
  size: 69, 61
  orig: 71, 63
  offset: 1, 1
  index: -1
donut/9
  rotate: true
  xy: 261, 170
  size: 68, 78
  orig: 70, 80
  offset: 1, 1
  index: -1
dtdd/10
  rotate: true
  xy: 1417, 131
  size: 31, 26
  orig: 33, 28
  offset: 1, 1
  index: -1
dtdd/11
  rotate: false
  xy: 144, 2
  size: 29, 29
  orig: 31, 36
  offset: 1, 6
  index: -1
dtdd/13
  rotate: false
  xy: 1782, 63
  size: 28, 30
  orig: 30, 32
  offset: 1, 1
  index: -1
dua/1
  rotate: true
  xy: 813, 180
  size: 58, 74
  orig: 60, 77
  offset: 1, 1
  index: -1
dua/10
  rotate: true
  xy: 1726, 136
  size: 55, 35
  orig: 57, 38
  offset: 1, 1
  index: -1
dua/11
  rotate: true
  xy: 1689, 193
  size: 45, 72
  orig: 47, 74
  offset: 1, 1
  index: -1
dua/12
  rotate: false
  xy: 1587, 135
  size: 68, 57
  orig: 70, 59
  offset: 1, 1
  index: -1
dua/13
  rotate: true
  xy: 579, 166
  size: 72, 73
  orig: 74, 75
  offset: 1, 1
  index: -1
dua/2
  rotate: true
  xy: 1290, 192
  size: 46, 79
  orig: 48, 81
  offset: 1, 1
  index: -1
dua/3
  rotate: false
  xy: 843, 2
  size: 53, 48
  orig: 55, 51
  offset: 1, 1
  index: -1
dua/4
  rotate: true
  xy: 263, 2
  size: 43, 65
  orig: 45, 67
  offset: 1, 1
  index: -1
dua/5
  rotate: false
  xy: 1454, 98
  size: 50, 50
  orig: 52, 52
  offset: 1, 1
  index: -1
dua/6
  rotate: false
  xy: 388, 47
  size: 53, 62
  orig: 55, 64
  offset: 1, 1
  index: -1
dua/7
  rotate: false
  xy: 1397, 52
  size: 55, 32
  orig: 57, 34
  offset: 1, 1
  index: -1
dua/8
  rotate: false
  xy: 385, 2
  size: 48, 43
  orig: 50, 45
  offset: 1, 1
  index: -1
dua/9
  rotate: true
  xy: 503, 173
  size: 65, 74
  orig: 67, 76
  offset: 1, 1
  index: -1
eath/1
  rotate: true
  xy: 85, 86
  size: 69, 44
  orig: 71, 46
  offset: 1, 1
  index: -1
eath/10
  rotate: false
  xy: 785, 2
  size: 56, 48
  orig: 58, 50
  offset: 1, 1
  index: -1
eath/11
  rotate: false
  xy: 1532, 2
  size: 27, 44
  orig: 30, 46
  offset: 2, 1
  index: -1
eath/12
  rotate: true
  xy: 1113, 149
  size: 39, 61
  orig: 41, 63
  offset: 1, 1
  index: -1
eath/13
  rotate: true
  xy: 1236, 90
  size: 51, 54
  orig: 54, 56
  offset: 2, 1
  index: -1
eath/2
  rotate: true
  xy: 1554, 48
  size: 46, 39
  orig: 48, 41
  offset: 1, 1
  index: -1
eath/3
  rotate: false
  xy: 1236, 2
  size: 52, 35
  orig: 55, 37
  offset: 2, 1
  index: -1
eath/4
  rotate: true
  xy: 422, 173
  size: 65, 79
  orig: 67, 81
  offset: 1, 1
  index: -1
eath/5
  rotate: true
  xy: 803, 140
  size: 38, 68
  orig: 40, 70
  offset: 1, 1
  index: -1
eath/6
  rotate: true
  xy: 654, 131
  size: 44, 72
  orig: 46, 74
  offset: 1, 1
  index: -1
eath/7
  rotate: false
  xy: 1755, 95
  size: 35, 33
  orig: 38, 36
  offset: 2, 1
  index: -1
eath/8
  rotate: false
  xy: 263, 106
  size: 65, 62
  orig: 68, 64
  offset: 2, 1
  index: -1
eath/9
  rotate: true
  xy: 1210, 190
  size: 48, 78
  orig: 50, 80
  offset: 1, 1
  index: -1
ech/1
  rotate: false
  xy: 728, 13
  size: 55, 55
  orig: 57, 58
  offset: 1, 1
  index: -1
ech/10
  rotate: false
  xy: 1459, 200
  size: 76, 38
  orig: 79, 43
  offset: 2, 1
  index: -1
ech/11
  rotate: false
  xy: 1236, 39
  size: 51, 49
  orig: 53, 51
  offset: 1, 1
  index: -1
ech/12
  rotate: false
  xy: 1016, 97
  size: 52, 50
  orig: 54, 53
  offset: 1, 1
  index: -1
ech/13
  rotate: false
  xy: 866, 96
  size: 49, 42
  orig: 51, 44
  offset: 1, 1
  index: -1
ech/2
  rotate: false
  xy: 1129, 12
  size: 51, 35
  orig: 53, 37
  offset: 1, 1
  index: -1
ech/3
  rotate: false
  xy: 1070, 92
  size: 55, 55
  orig: 57, 58
  offset: 1, 2
  index: -1
ech/4
  rotate: true
  xy: 94, 15
  size: 69, 48
  orig: 71, 50
  offset: 1, 1
  index: -1
ech/5
  rotate: true
  xy: 1371, 164
  size: 37, 72
  orig: 40, 74
  offset: 2, 1
  index: -1
ech/6
  rotate: true
  xy: 1714, 27
  size: 31, 33
  orig: 33, 35
  offset: 1, 1
  index: -1
ech/7
  rotate: true
  xy: 625, 6
  size: 58, 53
  orig: 60, 55
  offset: 1, 1
  index: -1
ech/8
  rotate: false
  xy: 493, 121
  size: 66, 50
  orig: 68, 52
  offset: 1, 1
  index: -1
ech/9
  rotate: false
  xy: 494, 66
  size: 60, 53
  orig: 62, 55
  offset: 1, 1
  index: -1
heo/13
  rotate: false
  xy: 1292, 94
  size: 59, 44
  orig: 61, 46
  offset: 1, 1
  index: -1
shuriken/1
  rotate: true
  xy: 654, 177
  size: 61, 77
  orig: 64, 79
  offset: 2, 1
  index: -1
shuriken/10
  rotate: false
  xy: 263, 47
  size: 63, 57
  orig: 65, 59
  offset: 1, 1
  index: -1
shuriken/11
  rotate: false
  xy: 969, 188
  size: 78, 50
  orig: 80, 52
  offset: 1, 1
  index: -1
shuriken/12
  rotate: false
  xy: 1127, 49
  size: 53, 41
  orig: 55, 44
  offset: 1, 1
  index: -1
shuriken/13
  rotate: true
  xy: 1127, 92
  size: 55, 52
  orig: 57, 54
  offset: 1, 1
  index: -1
shuriken/2
  rotate: true
  xy: 1296, 140
  size: 50, 58
  orig: 52, 60
  offset: 1, 1
  index: -1
shuriken/3
  rotate: false
  xy: 96, 166
  size: 81, 72
  orig: 84, 74
  offset: 2, 1
  index: -1
shuriken/4
  rotate: false
  xy: 889, 187
  size: 78, 51
  orig: 80, 53
  offset: 1, 1
  index: -1
shuriken/5
  rotate: false
  xy: 2, 75
  size: 81, 80
  orig: 83, 82
  offset: 1, 1
  index: -1
shuriken/6
  rotate: false
  xy: 494, 2
  size: 49, 62
  orig: 51, 64
  offset: 1, 1
  index: -1
shuriken/7
  rotate: true
  xy: 1601, 92
  size: 41, 43
  orig: 44, 45
  offset: 1, 1
  index: -1
shuriken/8
  rotate: true
  xy: 1595, 51
  size: 39, 43
  orig: 41, 46
  offset: 1, 2
  index: -1
shuriken/9
  rotate: false
  xy: 369, 111
  size: 59, 60
  orig: 61, 62
  offset: 1, 1
  index: -1
ta/1
  rotate: false
  xy: 1646, 87
  size: 27, 46
  orig: 29, 48
  offset: 1, 1
  index: -1
ta/10
  rotate: false
  xy: 330, 2
  size: 53, 43
  orig: 55, 45
  offset: 1, 1
  index: -1
ta/11
  rotate: false
  xy: 1634, 15
  size: 38, 33
  orig: 40, 35
  offset: 1, 1
  index: -1
ta/12
  rotate: true
  xy: 1397, 16
  size: 34, 53
  orig: 36, 55
  offset: 1, 1
  index: -1
ta/13
  rotate: false
  xy: 1675, 56
  size: 37, 37
  orig: 39, 39
  offset: 1, 1
  index: -1
ta/2
  rotate: true
  xy: 1561, 96
  size: 37, 38
  orig: 39, 41
  offset: 1, 1
  index: -1
ta/3
  rotate: false
  xy: 1356, 131
  size: 59, 31
  orig: 61, 33
  offset: 1, 1
  index: -1
ta/4
  rotate: false
  xy: 203, 6
  size: 58, 27
  orig: 60, 29
  offset: 1, 1
  index: -1
ta/5
  rotate: true
  xy: 728, 70
  size: 41, 66
  orig: 43, 68
  offset: 1, 1
  index: -1
ta/6
  rotate: true
  xy: 1182, 40
  size: 53, 52
  orig: 55, 54
  offset: 1, 1
  index: -1
ta/7
  rotate: false
  xy: 796, 52
  size: 65, 42
  orig: 67, 44
  offset: 1, 1
  index: -1
ta/8
  rotate: false
  xy: 917, 97
  size: 41, 47
  orig: 43, 49
  offset: 1, 1
  index: -1
ta/9
  rotate: false
  xy: 1454, 52
  size: 50, 44
  orig: 52, 46
  offset: 1, 1
  index: -1
