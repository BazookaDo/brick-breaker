﻿using System.Collections.Generic;
using Do.Scripts.Tools.Audio;
using Do.Scripts.Tools.Dialog;
using Do.Scripts.Tools.Language;
using Home.Daily;
using Home.House;
using Home.Level;
using Home.LevelRescue;
using Home.Spin;
using Loading;
using Popup;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Home.Main
{
    public class HomeManager : MonoBehaviour
    {
        public static HomeManager Instance { private set; get; }
        
        [SerializeField] private GameObject mainGroup, modeGroup;
        [SerializeField] private List<MainModeButton> mainModeButtons;
        [SerializeField] private HorizontalLayoutGroup horizontalLayoutGroup;
        private int _currentMode = 2;
        [SerializeField] private TextMeshProUGUI levelButtonHomeText;
        [SerializeField] private GraphicRaycaster graphicRay;

        [Header("PopUp")] 
        [SerializeField] private DailyPopup dailyPopup;
        [SerializeField] private SpinPopup spinPopup;
        [SerializeField] private LevelPopup levelPopup;
        [SerializeField] private HousePopup housePopup;
        [SerializeField] private LevelRescuePopup levelRescuePopup;

        [Header("Info")]
        [SerializeField] private GameObject infoGroup;
        [SerializeField] private TextMeshProUGUI coinText;
        [SerializeField] private TextMeshProUGUI starText;
        [SerializeField] private Transform coinTarget;
        [SerializeField] private CanvasGroup btnRemoveAds;
        private int _currentCoin, _currentStar;

        public Vector2 CoinPosition => coinTarget.position;

        private void Awake()
        {
            Instance = this;
        }

        private void OnDestroy()
        {
            Instance = null;
        }

        private void Start()
        {
            _currentMode = 2;
            LoadingManager.Instance.AddCallBackLoadData(delegate
            {
                dailyPopup.Initialize();
                spinPopup.Initialize();
                levelPopup.Initialize();
                housePopup.Initialize();
                levelRescuePopup.Initialize();
                ShowCoin();
                ShowStar();
                Check_Language_Button_home();
                if (RootManager.Instance.OpenRescueMode)
                {
                    Button_Rescue();
                    RootManager.Instance.OpenRescueMode = false;
                }
            });
            LoadingManager.Instance.AddCallBackEndLoading(delegate
            {
                CheckRemoveAds();
                PopupManager.Instance.Building_Popup();
                AudioManager.Instance.Play(MusicType.Home);
            });
        }

        #region Info

        public void ShowCoin()
        {
            _currentCoin = RootManager.Instance.Coin;
            coinText.text = _currentCoin.ToString();
        }

        public void ShowMoreCoin(int value)
        {
            _currentCoin += value;
            if (_currentCoin > RootManager.Instance.Coin)
                _currentCoin = RootManager.Instance.Coin;
            coinText.text = _currentCoin.ToString();
        }

        public void ShowStar()
        {
            _currentStar = RootManager.Instance.Star;
            starText.text = _currentStar.ToString();
        }

        public void ShowMoreStar(int value)
        {
            _currentStar += value;
            if (_currentStar > RootManager.Instance.Star)
                _currentStar = RootManager.Instance.Star;
            starText.text = _currentStar.ToString();
        }

        private void CheckRemoveAds()
        {
            btnRemoveAds.alpha = RootManager.Instance.IsRemoveAds ? 0.4f : 1f;
        }

        #endregion

        public void ActiveRay()
        {
            graphicRay.enabled = true;
        }

        public void UnActiveRay()
        {
            graphicRay.enabled = false;
        }

        public void ActiveMain()
        {
            mainGroup.SetActive(true);
            infoGroup.SetActive(true);
            modeGroup.SetActive(true);
        }

        public void UnActiveMain()
        {
            mainGroup.SetActive(false);
            infoGroup.SetActive(false);
            modeGroup.SetActive(false);
        }

        private void Switch_Mode(int mode)
        {
            switch (_currentMode)
            {
                case 0:
                    ActiveRay();
                    PopupManager.Instance.Shop_UnActive();
                    break;
                case 1:
                    housePopup.gameObject.SetActive(false);
                    break;
                case 2:
                    mainGroup.SetActive(false);
                    PopupManager.Instance.Building_UnActive();
                    break;
                case 3:
                    levelRescuePopup.InBack = null;
                    levelRescuePopup.gameObject.SetActive(false);
                    infoGroup.SetActive(true);
                    break;
                case 4:
                    ActiveRay();
                    PopupManager.Instance.Skin_UnActive();
                    break;
            }

            _currentMode = mode;
            for (var i = 0; i < mainModeButtons.Count; i++)
            {
                if (i == _currentMode)
                    mainModeButtons[i].Selected();
                else
                    mainModeButtons[i].UnSelected();
            }
            horizontalLayoutGroup.SetLayoutHorizontal();
            Check_Language_Button_home();
            switch (_currentMode)
            {
                case 0:
                    PopupManager.Instance.Shop_Popup(false, () =>
                    {
                        CheckRemoveAds();
                        Switch_Mode(2);
                    });
                    break;
                case 1:
                    housePopup.gameObject.SetActive(true);
                    break;
                case 2:
                    mainGroup.SetActive(true);
                    PopupManager.Instance.Building_Popup(true, false);
                    break;
                case 3:
                    levelRescuePopup.gameObject.SetActive(true);
                    infoGroup.SetActive(false);
                    break;
                case 4:
                    PopupManager.Instance.Skin_Popup(0, false, () => Switch_Mode(2));
                    break;
            }
        }

        private void Check_Language_Button_home()
        {
            if (_currentMode == 2)
            {
                var title = LanguageManager.Instance.Language == Language.Eng ? "LEVEL " : "CẤP ĐỘ ";
                levelButtonHomeText.text = title + (RootManager.Instance.CurrentLevelOpen + 1);
            }
            else
            {
                levelButtonHomeText.text = LanguageManager.Instance.Language == Language.Eng ? "PLAY" : "CHƠI";
            }
        }

        public void Open_Skin_Popup(int tab)
        {
            mainGroup.SetActive(false);
            PopupManager.Instance.Building_UnActive();
            _currentMode = 4;
            for (var i = 0; i < mainModeButtons.Count; i++)
            {
                if (i == _currentMode)
                    mainModeButtons[i].Selected();
                else
                    mainModeButtons[i].UnSelected();
            }
            horizontalLayoutGroup.SetLayoutHorizontal();
            Check_Language_Button_home();
            PopupManager.Instance.Skin_Popup(tab, false, () => Switch_Mode(2));
        }

        #region EventButton

        public void Button_Setting()
        {
            AudioManager.Instance.Play(SoundType.Click);
            UnActiveRay();
            DialogManager.Instance.Setting_Dialog(() =>
            {
                ActiveRay();
                Check_Language_Button_home();
            });
            DialogManager.Instance.Add_Event_OnChange_Language(Check_Language_Button_home);
        }

        public void Button_Daily()
        {
            AudioManager.Instance.Play(SoundType.Click);
            dailyPopup.gameObject.SetActive(true);
        }

        public void Button_Spin()
        {
            AudioManager.Instance.Play(SoundType.Click);
            spinPopup.gameObject.SetActive(true);
            spinPopup.InBack = () =>
            {
                mainGroup.SetActive(true);
                modeGroup.SetActive(true);
                PopupManager.Instance.Building_Popup();
            };
            mainGroup.SetActive(false);
            modeGroup.SetActive(false);
            PopupManager.Instance.Building_UnActive();
        }

        public void Button_Level()
        {
            AudioManager.Instance.Play(SoundType.Click);
            levelPopup.gameObject.SetActive(true);
            levelPopup.InBack = () =>
            {
                mainGroup.SetActive(true);
                modeGroup.SetActive(true);
                PopupManager.Instance.Building_Popup();
            };
            mainGroup.SetActive(false);
            modeGroup.SetActive(false);
            PopupManager.Instance.Building_UnActive();
        }

        public void Button_Shop()
        {
            AudioManager.Instance.Play(SoundType.Click);
            if (_currentMode == 0)
                return;
            Switch_Mode(0);
        }
        
        public void Button_House()
        {
            AudioManager.Instance.Play(SoundType.Click);
            if (_currentMode == 1)
                return;
            Switch_Mode(1);
        }
        
        public void Button_Play()
        {
            AudioManager.Instance.Play(SoundType.Click);
            if (_currentMode == 2)
            {
                levelPopup.PlayCurrentLevel();
                return;
            }
            Switch_Mode(2);
        }
        
        public void Button_Rescue()
        {
            AudioManager.Instance.Play(SoundType.Click);
            if (_currentMode == 3)
                return;
            Switch_Mode(3);
        }
        
        public void Button_Skin()
        {
            AudioManager.Instance.Play(SoundType.Click);
            if (_currentMode == 4)
                return;
            Switch_Mode(4);
        }

        #endregion
    }
}
