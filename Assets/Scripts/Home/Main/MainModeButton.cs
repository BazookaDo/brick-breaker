﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Home.Main
{
    public class MainModeButton : MonoBehaviour
    {
        [SerializeField] private Sprite bgOn, bgOff;
        [SerializeField] private float iconYOn, iconYOff;
        [SerializeField] private Vector2 textSizeOn, textSizeOff;
        [SerializeField] private float textYOn, textYOff;
        
        [Header("UI")]
        [SerializeField] private Image bgImage;
        [SerializeField] private RectTransform iconRect, textRect;
        private RectTransform _myRect, _bgRect;

        private void Awake()
        {
            _myRect = GetComponent<RectTransform>();
            _bgRect = bgImage.GetComponent<RectTransform>();
        }

        public void Selected()
        {
            if (bgImage.sprite == bgOn)
                return;
            var mySize = _myRect.sizeDelta;
            mySize.x = 284;
            _myRect.sizeDelta = mySize;
            
            bgImage.sprite = bgOn;
            bgImage.SetNativeSize();
            var pos = _bgRect.anchoredPosition;
            pos.y = 18;
            _bgRect.anchoredPosition = pos;

            pos = iconRect.anchoredPosition;
            pos.y = iconYOn;
            iconRect.anchoredPosition = pos;
            
            pos = textRect.anchoredPosition;
            pos.y = textYOn;
            textRect.anchoredPosition = pos;
            textRect.sizeDelta = textSizeOn;
        }

        public void UnSelected()
        {
            if (bgImage.sprite == bgOff)
                return;
            var mySize = _myRect.sizeDelta;
            mySize.x = 212;
            _myRect.sizeDelta = mySize;
            
            bgImage.sprite = bgOff;
            bgImage.SetNativeSize();
            var pos = _bgRect.anchoredPosition;
            pos.y = 0;
            _bgRect.anchoredPosition = pos;

            pos = iconRect.anchoredPosition;
            pos.y = iconYOff;
            iconRect.anchoredPosition = pos;
            
            pos = textRect.anchoredPosition;
            pos.y = textYOff;
            textRect.anchoredPosition = pos;
            textRect.sizeDelta = textSizeOff;
        }
    }
}
