﻿using System.Collections.Generic;
using System.Linq;
using Do.Scripts.Tools.Audio;
using Do.Scripts.Tools.Dialog;
using Do.Scripts.Tools.Language;
using EnhancedUI.EnhancedScroller;
using Home.Main;
using InGame;
using Loading;
using Other;
using UnityEngine;

namespace Home.Level
{
    public class LevelPopup : MonoBehaviour, IEnhancedScrollerDelegate
    {
	    [SerializeField] private List<LevelData> listLevelData = new List<LevelData>();
	    
	    [Header("UI")]
	    [SerializeField] private HomeManager homeManager;
	    [SerializeField] private EnhancedScroller scroller;
	    [SerializeField] private EnhancedScrollerCellView cellViewPrefab;
	    [SerializeField] private int target;
	    [SerializeField] private RectTransform selectedBounder;

	    private readonly List<LevelDataCell> _listCells = new List<LevelDataCell>();
	    
	    private bool _scroll;
		
	    private int _reloadScrollerFrameCountLeft = -1;
	    public RectTransform SelectedBounder => selectedBounder;

	    public Callback InBack;
	    
	    private void OnEnable()
	    {
		    if (!_scroll) 
			    return;
		    scroller.RefreshActiveCellViews();
		    scroller.JumpToDataIndex(target, 0, 0);
		    if (target > 0)
			    target = 0;
	    }

	    private void Start()
	    {
		    scroller.Delegate = this;
		    LoadDataScroll();
		    if (!_scroll)
		    {
			    _scroll = true;
			    if (target > 0)
			    {
				    scroller.JumpToDataIndex(target, 0, 0);
				    target = 0;
			    }
		    }

		    var content = scroller.ScrollRect.content;
		    var pos = content.localPosition;
		    pos.z = 0;
		    content.localPosition = pos;
	    }

	    private void OnDisable()
	    {
		    target = TargetScroll();
	    }

	    #region DATA
	    public void Initialize()
	    {
		    listLevelData = RootManager.Instance.ListLevelData.ToList();
		    // listLevelData.Reverse();
		    // add scroll
		    var current = 0;
		    var levelDataCell = new LevelDataCell
		    {
			    listData = new List<LevelData>()
		    };
		    foreach (var levelData in listLevelData)
		    {
			    levelDataCell.listData.Add(levelData);
			    // if (levelData.levelState == LevelState.Open)
				   //  target = _listCells.Count;
			    current++;
			    if(current < 5)
				    continue;
			    current = 0;
			    _listCells.Add(new LevelDataCell
			    {
				    listData = levelDataCell.listData
			    });
			    levelDataCell = new LevelDataCell
			    {
				    listData = new List<LevelData>()
			    };
		    }

		    if (current > 0)
		    {
			    _listCells.Add(new LevelDataCell
			    {
				    listData = levelDataCell.listData
			    });
		    }
		    _listCells.Reverse();
		    target = 0;
		    foreach (var load in _listCells.Select(cell => cell.listData.All(data => data.levelState != LevelState.Open)))
		    {
			    if (load)
			    {
				    target++;
				    continue;
			    }
			    break;
		    }
	    }
	    
	    #endregion

	    public void SelectedBounderFollowParent(RectTransform parent, bool active)
	    {
		    selectedBounder.gameObject.SetActive(active);
		    if (!active) 
			    return;
		    selectedBounder.SetParent(parent);
		    selectedBounder.localPosition = Vector3.zero;
		    selectedBounder.localScale = Vector3.one;
		    selectedBounder.SetSiblingIndex(0);
	    }

	    private int TargetScroll()
	    {
		    for (var i = 0; i < _listCells.Count; i++)
		    {
			    var dataCell = _listCells[i];
			    if (dataCell.listData.Any(levelData => levelData.levelState == LevelState.Open))
				    return i;
		    }
		    return 0;
	    }

	    public void PlayLevel(LevelData levelData)
	    {
		    if (levelData.levelState == LevelState.Lock)
			    return;
		    Utils.GameMode = GameMode.Normal;
		    Utils.CurrentLevel = levelData.level;
		    var title = LanguageManager.Instance.Language == Language.Eng ? "LEVEL " : "CẤP ĐỘ ";
		    DialogManager.Instance.Play_Dialog(title + (Utils.CurrentLevel + 1),
			    delegate
			    {
				    RootManager.Instance.PlayGame();
			    }, 
			    delegate
			    {
				    RootManager.Instance.MoreBall = true;
				    RootManager.Instance.PlayGame();
			    },
			    null);
	    }

	    public void PlayCurrentLevel()
	    {
		    Utils.GameMode = GameMode.Normal;
		    Utils.CurrentLevel = RootManager.Instance.CurrentLevelOpen;
		    var title = LanguageManager.Instance.Language == Language.Eng ? "LEVEL " : "CẤP ĐỘ ";
		    DialogManager.Instance.Play_Dialog(title + (Utils.CurrentLevel + 1),
			    delegate
			    {
				    RootManager.Instance.PlayGame();
			    }, 
			    delegate
			    {
				    RootManager.Instance.MoreBall = true;
				    RootManager.Instance.PlayGame();
			    }, 
			    null);
	    }

	    public void Btn_Back()
	    {
		    AudioManager.Instance.Play(SoundType.Click);
		    gameObject.SetActive(false);
		    InBack?.Invoke();
		    InBack = null;
	    }
	    
		/// <summary>
		/// /////////
		/// </summary>

		#region EnhancedScroller Control

		private void LoadDataScroll()
		{
			// capture the scroller dimensions so that we can reset them when we are done
			var rectTransform = scroller.GetComponent<RectTransform>();
			var size = rectTransform.sizeDelta;

			// set the dimensions to the largest size possible to acommodate all the cells
			rectTransform.sizeDelta = new Vector2(size.x, float.MaxValue);

			// First Pass: reload the scroller so that it can populate the text UI elements in the cell view.
			// The content size fitter will determine how big the cells need to be on subsequent passes
			scroller.ReloadData();

			// reset the scroller size back to what it was originally
			rectTransform.sizeDelta = size;

			// set up our frame countdown so that we can reload the scroller on subsequent frames
			_reloadScrollerFrameCountLeft = 1;
		}

		void LateUpdate()
		{
			// only process if we have a countdown left
			if (_reloadScrollerFrameCountLeft != -1)
			{
				// skip the first frame (frame countdown 1) since it is the one where we set up the scroller text.
				if (_reloadScrollerFrameCountLeft < 1)
				{

					// reload two times, the first to put the newly set content size fitter values into the model,
					// the second to set the scroller's cell sizes based on the model.
					scroller.ReloadData();
				}

				// decrement the frame count
				_reloadScrollerFrameCountLeft--;
			}
		}

		#endregion
		
		/// <summary>
		/// /////////
		/// </summary>

		#region EnhancedScroller Handlers

		public int GetNumberOfCells(EnhancedScroller scroll)
		{
			return _listCells.Count;
		}

		public float GetCellViewSize(EnhancedScroller scroll, int dataIndex)
		{
			// we pull the size of the cell from the model.
			// First pass (frame countdown 2): this size will be zero as set in the LoadData function
			// Second pass (frame countdown 1): this size will be set to the content size fitter in the cell view
			// Third pass (frmae countdown 0): this set value will be pulled here from the scroller
			return cellViewPrefab.GetComponent<RectTransform>().sizeDelta.y;
		}

		public EnhancedScrollerCellView GetCellView(EnhancedScroller scroll, int dataIndex, int cellIndex)
		{
			var cellView = scroll.GetCellView(cellViewPrefab) as LevelCellView;
			if (cellView == null) 
				return null;
			cellView.SetData(this, _listCells[dataIndex]);
			return cellView;

		}

		#endregion
    }
}
