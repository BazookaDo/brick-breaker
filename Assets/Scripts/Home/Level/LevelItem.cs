﻿using System;
using System.Collections.Generic;
using Do.Scripts.Tools.Audio;
using Do.Scripts.Tools.Language;
using Loading;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Home.Level
{
    public class LevelItem : MonoBehaviour
    {
        private LevelPopup _levelPopup;
        [SerializeField]
        private LevelData _levelData;

        [Header("UI")]
        [SerializeField] private TextMeshProUGUI levelText;
        [SerializeField] private Image icon;
        [SerializeField] private List<Sprite> iconStates;
        [SerializeField] private GameObject starRoot;
        [SerializeField] private List<Image> stars;
        [SerializeField] private Sprite starOn, starOff;
        private RectTransform _parent;
        private string _title;
        
        public void SetDataLevel(LevelPopup levelPopup, LevelData levelData)
        {
            gameObject.SetActive(true);
            _levelPopup = levelPopup;
            _levelData = levelData;
            if (_parent == null)
                _parent = GetComponent<RectTransform>();
            UpdateUI();
            
            if (RootManager.Instance.CurrentLevelOpen == levelData.level)
                _levelPopup.SelectedBounderFollowParent(_parent, true);
            else if (_levelPopup.SelectedBounder.parent == _parent)
                _levelPopup.SelectedBounderFollowParent(null, false);
        }

        private void UpdateUI()
        {
            _title = LanguageManager.Instance.Language == Language.Eng ? "LEVEL" : "CẤP ĐỘ";
            levelText.text = "<size=40>" + _title + "\n" + "<size=65>" + (_levelData.level + 1);
            var levelState = _levelData.levelState;
            icon.sprite = iconStates[(int) levelState];
            icon.SetNativeSize();
            if (levelState == LevelState.Pass)
            {
                if (!starRoot.activeSelf)
                    starRoot.SetActive(true);
                for (var i = 0; i < stars.Count; i++)
                {
                    stars[i].sprite = i < _levelData.star ? starOn : starOff;
                }
            }
            else
            {
                if (starRoot.activeSelf)
                    starRoot.SetActive(false);
            }
        }

        public void Btn_Play()
        {
            AudioManager.Instance.Play(SoundType.Click);
            if (_levelData.levelState == LevelState.Lock)
                return;
            _levelPopup.PlayLevel(_levelData);
        }
    }

    [Serializable]
    public class LevelDataCell
    {
        public List<LevelData> listData;
    }

    [Serializable]
    public class LevelData
    {
        public int level;
        public LevelState levelState;
        public int star;
    }

    public enum LevelState
    {
        Lock,
        Open,
        Pass
    }
}
