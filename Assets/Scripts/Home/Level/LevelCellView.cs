﻿using System.Collections.Generic;
using EnhancedUI.EnhancedScroller;
using UnityEngine;

namespace Home.Level
{
    public class LevelCellView : EnhancedScrollerCellView
    {
        [SerializeField] private List<LevelItem> levelItems = new List<LevelItem>();
        private LevelPopup _levelPopup;
        private LevelDataCell _levelDataCell;
        
        public void SetData(LevelPopup levelPopup, LevelDataCell levelDataCell)
        {
            _levelPopup = levelPopup;
            _levelDataCell = levelDataCell;
            var listLevelDataCount = _levelDataCell.listData.Count;
            var levelItemsCount = levelItems.Count;
            for (var i = 0; i < listLevelDataCount; i++)
            {
                levelItems[i].SetDataLevel(_levelPopup, _levelDataCell.listData[i]);
            }

            if (listLevelDataCount >= levelItemsCount)
                return;
            for (var i = listLevelDataCount; i < levelItemsCount; i++)
            {
                levelItems[i].gameObject.SetActive(false);
            }
        }
        
        public override void RefreshCellView()
        {
            var listLevelDataCount = _levelDataCell.listData.Count;
            for (var i = 0; i < listLevelDataCount; i++)
            {
                levelItems[i].SetDataLevel(_levelPopup, _levelDataCell.listData[i]);
            }
        }
    }
}
