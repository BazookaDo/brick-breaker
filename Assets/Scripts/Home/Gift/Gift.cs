﻿using System;

namespace Home.Gift
{
    [Serializable]
    public class Gift
    {
        public GiftType giftType;
        public int value;
        public ItemBooster itemBoosterType;
    }
    public enum GiftType
    {
        Coin,
        Item, 
        SkinBall,
        SkinCharacter,
        RemoveAds
    }
}
public enum ItemBooster
{
    Bomb,
    DoubleGun,
    Counter,
    Saw
}

public enum SkinBall
{
    Default,
    Dumbbell,
    Cup,
    Mobile,
    Basketball,
    Darts,
    Frog,
    Donut,
    Pig,
    Boomerang,
    Globe,
    Pineapple,
    Billiard
}

public enum SkinCharacter
{
    Default,
    Fixer,
    Magician,
    Doctor,
    Gym,
    Guard,
    Engineer,
    Hawaii,
    Hiphop,
    Gentleman,
    Farmer
}

public enum SkinTheme
{
    Default,
    Theme2,
    Theme3,
    Theme4
}
