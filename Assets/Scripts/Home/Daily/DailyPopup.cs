﻿using System;
using System.Collections.Generic;
using Do.Scripts.Tools.Audio;
using Do.Scripts.Tools.Collection_Effect;
using Do.Scripts.Tools.Dialog;
using Do.Scripts.Tools.Language;
using Do.Scripts.Tools.Other;
using Home.Gift;
using Home.Main;
using Loading;
using Popup;
using TMPro;
using UnityEngine;

namespace Home.Daily
{
    public class DailyPopup : MonoBehaviour
    {
        private const string KEY_DAILY = "DAILY_REWARD";
        private const string KEY_TARGET_COLLECT = "DAILY_TARGET_COLLECT";
        private const string KEY_CURRENT_COLLECTED = "DAILY_CURRENT_COLLECTED";

        private const string COUNT_DOWN_ENG = "NEW GIFT IN: ";
        private const string COUNT_DOWN_VIE = "QUÀ MỚI SAU: ";

        [SerializeField] private HomeManager homeManager;
        [Header("DATA")]
        [SerializeField] private List<Gift.Gift> gifts;
        private Gift.Gift _gift;

        private int _targetCollect, _currentCollected;
        private DateTime _dateTime;

        private int TargetCollect
        {
            get => _targetCollect;
            set
            {
                _targetCollect = value;
                MyPref.SetInt(KEY_TARGET_COLLECT, _targetCollect);
            }
        }
        
        private int CurrentCollected
        {
            get => _currentCollected;
            set
            {
                _currentCollected = value;
                MyPref.SetInt(KEY_CURRENT_COLLECTED, _currentCollected);
            }
        }

        [Header("UI")] [SerializeField] private List<DailyItem> dailyItems;
        [SerializeField] private GameObject btnClaim, btnClaimX3, btn_Claimed;
        [SerializeField] private RectTransform selectedBounder;
        [SerializeField] private Vector2 normalSize, specialSize;
        
        [SerializeField] private GameObject countDown;
        [SerializeField] private TextMeshProUGUI countDownText;
        [SerializeField] private GameObject notification;

        private float _countDownTime;
        private string _localTitle;

        public void Initialize()
        {
            for (var i = 0; i < gifts.Count; i++)
            {
                dailyItems[i].SetData(gifts[i]);
            }
            _targetCollect = MyPref.GetInt(KEY_TARGET_COLLECT, 1);
            _currentCollected = MyPref.GetInt(KEY_CURRENT_COLLECTED);
            CheckTime();
        }

        private void OnEnable()
        {
            CheckTime();
            for (var i = 0; i < dailyItems.Count; i++)
            {
                dailyItems[i].SetState(i < CurrentCollected);
                if (i != CurrentCollected)
                    continue;
                if (i < TargetCollect)
                    SetSelectedBounder(true, i);
                else
                    SetSelectedBounder(false);
            }
            _localTitle = LanguageManager.Instance.Language == Language.Eng ? COUNT_DOWN_ENG : COUNT_DOWN_VIE;
        }

        private void CheckTime()
        {
            var dateTimeNow = DateTime.Now;
            if (!MyPref.HasKey(KEY_DAILY))
            {
                var tick = dateTimeNow.Ticks;
                MyPref.SetString(KEY_DAILY, tick.ToString());
                notification.SetActive(true);
                return;
            }

            if (CurrentCollected >= dailyItems.Count)
                return;
            var saveTime = Convert.ToInt64(MyPref.GetString(KEY_DAILY));
            _dateTime = DateTime.FromBinary(saveTime);
            if (CurrentCollected < TargetCollect)
            {
                UnActiveCountDown();
                notification.SetActive(true);
                return;
            }

            var isNewDay = false;
            if (dateTimeNow.Year > _dateTime.Year)
                isNewDay = true;
            else if (dateTimeNow.Month > _dateTime.Month)
                isNewDay = true;
            else if (dateTimeNow.Day > _dateTime.Day)
                isNewDay = true;
            if (isNewDay)
            {
                TargetCollect++;
                UnActiveCountDown();
                notification.SetActive(true);
            }
            else
            {
                ActiveCountDown();
                notification.SetActive(false);
            }
        }

        private void ActiveCountDown()
        {
            if (!countDown.activeSelf)
                countDown.SetActive(true);
            var dateTime = DateTime.Now;
            _countDownTime = 3600 * (24 - dateTime.Hour) - (dateTime.Second + dateTime.Minute * 60);
        }

        private void UnActiveCountDown()
        {
            if (!countDown.activeSelf)
                return;
            countDown.SetActive(false);
            _countDownTime = 0;
            SetSelectedBounder(true, CurrentCollected);
        }

        private void ShowTime()
        {
            var currentTime = TimeSpan.FromSeconds(Mathf.RoundToInt(_countDownTime));
            countDownText.text = _localTitle + "<color=\"yellow\">" + currentTime;
        }


        private void Update()
        {
            if (_countDownTime <= 0)
                return;
            _countDownTime -= Time.deltaTime;
            ShowTime();
            if (_countDownTime > 0)
                return;
            TargetCollect++;
            UnActiveCountDown();
            notification.SetActive(true);
        }

        private void SetSelectedBounder(bool isActive, int currentItem = 0)
        {
            if (isActive)
            {
                selectedBounder.gameObject.SetActive(true);
                selectedBounder.anchoredPosition = dailyItems[currentItem].AnchoredPosition;
                selectedBounder.sizeDelta = currentItem < dailyItems.Count ? normalSize : specialSize;
                ActiveButtonClaim(true);
            }
            else
            {
                selectedBounder.gameObject.SetActive(false);
                ActiveButtonClaim(false);
            }
        }

        private void ActiveButtonClaim(bool isActive)
        {
            btnClaim.SetActive(isActive);
            btnClaimX3.SetActive(isActive);
            btn_Claimed.SetActive(!isActive);
        }

        private void UI_Claim_Reward()
        {
            homeManager.UnActiveRay();
            var value = _gift.value;
            switch (_gift.giftType)
            {
                case GiftType.Coin:
                    var pos = dailyItems[CurrentCollected].transform.position;
                    RootManager.Instance.Coin += value;
                    DialogManager.Instance.Reward_Dialog(_gift, delegate
                    {
                        homeManager.ActiveRay();
                        gameObject.SetActive(false);
                        CollectionEffectManger.Instance.PlayEffectCoin(pos,
                            homeManager.CoinPosition, delegate
                            {
                                homeManager.ShowMoreCoin(value / 5);
                            });
                    });
                    break;
                case GiftType.Item:
                    RootManager.Instance.SetItem(_gift.itemBoosterType, value);
                    DialogManager.Instance.Reward_Dialog(_gift , delegate
                    {
                        homeManager.ActiveRay();
                        gameObject.SetActive(false);
                    });
                    break;
                case GiftType.SkinBall:
                    PopupManager.Instance.OpenSkin((SkinBall) _gift.value);
                    DialogManager.Instance.Reward_Dialog(_gift , delegate
                    {
                        homeManager.ActiveRay();
                        gameObject.SetActive(false);
                        homeManager.Open_Skin_Popup(0);
                    });
                    break;
                case GiftType.SkinCharacter:
                    PopupManager.Instance.OpenSkin((SkinCharacter) _gift.value);
                    DialogManager.Instance.Reward_Dialog(_gift , delegate
                    {
                        homeManager.ActiveRay();
                        gameObject.SetActive(false);
                        homeManager.Open_Skin_Popup(1);
                    });
                    break;
            }
            SetSelectedBounder(false);
            ActiveCountDown();
            notification.SetActive(false);
            dailyItems[CurrentCollected].SetState(true);
            CurrentCollected++;
            _dateTime = DateTime.Now;
            var tick = _dateTime.Ticks;
            MyPref.SetString(KEY_DAILY, tick.ToString());
        }

        #region EventButton

        public void Button_Exit()
        {
            AudioManager.Instance.Play(SoundType.Click);
            gameObject.SetActive(false);
        }

        public void Button_Claim()
        {
            AudioManager.Instance.Play(SoundType.Click);
            var gift = gifts[CurrentCollected];
            _gift = new Gift.Gift
            {
                giftType = gift.giftType,
                itemBoosterType =  gift.itemBoosterType,
                value = gift.value
            };
            UI_Claim_Reward();
        }

        public void Button_Claim_X3()
        {
            AudioManager.Instance.Play(SoundType.Click);
            var gift = gifts[CurrentCollected];
            _gift = new Gift.Gift
            {
                giftType = gift.giftType,
                itemBoosterType =  gift.itemBoosterType,
                value = gift.giftType == GiftType.Coin ? gift.value * 3 : gift.value
            };
            UI_Claim_Reward();
        }

        #endregion
    }
}
