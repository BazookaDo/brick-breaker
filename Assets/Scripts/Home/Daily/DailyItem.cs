﻿using TMPro;
using UnityEngine;

namespace Home.Daily
{
    public class DailyItem : MonoBehaviour
    {
        [Header("UI")] 
        [SerializeField] private TextMeshProUGUI valueText;
        [SerializeField] private RectTransform myRect;
        [SerializeField] private GameObject done;

        public Vector2 AnchoredPosition => myRect.anchoredPosition;

        public void SetData(Gift.Gift gift)
        {
            if (valueText != null)
                valueText.text = gift.value.ToString();
        }
        public void SetState(bool isDone)
        {
            if (isDone)
            {
                if (!done.activeSelf)
                    done.SetActive(true);
            }
            else
            {
                if (done.activeSelf)
                    done.SetActive(false);
            }
        }
    }
}
