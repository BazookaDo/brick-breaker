﻿using System.Collections.Generic;
using UnityEngine;

namespace Home.LevelRescue
{
    [CreateAssetMenu(fileName = "Data Rescue", order = 1)]
    public class DataRescue : ScriptableObject
    {
        public List<LevelRescueData> listLevelRescueData;
    }
}
