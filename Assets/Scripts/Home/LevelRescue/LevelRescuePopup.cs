﻿using System.Collections.Generic;
using System.Linq;
using Do.Scripts.Tools.Audio;
using Do.Scripts.Tools.Dialog;
using Do.Scripts.Tools.Language;
using UnityEngine;
using Home.Main;
using InGame;
using Loading;
using Other;

namespace Home.LevelRescue
{
    public class LevelRescuePopup : MonoBehaviour
    {
        private List<LevelRescueData> listLevelRescueData = new List<LevelRescueData>();
        
        [Header("UI")]
        [SerializeField] private HomeManager homeManager;
        [SerializeField] private List<LevelRescueItem> listLevelRescueItem;
        [SerializeField] private RectTransform character;
        [SerializeField] private RectTransform content;
        [SerializeField] private Material bg;
        [SerializeField] private int target;

        public Callback InBack;
        private void OnEnable()
        {
	        bg.mainTextureOffset = Vector2.one;
	        content.anchoredPosition = Vector2.zero;
        }
        private void Start()
        {
	        content.sizeDelta = new Vector2(1080, 1557 * (content.childCount - 1) + 100);
	        var position = listLevelRescueItem[RootManager.Instance.CurrentLevelRescueOpen].Position;
	        var localScale = character.localScale;
	        if (character.anchoredPosition.x > 0)
	        {
		        
		        localScale.x = -Mathf.Abs(localScale.x);
	        }
	        else
	        {
		        localScale.x = Mathf.Abs(localScale.x);
	        }
	        character.position = position;
	        position = character.anchoredPosition;
	        position.y += 50;
	        character.anchoredPosition = position;
	        character.localScale = localScale;
        }

        private void OnDisable()
        {
            target = TargetScroll();
        }

	    #region DATA
	    public void Initialize()
	    {
		    listLevelRescueData = RootManager.Instance.ListLevelRescueData.ToList();
		    var count = listLevelRescueData.Count;
		    for (var i = 0; i < count; i++)
		    {
			    listLevelRescueItem[i].SetDataLevel(this, listLevelRescueData[i]);
		    }
	    }
	    
	    #endregion
	    public void SelectedBounderFollowParent(RectTransform parent, bool active)
	    {
		    
	    }

	    private int TargetScroll()
	    {
		    // for (var i = 0; i < _listCells.Count; i++)
		    // {
			   //  var dataCell = _listCells[i];
			   //  if (dataCell.listDataRescue.Any(levelData => levelData.levelState == LevelState.Open))
				  //   return i;
		    // }
		    return 0;
	    }

	    public void UpdateBackground()
	    {
		    var height = content.sizeDelta.y;
		    var duration = content.anchoredPosition.y;
		    var size = bg.mainTextureOffset;
		    size.y = 1 - duration / height;
		    bg.mainTextureOffset = size;
	    }

	    public void PlayLevel(LevelRescueData levelRescueData)
	    {
		    if (levelRescueData.levelState == LevelRescueState.Lock || levelRescueData.level > RootManager.Instance.CurrentLevelRescueOpen)
			    return;
		    Utils.GameMode = GameMode.Rescue;
		    Utils.CurrentLevel = levelRescueData.level;
		    var title = LanguageManager.Instance.Language == Language.Eng ? "LEVEL " : "CẤP ĐỘ ";
		    DialogManager.Instance.Play_Dialog(title + (Utils.CurrentLevel + 1),
			    delegate { RootManager.Instance.PlayGame(); }, delegate { RootManager.Instance.PlayGame(); }, null);
	    }

	    public void Btn_Back()
	    {
		    AudioManager.Instance.Play(SoundType.Click);
		    gameObject.SetActive(false);
		    InBack?.Invoke();
		    InBack = null;
	    }
    }
}
