﻿using System;
using System.Collections.Generic;
using Do.Scripts.Tools.Audio;
using Loading;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Home.LevelRescue
{
    public class LevelRescueItem : MonoBehaviour
    {
        private LevelRescuePopup _levelRescuePopup;
        [SerializeField]
        private LevelRescueData _levelRescueData;

        [Header("UI")] 
        [SerializeField] private TextMeshProUGUI levelText;
        [SerializeField] private TextMeshProUGUI targetText;
        [SerializeField] private GameObject lockLevel;
        [SerializeField] private Image icon;
        [SerializeField] private List<Sprite> iconCoinStates, iconInsertStates;
        private readonly Color colorLock = new Color(1, 1, 1, 0.5f);
        private readonly Color colorUnlock = new Color(1, 1, 1, 1f);
        public Vector2 Position => transform.position;

        public void SetDataLevel(LevelRescuePopup levelRescuePopup, LevelRescueData levelRescueData)
        {
            _levelRescuePopup = levelRescuePopup;
            _levelRescueData = levelRescueData;
            UpdateUI();
        }

        private void UpdateUI()
        {
            var levelState = _levelRescueData.levelState;
            if (_levelRescueData.gift.giftType == LevelRescueGiftType.Coin)
            {
                if (levelState == LevelRescueState.Pass)
                {
                    icon.sprite = iconCoinStates[2];
                    levelText.color = colorUnlock;
                }
                else if (levelState == LevelRescueState.Unlock)
                {
                    if (_levelRescueData.level == RootManager.Instance.CurrentLevelRescueOpen)
                    {
                        icon.sprite = iconCoinStates[1];
                        levelText.color = colorUnlock;
                    }
                    else
                    {
                        icon.sprite = iconCoinStates[0];
                        levelText.color = colorLock;
                    }
                }
                else
                {
                    icon.sprite = iconCoinStates[0];
                    levelText.color = colorLock;
                }
                levelText.gameObject.SetActive(true);
                levelText.text = (_levelRescueData.level + 1).ToString();
            }
            else
            {
                icon.sprite = iconInsertStates[levelState == LevelRescueState.Pass ? 0 : 1];
                levelText.gameObject.SetActive(false);
            }
            if (levelState == LevelRescueState.Lock)
            {
                lockLevel.SetActive(true);
                targetText.text = "LV " + (_levelRescueData.targetOpen + 1);
                targetText.color = colorLock;
            }
            icon.SetNativeSize();
        }

        public void Btn_Play()
        {
            AudioManager.Instance.Play(SoundType.Click);
            _levelRescuePopup.PlayLevel(_levelRescueData);
        }
    }

    [Serializable]
    public class LevelRescueData
    {
        public int level;
        public LevelRescueState levelState;
        public int targetOpen;
        public LevelRescueGift gift;
    }
    
    [Serializable]
    public class LevelRescueGift
    {
        public LevelRescueGiftType giftType;
        public int giftValue;
    }

    public enum LevelRescueGiftType
    {
        Coin,
        Insert
    }

    public enum LevelRescueState
    {
        Lock,
        Unlock,
        Pass
    }
}
