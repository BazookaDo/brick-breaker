﻿using System;
using System.Collections.Generic;
using Do.Scripts.Tools.Audio;
using Do.Scripts.Tools.Collection_Effect;
using Do.Scripts.Tools.Dialog;
using Do.Scripts.Tools.Language;
using Do.Scripts.Tools.Other;
using Home.Gift;
using Home.Main;
using InGame;
using Loading;
using Popup;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Home.Spin
{
    public class SpinPopup : MonoBehaviour
    {
        private const string KEY_SPIN = "KEY_SPIN";
        private const string KEY_TIME_SPIN = "KEY_TIME_SPIN";
        
        [SerializeField] private HomeManager homeManager;
        [Header("DATA")]
        [SerializeField] private List<Gift.Gift> gifts;
        [Space(20)]
        [SerializeField] private int minuteCountDown = 60;
        [SerializeField] private int spinCount;
        [SerializeField] private float speed;
        [SerializeField] private float minAccelerate, maxAccelerate;
        
        private float _originEuler;
        private float _accelerateSpeed, _targetAccelerate;
        private Accelerate _accelerate;

        [SerializeField]
        private Gift.Gift _gift;

        [Header("UI")] 
        [SerializeField] private List<SpinItem> spinItems;
        [SerializeField] private RectTransform spinRect;
        [SerializeField] private GameObject btnSpin, btnSpinAds, btnSpinning;
        
        [SerializeField] private GameObject countDown;
        [SerializeField] private TextMeshProUGUI countDownText;
        [SerializeField] private GameObject notification;
        
        private const string COUNT_DOWN_ENG = "FREE SPIN IN: ";
        private const string COUNT_DOWN_VIE = "QUAY MIỄN PHÍ SAU: ";

        public Callback InBack;

        private bool _IsSpinning, _spin;
        private float _time;
        private string _localTitle;
        private int _originGift;

        private float Current { get; set; }
        private float Target { get; set; }

        public void Initialize()
        {
            _spin = !MyPref.HasKey(KEY_SPIN) || MyPref.GetBool(KEY_SPIN); 
            for (var i = 0; i < gifts.Count; i++)
            {
                spinItems[i].SetData(gifts[i]);
            }
            CheckTime();
        }

        private void OnEnable()
        {
            spinRect.eulerAngles = Vector3.zero;
            _localTitle = LanguageManager.Instance.Language == Language.Eng ? COUNT_DOWN_ENG : COUNT_DOWN_VIE;
            CheckTime();
        }

        private void CheckTime()
        {
            if (!_spin)
            {
                var now = DateTime.Now.Ticks;
                var old = Convert.ToInt64(MyPref.GetString(KEY_TIME_SPIN));
                var span = new TimeSpan(now - old);
                _time = minuteCountDown * 60 - (int) span.TotalSeconds;
                if (_time <= 0)
                {
                    _time = 0;
                    _spin = true;
                    MyPref.SetBool(KEY_SPIN, _spin);
                }
            }
            ActiveButton(_spin);
        }

        private void ActiveButton(bool isActive)
        {
            if (notification.activeSelf != isActive)
                notification.SetActive(isActive);
            if (btnSpin.activeSelf != isActive)
                btnSpin.SetActive(isActive);
            if (btnSpinAds.activeSelf == isActive)
                btnSpinAds.SetActive(!isActive);
            if (countDown.activeSelf == isActive)
                countDown.SetActive(!isActive);
            if (btnSpinning.activeSelf)
                btnSpinning.SetActive(false);
        }

        private void ShowTime()
        {
            var currentTime = TimeSpan.FromSeconds(Mathf.RoundToInt(_time));
            countDownText.text = _localTitle + "<color=\"yellow\">" + currentTime;
        }

        private void Update()
        {
            var deltaTime = Time.deltaTime;
            if (!_spin)
            {
                _time -= deltaTime;
                ShowTime();
                if (_time <= 0)
                {
                    _time = 0;
                    _spin = true;
                    MyPref.SetBool(KEY_SPIN, _spin);
                    if (_IsSpinning)
                    {
                        notification.SetActive(true);
                        countDown.SetActive(false);
                    }
                    else
                        ActiveButton(_spin);
                }
            }
            
            if (!_IsSpinning)
                return;
            if (_accelerate == Accelerate.Increase)
            {
                if (_accelerateSpeed < maxAccelerate)
                    _accelerateSpeed += 10 * deltaTime;
                if (_accelerateSpeed >= maxAccelerate)
                {
                    _accelerateSpeed = maxAccelerate;
                    _accelerate = Accelerate.Loop;
                    _targetAccelerate = Current - _originEuler;
                }
            }
            else if (_accelerate == Accelerate.Reduce)
            {
                if (_accelerateSpeed > minAccelerate)
                    _accelerateSpeed -= 10 * deltaTime;
            }
            if (Current < Target)
                Current += speed * _accelerateSpeed * deltaTime;
            if (Target - Current <= _targetAccelerate)
                _accelerate = Accelerate.Reduce;
                
            var Euler = Vector3.zero;
            Euler.z = Current;
            spinRect.eulerAngles = Euler;
            if (Current < Target) 
                return;
            btnSpinning.SetActive(false);
            if (_spin)
                btnSpin.SetActive(true);
            else
                btnSpinAds.SetActive(true);
            _IsSpinning = false;
            Current -= (spinCount + 1) * 360;
            Euler.z = Current;
            spinRect.eulerAngles = Euler;
            CollectGift();
        }

        private void CollectGift()
        {
            var value = _gift.value;
            switch (_gift.giftType)
            {
                case GiftType.Coin:
                    var pos = spinItems[_originGift].transform.position;
                    RootManager.Instance.Coin += value;
                    homeManager.UnActiveRay();
                    DialogManager.Instance.Reward_Dialog(_gift,
                        delegate
                        {
                            homeManager.ActiveRay();
                            CollectionEffectManger.Instance.PlayEffectCoin(pos,
                                homeManager.CoinPosition, delegate
                                {
                                    homeManager.ShowMoreCoin(value / 5);
                                });
                        });
                    break;
            }
        }

        public void Button_Spin()
        {
            AudioManager.Instance.Play(SoundType.Click);
            if (_IsSpinning || !_spin)
                return;
            btnSpin.SetActive(false);
            btnSpinning.SetActive(true);
            _IsSpinning = true;
            _accelerateSpeed = minAccelerate;
            _accelerate = Accelerate.Increase;
            Current = spinRect.eulerAngles.z;
            _originEuler = Current;
            _originGift = Random.Range(0, gifts.Count);
            _gift = gifts[_originGift];
            var between = _originGift > 0 ? _originGift * 45 : 360;
            Target = Random.Range(between - 10, between + 10) + 360 * spinCount;
            
            _spin = false;
            MyPref.SetBool(KEY_SPIN, _spin);
            var now = DateTime.Now.Ticks;
            MyPref.SetString(KEY_TIME_SPIN, now.ToString());
            notification.SetActive(false);
            countDown.SetActive(true);
            _time = minuteCountDown * 60;
            ShowTime();
        }

        public void Button_Spin_Ads()
        {
            AudioManager.Instance.Play(SoundType.Click);
            if (_IsSpinning || _spin)
                return;
            btnSpinAds.SetActive(false);
            btnSpinning.SetActive(true);
            _IsSpinning = true;
            _accelerateSpeed = minAccelerate;
            _accelerate = Accelerate.Increase;
            Current = spinRect.eulerAngles.z;
            _originEuler = Current;
            _originGift = Random.Range(0, gifts.Count);
            _gift = gifts[_originGift];
            var between = _originGift > 0 ? _originGift * 45 : 360;
            Target = Random.Range(between - 10, between + 10) + 360 * spinCount;
        }

        public void Button_Back()
        {
            AudioManager.Instance.Play(SoundType.Click);
            if (_IsSpinning)
                return;
            gameObject.SetActive(false);
            InBack?.Invoke();
            InBack = null;
        }
        
        private enum Accelerate
        {
            Increase,
            Loop,
            Reduce
        }
    }
}
