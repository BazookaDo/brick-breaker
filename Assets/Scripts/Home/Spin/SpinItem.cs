﻿using TMPro;
using UnityEngine;

namespace Home.Spin
{
    public class SpinItem : MonoBehaviour
    {
        [Header("UI")]
        [SerializeField] private TextMeshProUGUI valueText;

        public void SetData(Gift.Gift gift)
        {
            valueText.text = gift.value.ToString();
        }
    }
}
