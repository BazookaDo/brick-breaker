﻿using System;
using System.Collections;
using System.Collections.Generic;
using Do.Scripts.Tools.Other;
using UnityEngine;
using UnityEngine.UI;

namespace Home.Spin
{
    public class SpinLight : MonoBehaviour
    {
        [Range(0.1f, 1f)] [SerializeField] private float durationStepAuto = 0.25f;
        [Range(1, 5)] [SerializeField] private int countLoopAuto = 3;
        [Range(0.1f, 1f)] [SerializeField] private float durationStepSpin = 0.15f;
        [Range(3, 10)] [SerializeField] private int countLoopSpin = 5;
        [Header("UI")]
        [SerializeField] private List<Image> lamps;
        [SerializeField] private Sprite on, off;
        
        private IEnumerator _routine;
        private void OnEnable()
        {
            foreach (var lamp in lamps)
            {
                lamp.sprite = off;
            }
            _routine = Auto();
            StartCoroutine(_routine);
        }

        private IEnumerator Auto()
        {
            foreach (var lamp in lamps)
            {
                yield return Yield.GetTime(durationStepAuto);
                lamp.sprite = on;
            }

            for (var i = 0; i < countLoopAuto; i++)
            {
                yield return Yield.GetTime(durationStepAuto);
                foreach (var lamp in lamps)
                {
                    lamp.sprite = off;
                }
                yield return Yield.GetTime(durationStepAuto);
                foreach (var lamp in lamps)
                {
                    lamp.sprite = @on;
                }
            }
            yield return Yield.GetTime(durationStepAuto);
            foreach (var lamp in lamps)
            {
                lamp.sprite = off;
            }

            _routine = Auto();
            StartCoroutine(_routine);
        }

        private void OnDisable()
        {
            StopCoroutine(_routine);
            _routine = null;
        }
    }
}
