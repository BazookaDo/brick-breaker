﻿using System.Collections.Generic;
using Loading;
using UnityEngine;

namespace Home.House
{
    public class HousePopup : MonoBehaviour
    {
        [SerializeField] private List<HouseItem> houseItems;
        [SerializeField] private RectTransform selectedBounder;


        private void OnEnable()
        {
            Initialize();
        }

        public void Initialize()
        {
            for (var i = 0; i < houseItems.Count; i++)
            {
                houseItems[i].Initialize(RootManager.Instance.GetBuildingItem(i) >= 0);
            }
        }
        
        public void SetSelectedBounder(RectTransform parent)
        {
            selectedBounder.SetParent(parent);
            selectedBounder.SetSiblingIndex(1);
            selectedBounder.localPosition = Vector3.zero;
            selectedBounder.localScale = Vector3.one;
        }
    }
}
