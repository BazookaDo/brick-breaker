﻿using Do.Scripts.Tools.Audio;
using Loading;
using Popup;
using UnityEngine;

namespace Home.House
{
    public class HouseItem : MonoBehaviour
    {
        [SerializeField] private HousePopup housePopup;
        [SerializeField] private int id;
        [SerializeField] private GameObject locked;
        
        public void Initialize(bool open)
        {
            locked.SetActive(!open);
            if (RootManager.Instance.Building == id)
                housePopup.SetSelectedBounder(GetComponent<RectTransform>());
        }

        public void HandleClick()
        {
            AudioManager.Instance.Play(SoundType.Click);
            if (locked.activeSelf || RootManager.Instance.Building == id)
                return;
            RootManager.Instance.Building = id;
            housePopup.Initialize();
            PopupManager.Instance.Initialize_Building();
        }
    }
}
