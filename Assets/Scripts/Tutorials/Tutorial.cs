﻿using UnityEngine;

namespace Tutorials
{
    public class Tutorial : MonoBehaviour
    {
        public void ShowWithPosition(Vector2 target)
        {
            transform.position = target;
        }
    }
}
