﻿using Spine.Unity;
using UnityEngine;

namespace Tutorials
{
    public class TutorialStepBuilding : Tutorial
    {
        [SerializeField] private SkeletonAnimation skeletonAnimation;
        [SerializeField] private AnimationReferenceAsset anim;

        private void Start()
        {
            skeletonAnimation.AnimationState.SetAnimation(0, anim, true);
        }
    }
}
                                                                