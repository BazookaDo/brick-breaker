﻿using DG.Tweening;
using Do.Scripts.Tools.Tween;
using Spine.Unity;
using UnityEngine;
using AnimationState = Spine.AnimationState;

namespace Tutorials
{
    public class TutorialStepShoot : Tutorial
    {
        [SerializeField] private float timeAnim = 1f;
        [SerializeField] private Transform shoot, hand;
        [SerializeField] private SkeletonAnimation skeletonAnimation;
        [SerializeField] private AnimationReferenceAsset start, left, right, end;

        private AnimationState _animationState;
        private Vector3 _minEuler, _maxEuler;
        private Vector2 _minPos, _maxPos;

        private Tween _tweenShoot, _tweenHand;

        private void Start()
        {
            _animationState = skeletonAnimation.AnimationState;
            _minEuler = new Vector3(0, 0, 30);
            _maxEuler = new Vector3(0, 0, -30);
            _minPos = _maxPos = hand.position;
            _minPos.x = -2f;
            _maxPos.x = 2f;
            Animation();
        }

        private void Animation()
        {
            shoot.eulerAngles = _minEuler;
            hand.position = _minPos;
            _tweenShoot = MyTween.Instance.DoTween_Float(0.5f, 0f, delegate
            {
                _animationState.SetAnimation(0, start, false);
                _tweenShoot = MyTween.Instance.DoTween_Float(0.1f, 0f, delegate
                {
                    _animationState.SetAnimation(0, right, true);
                    _tweenHand = hand.DOMove(_maxPos, timeAnim).SetEase(Ease.Linear);
                    _tweenShoot = shoot.DORotate(_maxEuler, timeAnim).SetEase(Ease.Linear).OnComplete(delegate
                    {
                        _animationState.SetAnimation(0, left, true);
                        _tweenHand = hand.DOMove(_minPos, timeAnim).SetEase(Ease.Linear);
                        _tweenShoot = shoot.DORotate(_minEuler, timeAnim).SetEase(Ease.Linear).OnComplete(delegate
                        {
                            _animationState.SetAnimation(0, end, false);
                            _tweenShoot = MyTween.Instance.DoTween_Float(0.1f, 0f, Animation);
                        });
                    });
                });
            });
        }

        private void OnDestroy()
        {
            _tweenShoot?.Kill();
            _tweenHand?.Kill();
        }
    }
}
