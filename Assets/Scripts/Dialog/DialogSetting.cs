﻿using System;
using System.Collections.Generic;
using Do.Scripts.Tools.Audio;
using Do.Scripts.Tools.Dialog;
using Do.Scripts.Tools.Language;
using Loading;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Dialog
{
    public class DialogSetting : MonoBehaviour
    {
        [SerializeField] private Image btnSound, btnMusic;
        [SerializeField] private Sprite on, off;
        [SerializeField] private TextMeshProUGUI btnSoundText, btnMusicText;
        [SerializeField] private TextMeshProUGUI languageText;
        [SerializeField] private CanvasGroup btnRate;
        public Callback InBack;

        private void OnEnable()
        {
            ShowIconLanguage();
            CheckRate();
        }

        public void Btn_SetSound()
        {
            if (AudioManager.Instance.Sound)
            {
                AudioManager.Instance.Sound = false;
                SetOnOff(btnSound, AudioManager.Instance.Sound, btnSoundText);
            }
            else
            {
                AudioManager.Instance.Sound = true;
                SetOnOff(btnSound, AudioManager.Instance.Sound, btnSoundText);
            }
            // AudioManager.Instance.Play(SoundType.Click);
        }

        public void Btn_SetMusic()
        {
            if (AudioManager.Instance.Music)
            {
                AudioManager.Instance.Music = false;
                SetOnOff(btnMusic, AudioManager.Instance.Music, btnMusicText);
            }
            else
            {
                AudioManager.Instance.Music = true;
                SetOnOff(btnMusic, AudioManager.Instance.Music, btnMusicText);
            }
            // AudioManager.Instance.Play(SoundType.Click);
        }
        
        public void Btn_RateUs()
        {
            // AudioManager.Instance.Play(SoundType.Click);
            if (RootManager.Instance.IsRate)
                return;
            RootManager.Instance.IsRate = true;
            CheckRate();
            // DialogManager.Instance.ActiveRateDialog(CheckRate);
        }

        public void Btn_Contact()
        {
            
        }

        private void CheckRate()
        {
            if (!RootManager.Instance.IsRate)
                return;
            btnRate.alpha = 0.5f;
        }


        private void SetOnOff(Image image, bool isOn, TextMeshProUGUI text)
        {
            if (isOn)
            {
                image.sprite = on;
                image.GetComponent<RectTransform>().anchoredPosition = new Vector3(40, 0, 0);
                text.text = LanguageManager.Instance.Language == Language.Eng ? "ON" : "BẬT";
            }
            else
            {
                image.sprite = off;
                image.GetComponent<RectTransform>().anchoredPosition = new Vector3(-40, 0, 0);
                text.text = LanguageManager.Instance.Language == Language.Eng ? "OFF" : "TẮT";
            }
        }

        private void ShowIconLanguage()
        {
            SetOnOff(btnSound, AudioManager.Instance.Sound, btnSoundText);
            SetOnOff(btnMusic, AudioManager.Instance.Music, btnMusicText);
            languageText.text = LanguageManager.Instance.Language.ToString();
        }

        public void ActiveLanguageDialog()
        {
            DialogManager.Instance.Language_Dialog(ShowIconLanguage);
        }

        public void Btn_Exit()
        {
            gameObject.SetActive(false);
            InBack?.Invoke();
        }
    }
}
