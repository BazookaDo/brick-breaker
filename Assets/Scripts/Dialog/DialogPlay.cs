﻿using Do.Scripts.Tools.Dialog;
using TMPro;
using UnityEngine;

namespace Dialog
{
    public class DialogPlay : MonoBehaviour
    {
        [SerializeField] private DialogManager dialogManager;
        [SerializeField] private TextMeshProUGUI titleText;

        public void Active(string levelName)
        {
            titleText.text = levelName;
        }

        public void Button_Play()
        {
            
        }

        public void Button_Play_Ads()
        {
            
        }
    }
}
