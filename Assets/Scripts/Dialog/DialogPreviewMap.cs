﻿using System.Collections.Generic;
using InGame.Data;
using InGame.Obstacle;
using Loading;
using UnityEngine;

namespace Dialog
{
    public class DialogPreviewMap : MonoBehaviour
    {
        [SerializeField] private RectTransform root, childRoot;
        [SerializeField] private PreviewItem previewItemPrefab;
        [SerializeField] private List<Sprite> bricks, effects, items;

        private List<PreviewItem> _previewItems;
        private Vector2 _vertical, _horizontal;
        public void Initialized()
        {
            if (childRoot != null)
                Destroy(childRoot.gameObject);
            childRoot = Instantiate(new GameObject("Child"), root).AddComponent<RectTransform>();
            _previewItems = new List<PreviewItem>();
            var level = RootManager.Instance.Get_ObstacleLevel();
            _vertical = Vector2.zero;
            _horizontal = _vertical;
            foreach (var obstacleItem in level)
            {
                var pos = obstacleItem.position;
                if (pos.y < _vertical.x)
                    _vertical.x = pos.x;
                if (pos.y > _vertical.y)
                    _vertical.y = pos.y;
                if (pos.x < _horizontal.x)
                    _horizontal.x = pos.x;
                if (pos.x > _horizontal.y)
                    _horizontal.y = pos.y;
                var item = Instantiate(previewItemPrefab, childRoot);
                item.myRect.anchoredPosition = pos;
                var size = Vector2.one;
                if (obstacleItem.obstacleType == ObstacleType.Brick)
                {
                    if (obstacleItem.brickData.brickType == BrickType.Stone)
                        item.SetData(bricks[2], size, Vector3.zero);
                    else if (obstacleItem.brickData.brickType == BrickType.Triangle)
                        item.SetData(bricks[1], Vector2.one, obstacleItem.brickData.rotation);
                    else
                    {
                        size = obstacleItem.brickData.size;
                        var icon = bricks[0];
                        var effectType = obstacleItem.brickData.brickEffectType;
                        if (effectType != BrickEffectType.None)
                        {
                            switch (effectType)
                            {
                                case BrickEffectType.Freeze:
                                    item.SetData(icon, size, Vector3.zero, effects[0]);
                                    break;
                                case BrickEffectType.Fire:
                                    item.SetData(icon, size, Vector3.zero, effects[1]);
                                    break;
                                case BrickEffectType.Poison:
                                    item.SetData(icon, size, Vector3.zero, effects[2]);
                                    break;
                                case BrickEffectType.Lighting:
                                    item.SetData(icon, size, Vector3.zero, effects[3]);
                                    break;
                                case BrickEffectType.Rocket:
                                    item.SetData(icon, size, Vector3.zero, effects[4]);
                                    break;
                                default:
                                    item.SetData(icon, size, Vector3.zero);
                                    break;
                            }
                        }
                        else 
                            item.SetData(icon, size, Vector3.zero);
                    }
                }
                else
                {
                    var icon = items[(int) obstacleItem.itemData.itemType];
                    item.SetData(icon, size, Vector3.zero);
                }
                _previewItems.Add(item);
            }
            const float oldScale = 40f;
            var scale = oldScale / ((_horizontal.y - _horizontal.x) / 12);
            var distance = _vertical.y - _vertical.x; 
            var anchoredPosition = childRoot.anchoredPosition;
            anchoredPosition.y -= (_vertical.y - distance / 2) * scale;
            childRoot.anchoredPosition = anchoredPosition;
            foreach (var item in _previewItems)
            {
                item.Scale(scale);
            }
        }

        public void Show(Vector2 anchoredPosition)
        {
            gameObject.SetActive(true);
            root.anchoredPosition = anchoredPosition;
        }
    }
}
