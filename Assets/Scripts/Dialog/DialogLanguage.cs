﻿using Do.Scripts.Tools.Language;
using UnityEngine;

namespace Dialog
{
    public class DialogLanguage : MonoBehaviour
    {

        [SerializeField] private RectTransform selected;

        public void VieOn()
        {
            selected.anchoredPosition = new Vector2(-150, 100);
        }
        public void EngOn()
        {
            selected.anchoredPosition = new Vector2(150, 100);
        }

        public void SetLanguage_Eng()
        {
            LanguageManager.Instance.SetLanguage_English();
            EngOn();
        }

        public void SetLanguage_Vie()
        {
            LanguageManager.Instance.SetLanguage_Vietnamese();
            VieOn();
        }
    }
}
