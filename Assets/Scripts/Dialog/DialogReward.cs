﻿using System.Collections.Generic;
using Home.Gift;
using Spine.Unity;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Dialog
{
    public class DialogReward : MonoBehaviour
    {
        [SerializeField] private float convertSize = 150;
        [SerializeField] private Sprite coin, removeAds;
        [SerializeField] private List<Sprite> boosters;
        [SerializeField] private List<Sprite> balls;
        [SerializeField] private List<string> characters;

        [Header("UI")] 
        [SerializeField] private SkeletonGraphic anim;
        [SerializeField] private RectTransform iconRect;
        [SerializeField] private Image icon;
        [SerializeField] private TextMeshProUGUI valueText;

        private void UnActiveAll()
        {
            icon.gameObject.SetActive(false);
            anim.gameObject.SetActive(false);
            valueText.gameObject.SetActive(false);
        }

        public void Active(Gift gift)
        {
            UnActiveAll();
            var skip = false;
            switch (gift.giftType)
            {
                case GiftType.Coin:
                    icon.gameObject.SetActive(true);
                    icon.sprite = coin;
                    valueText.gameObject.SetActive(true);
                    valueText.text = gift.value.ToString();
                    break;
                case GiftType.RemoveAds:
                    icon.gameObject.SetActive(true);
                    icon.sprite = removeAds;
                    break;
                case GiftType.Item:
                    icon.gameObject.SetActive(true);
                    icon.sprite = boosters[(int) gift.itemBoosterType];
                    valueText.gameObject.SetActive(true);
                    valueText.text = gift.value.ToString();
                    break;
                case GiftType.SkinBall:
                    icon.gameObject.SetActive(true);
                    icon.sprite = balls[gift.value];
                    break;
                case GiftType.SkinCharacter:
                    skip = true;
                    anim.gameObject.SetActive(true);
                    anim.initialSkinName = characters[gift.value];
                    anim.Initialize(true);
                    break;
            }

            if (skip)
                return;
            icon.SetNativeSize();
            var size = iconRect.sizeDelta;
            if (size.x > size.y)
            {
                var ratio = convertSize / size.x;
                size.x = convertSize;
                size.y *= ratio;
            }
            else
            {
                var ratio = convertSize / size.y;
                size.y = convertSize;
                size.x *= ratio;
            }
            iconRect.sizeDelta = size;
        }
        public void Active(List<Gift> gifts)
        {
            UnActiveAll();
//            valueText.text = gift.value.ToString();
        }
    }
}
