﻿using UnityEngine;
using UnityEngine.UI;

namespace Dialog
{
    public class PreviewItem : MonoBehaviour
    {
        public RectTransform myRect;
        [SerializeField] private Image iconImage, effectImage;
        [SerializeField] private RectTransform iconRect, effectRect;
        
        public void SetData(Sprite icon, Vector2 size, Vector3 rotation, Sprite effect = null)
        {
            iconImage.sprite = icon;
            iconRect.sizeDelta = size;
            Debug.Log(size);
            iconRect.eulerAngles = rotation;
            if (effect == null) 
                return;
            effectImage.gameObject.SetActive(true);
            effectImage.sprite = effect;
            effectRect.sizeDelta = Vector2.one;
        }

        public void Scale(float scale)
        {
            myRect.anchoredPosition *= scale;
            iconRect.sizeDelta *= scale;
            effectRect.sizeDelta *= scale;
        }
    }
}
