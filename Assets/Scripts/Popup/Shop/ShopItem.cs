﻿using System;
using System.Collections.Generic;
using Home.Gift;
using TMPro;
using UnityEngine;

namespace Popup.Shop
{
    public class ShopItem : MonoBehaviour
    {
        [Header("DATA")]
        public ShopItemData shopItemData;
        
        [Header("UI")]
        [SerializeField] private ShopPopup shopPopup;

        [SerializeField] private List<TextMeshProUGUI> valueTexts;


        public void SetData()
        {
            for (var i = 0; i < shopItemData.gifts.Count; i++)
            {
                var gift = shopItemData.gifts[i];
                var giftType = gift.giftType;
                if (giftType == GiftType.Coin || giftType == GiftType.Item)
                {
                    valueTexts[i].text = "+" + gift.value;
                }
                else
                {
                    
                }
            }
        }

        public void Btn_Buy()
        {
            shopPopup.Handle_Buy(shopItemData, transform.position);
        }
    }

    [Serializable]
    public class ShopItemData
    {
        public List<Gift> gifts;
    }
}
