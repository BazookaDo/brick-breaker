﻿using System.Collections.Generic;
using Do.Scripts.Tools.Audio;
using Do.Scripts.Tools.Collection_Effect;
using Do.Scripts.Tools.Dialog;
using Home.Gift;
using Home.Main;
using Loading;
using UnityEngine;

namespace Popup.Shop
{
    public class ShopPopup : MonoBehaviour
    {
        [SerializeField] private PopupManager popupManager;
        [SerializeField] private List<ShopItem> shopItems;
        [SerializeField] private RectTransform root;

        public void Initialize()
        {
            foreach (var shopItem in shopItems)
            {
                shopItem.SetData();
            }
        }

        private void OnEnable()
        {
            if (RootManager.Instance.IsRemoveAds && shopItems[0].gameObject.activeSelf)
            {
                shopItems[0].gameObject.SetActive(false);
                root.sizeDelta = new Vector2(1080, 1960);
            }

            root.anchoredPosition = new Vector2(0, 745);
        }


        public void Handle_Buy(ShopItemData shopItemData, Vector2 position)
        {
            AudioManager.Instance.Play(SoundType.Click);
            foreach (var gift in shopItemData.gifts)
            {
                switch (gift.giftType)
                {
                    case GiftType.RemoveAds:
                        RootManager.Instance.IsRemoveAds = true;
                        shopItems[0].gameObject.SetActive(false);
                        root.sizeDelta = new Vector2(1080, 1960);
                        root.anchoredPosition = new Vector2(0, 745);
                        break;
                    case GiftType.Coin:
                        RootManager.Instance.Coin += gift.value;
                        CollectionEffectManger.Instance.PlayEffectCoin(position,
                            popupManager.CoinPosition, delegate
                            {
                                if (popupManager.InfoActive)
                                    popupManager.ShowMoreCoin(gift.value / 5);
                                else
                                    HomeManager.Instance.ShowMoreCoin(gift.value / 5);
                            });
                        break;
                    case GiftType.Item:
                        break;
                    case GiftType.SkinBall:
                        break;
                    case GiftType.SkinCharacter:
                        break;
                }
            }
        }
    }
}
