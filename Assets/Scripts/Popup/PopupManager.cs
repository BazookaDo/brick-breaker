﻿using System;
using Do.Scripts.Tools.Audio;
using Home.Main;
using Loading;
using Popup.Building;
using Popup.Shop;
using Popup.Skin;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Popup
{
    public class PopupManager : MonoBehaviour
    {
        public static PopupManager Instance { private set; get; }
        [SerializeField] private GraphicRaycaster graphicRay;
        
        [Header("Base")]
        [SerializeField] private GameObject popupCamera;
        [SerializeField] private GameObject canvas;
        [SerializeField] private GameObject infoGroup;
        [SerializeField] private GameObject background;
        [SerializeField] private TextMeshProUGUI coinText;
        [SerializeField] private TextMeshProUGUI starText;
        [SerializeField] private Transform coinTarget, starTarget;
        [SerializeField] private Button btnBack;
        private int _currentCoin, _currentStar;

        public Vector2 CoinPosition => coinTarget.position;
        public Vector2 StarPosition => starTarget.position;

        public bool InfoActive => infoGroup.activeSelf;

        [Header("Building")] 
        [SerializeField] private BuildingPopup buildingPopup;
        
        [Header("Shop")] 
        [SerializeField] private ShopPopup shopPopup;
        private Callback _shopCallback;

        [Header("Skin")]
        [SerializeField] private SkinPopup skinPopup;
        private Callback _skinCallback;
        private void Awake()
        {
            if (Instance != null)
                return;
            Instance = this;
            if (canvas.activeSelf)
                canvas.SetActive(false);
        }

        private void Start()
        {
            LoadingManager.Instance.AddCallBackLoadData(() =>
            {
                buildingPopup.Initialize();
                shopPopup.Initialize();
                skinPopup.Initialize();
            });
        }
        public void ActiveRay()
        {
            graphicRay.enabled = true;
        }

        public void UnActiveRay()
        {
            graphicRay.enabled = false;
        }

        #region Info

        public void ShowCoin()
        {
            _currentCoin = RootManager.Instance.Coin;
            coinText.text = _currentCoin.ToString();
            if (infoGroup.activeSelf)
                return;
            if (HomeManager.Instance != null)
                HomeManager.Instance.ShowCoin();
        }

        public void ShowMoreCoin(int value)
        {
            _currentCoin += value;
            if (_currentCoin > RootManager.Instance.Coin)
                _currentCoin = RootManager.Instance.Coin;
            coinText.text = _currentCoin.ToString();
        }

        public void ShowStar()
        {
            _currentStar = RootManager.Instance.Star;
            starText.text = _currentStar.ToString();
            if (infoGroup.activeSelf)
                return;
            if (HomeManager.Instance != null)
                HomeManager.Instance.ShowStar();
        }

        public void ShowMoreStar(int value)
        {
            _currentStar += value;
            if (_currentStar > RootManager.Instance.Star)
                _currentStar = RootManager.Instance.Star;
            starText.text = _currentStar.ToString();
        }

        public void Btn_Shop()
        {
            AudioManager.Instance.Play(SoundType.Click);
            if (IsShopShow)
                return;
            shopPopup.gameObject.SetActive(true);
            skinPopup.gameObject.SetActive(false);
            btnBack.onClick.RemoveAllListeners();
            btnBack.onClick.AddListener(() =>
            {
                shopPopup.gameObject.SetActive(false);
                skinPopup.gameObject.SetActive(true);
                btnBack.onClick.RemoveAllListeners();
                btnBack.onClick.AddListener(() =>
                {
                    _skinCallback?.Invoke();
                    Skin_UnActive();
                });
            });
        }

        #endregion

        private void ActiveBase(bool info)
        {
            popupCamera.SetActive(true);
            canvas.SetActive(true);
            infoGroup.SetActive(info);
            if (!info)
                return;
            ShowCoin();
            ShowStar();
        }

        private void UnActiveBase()
        {
            popupCamera.SetActive(false);
            canvas.SetActive(false);
            if (infoGroup.activeSelf)
                infoGroup.SetActive(false);
            btnBack.onClick.RemoveAllListeners();
        }

        #region Building

        public bool IsBuildingShow => buildingPopup.gameObject.activeSelf;

        public void Building_Popup(bool showUnlock = true, bool info = false)
        {
            ActiveBase(info);
            if (!showUnlock)
                buildingPopup.UnActiveUnlock();
            buildingPopup.gameObject.SetActive(true);
        }

        public void Building_UnActive()
        {
            buildingPopup.gameObject.SetActive(false);
            UnActiveBase();
        }

        public void Initialize_Building()
        {
            buildingPopup.Initialize();
        }

        public void Building_Open_Current(Callback callback)
        {
            buildingPopup.Open_Current(callback);
        }

        #endregion
        
        /// <summary>
        /// /////////
        /// </summary>

        #region Shop

        public bool IsShopShow => shopPopup.gameObject.activeSelf;

        public void Shop_Popup(bool info = true, Callback callback = null)
        {
            ActiveBase(info);
            background.SetActive(true);
            shopPopup.gameObject.SetActive(true);
            _shopCallback = callback;
            btnBack.onClick.AddListener(() =>
            {
                AudioManager.Instance.Play(SoundType.Click);
                Shop_UnActive();
                _shopCallback?.Invoke();
                _shopCallback = null;
            });
        }

        public void Shop_UnActive()
        {
            background.SetActive(false);
            shopPopup.gameObject.SetActive(false);
            UnActiveBase();
        }

        #endregion
        
        /// <summary>
        /// /////////
        /// </summary>

        #region Skin

        public bool IsSkinShow => skinPopup.gameObject.activeSelf;

        public void Skin_Popup(int tab = 0, bool info = true, Callback callback = null)
        {
            ActiveBase(info);
            background.SetActive(true);
            skinPopup.gameObject.SetActive(true);
            skinPopup.ActiveTab(tab);
            _skinCallback = callback;
            btnBack.onClick.AddListener(() =>
            {
                AudioManager.Instance.Play(SoundType.Click);
                Skin_UnActive();
                _skinCallback?.Invoke();
                _skinCallback = null;
            });
        }

        public void Skin_UnActive()
        {
            background.SetActive(false);
            skinPopup.gameObject.SetActive(false);
            skinPopup.ResetTab();
            UnActiveBase();
        }

        public void OpenSkin(SkinBall skinBall)
        {
            skinPopup.skinBallTab.OpenSkin(skinBall);
        }

        public void OpenSkin(SkinCharacter skinCharacter)
        {
            skinPopup.skinCharacterTab.OpenSkin(skinCharacter);
        }

        public void OpenSkin(SkinTheme skinTheme)
        {
            skinPopup.skinThemeTab.OpenSkin(skinTheme);
        }
        

        #endregion
    }
}
