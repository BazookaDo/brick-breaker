﻿using System.Collections.Generic;
using UnityEngine;

namespace Popup.Building
{
    public class BuildingRoom : MonoBehaviour
    {
        private BuildingPopup _buildingPopup;
        private BuildingLevelData _buildingLevelData;
        [SerializeField] private List<BuildingObject> buildingObjects;

        public void SetData(BuildingPopup buildingPopup, BuildingLevelData buildingLevelData)
        {
            _buildingPopup = buildingPopup;
            _buildingLevelData = buildingLevelData;
            UpdateUI();
        }

        public void UpdateUI()
        {
            var data = _buildingLevelData.listItemData;
            for (var i = 0; i < data.Count; i++)
            {
                buildingObjects[i].SetData(_buildingPopup, data[i]);
            }
        }

        public void ActiveUnlock()
        {
            foreach (var buildingObject in buildingObjects)
            {
                buildingObject.ActiveUnlock();
            }
        }

        public void UnActiveUnlock()
        {
            foreach (var buildingObject in buildingObjects)
            {
                buildingObject.UnActiveUnlock();
            }
        }

        public int CurrentId()
        {
            var listItemData = _buildingLevelData.listItemData;
            for (var i = 0; i < listItemData.Count; i++)
            {
                if (listItemData[i].state == BuildingState.Unlock)
                    return i;
            }
            return -1;
        }

        public BuildingItemData GetBuildingItemData(int i)
        {
            return _buildingLevelData.listItemData[i];
        }

        public Vector2 GetPosition(int i)
        {
            return buildingObjects[i].Position;
        }
    }
}
