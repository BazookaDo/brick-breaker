﻿using System;
using System.Collections.Generic;
using Do.Scripts.Tools.Audio;
using Do.Scripts.Tools.Dialog;
using Do.Scripts.Tools.Language;
using Home.Main;
using InGame;
using Loading;
using Tutorials;
using UnityEngine;

namespace Popup.Building
{
    public class BuildingPopup : MonoBehaviour
    {
        [SerializeField] private BuildingData buildingData;
        [SerializeField] private List<BuildingRoom> listRoom;
        [SerializeField] private BuildingEffectController buildingEffectController;
        private BuildingRoom _buildingRoom;

        [SerializeField] private Tutorial tutorial;
        private Tutorial _tutorial;
        
        public void Initialize()
        {
            var data = buildingData.buildingData;
            for (var i = 0; i < data.Count; i++)
            {
                var room = listRoom[i];
                room.SetData(this, data[i]);
                if (i == RootManager.Instance.Building)
                {
                    room.gameObject.SetActive(true);
                    _buildingRoom = room;
                }
                else
                    room.gameObject.SetActive(false);
            }
        }

        private void OnEnable()
        {
            if (RootManager.Instance.IsTutorial(1))
            {
                _tutorial = Instantiate(tutorial);
                var id = _buildingRoom.CurrentId();
                _tutorial.ShowWithPosition(_buildingRoom.GetPosition(id));
                HomeManager.Instance.UnActiveMain();
            }
        }

        public void Unlock_Building(BuildingItemData buildingItemData, Vector2 anchoredPosition)
        {
            AudioManager.Instance.Play(SoundType.Click);
            if (RootManager.Instance.IsTutorial(1))
            {
                RootManager.Instance.PassTutorial();
                Destroy(_tutorial.gameObject);
            }
            var rootManager = RootManager.Instance;
            if (rootManager.Star < 3)
            {
                DialogManager.Instance.Message_Dialog(LanguageManager.Instance.Language == Language.Eng ? "Collect More Star !!!" : "HÃY THU THẬP NHIỀU SAO HƠN !!!");
                HomeManager.Instance.ActiveMain();
                return;
            }

            if (HomeManager.Instance != null)
            {
                HomeManager.Instance.UnActiveRay();
                HomeManager.Instance.UnActiveMain();
            }
            else
                InGameCanvasController.Instance.UnActiveRay();

            _buildingRoom.UnActiveUnlock();

            buildingEffectController.Effect(buildingItemData.buildingType, anchoredPosition, delegate
            {
                rootManager.Star -= 3;
                PopupManager.Instance.ShowStar();
                rootManager.OpenBuildingItem(buildingItemData);
                var value = rootManager.GetBuildingItem(rootManager.Building);
                value++;
                rootManager.SetBuildingItem(rootManager.Building, value);
                rootManager.UpdateBuildingData();
                _buildingRoom.UpdateUI();
                _buildingRoom.UnActiveUnlock();
            }, delegate
            {
                if (HomeManager.Instance != null)
                {
                    HomeManager.Instance.ActiveRay();
                    HomeManager.Instance.ActiveMain();
                }
                else
                    InGameCanvasController.Instance.ActiveRay();
                _buildingRoom.ActiveUnlock();
            });
        }
        
        public void UnActiveUnlock()
        {
            _buildingRoom.UnActiveUnlock();
        }

        public void Open_Current(Callback callback)
        {
            var rootManager = RootManager.Instance;
            var id = _buildingRoom.CurrentId();
            if (id >= 0)
            {
                var buildingItemData = _buildingRoom.GetBuildingItemData(id);
                buildingEffectController.Effect(buildingItemData.buildingType, _buildingRoom.GetPosition(id), () =>
                {
                    rootManager.OpenBuildingItem(buildingItemData);
                    var value = rootManager.GetBuildingItem(rootManager.Building);
                    value++;
                    rootManager.SetBuildingItem(rootManager.Building, value);
                    rootManager.UpdateBuildingData();
                    _buildingRoom.UpdateUI();
                    _buildingRoom.ActiveUnlock();
                }, callback);
            }
            else 
                callback?.Invoke();
        }
    }
}
