﻿using UnityEngine;
using UnityEngine.UI;

namespace Popup.Building
{
    public class BuildingObject : MonoBehaviour
    {
        private BuildingPopup _buildingPopup;
        private BuildingItemData _buildingItemData;
        [SerializeField] private Image icon;
        [SerializeField] private GameObject unlock;

        public Vector2 Position => unlock.transform.position;

        private bool _isUnlock;
        public void SetData(BuildingPopup buildingPopup, BuildingItemData buildingItemData)
        {
            _buildingPopup = buildingPopup;
            _buildingItemData = buildingItemData;
            UpdateUI();
        }

        private void UpdateUI()
        {
            if (_buildingItemData.state == BuildingState.Open)
            {
                if (_buildingItemData.buildingType == BuildingType.Remove)
                {
                    icon.color = new Color(0,0,0,0);
                    gameObject.SetActive(false);
                }
                else
                {
                    icon.color = new Color(1,1,1,1);
                    icon.sprite = _buildingItemData.spriteNew;
                    if (unlock.activeSelf)
                        unlock.SetActive(false);
                    gameObject.SetActive(true);
                    _isUnlock = false;
                }
            }
            else
            {
                if (_buildingItemData.buildingType == BuildingType.NewBuild)
                {
                    icon.sprite = _buildingItemData.spriteNew;
                    icon.color = new Color(0,0,0,0);
                }
                else
                {
                    icon.color = new Color(1,1,1,1);
                    icon.sprite = _buildingItemData.spriteOld;
                }
                unlock.SetActive(_buildingItemData.state == BuildingState.Unlock);
                _isUnlock = _buildingItemData.state == BuildingState.Unlock;
                gameObject.SetActive(true);
            }
        }

        public void ActiveUnlock()
        {
            if (_isUnlock)
                unlock.SetActive(true);
        }

        public void UnActiveUnlock()
        {
            if (_isUnlock)
                unlock.SetActive(false);
        }

        public void HandleClick()
        {
            _buildingPopup.Unlock_Building(_buildingItemData, Position);
        }
    }
}
