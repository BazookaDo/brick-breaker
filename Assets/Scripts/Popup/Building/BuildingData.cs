﻿using System;
using System.Collections.Generic;
using Popup.Skin;
using UnityEngine;

namespace Popup.Building
{
    [CreateAssetMenu(fileName = "Data Building", order = 1)]
    public class BuildingData : ScriptableObject
    {
        public List<BuildingLevelData> buildingData;
    }

    [Serializable]
    public class BuildingLevelData
    {
        public List<BuildingItemData> listItemData;
    }

    [Serializable]
    public class BuildingItemData
    {
        public int id;
        public BuildingState state;
        public List<int> levelLocks;
        public BuildingType buildingType;
        public Sprite spriteOld, spriteNew;
    }

    public enum BuildingState
    {
        Lock,
        Unlock,
        Open
    }

    public enum BuildingType
    {
        Remove,
        ReBuild,
        NewBuild
    }
}
