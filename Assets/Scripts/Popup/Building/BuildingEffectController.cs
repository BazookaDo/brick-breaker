﻿using System.Collections;
using Do.Scripts.Tools.Audio;
using Do.Scripts.Tools.Other;
using Spine.Unity;
using UnityEngine;
using AnimationState = Spine.AnimationState;

namespace Popup.Building
{
    public class BuildingEffectController : MonoBehaviour
    {
        [SerializeField] private float removeTime = 1f;
        [SerializeField] private float newBuildTime = 1f;
        [SerializeField] private RectTransform remove, newBuild;
        [SerializeField] private SkeletonGraphic removeSke, newBuildSke;
        [SerializeField] private AnimationReferenceAsset removeAnim, newBuildAnim;
        private AnimationState _removeState, _newBuildState;

        private IEnumerator _routine;
        private Callback _callbackUpdate, _callbackOff;
        public void Effect(BuildingType buildingType, Vector2 position, Callback callbackUpdate, Callback callbackOff)
        {
            if (buildingType == BuildingType.Remove)
            {
                remove.position = position;
                removeSke.transform.localScale = position.x <= 0 ? Vector3.one : new Vector3(-1, 1, 1);
                _routine = Remove();
            }
            else
            {
                newBuild.position = position;
                newBuildSke.transform.localScale = position.x <= 0 ? Vector3.one : new Vector3(-1, 1, 1);
                _routine = NewBuild();
            }
            _callbackUpdate = callbackUpdate;
            _callbackOff = callbackOff;
            StartCoroutine(_routine);
            AudioManager.Instance.Play(SoundType.Building);
        }

        private IEnumerator Remove()
        {
            var time = Yield.GetTime(removeTime);
            remove.gameObject.SetActive(true);
            removeSke.Skeleton.A = 0;
            yield return Yield.EndFrame;
            if (_removeState == null)
                _removeState = removeSke.AnimationState;
            removeSke.Skeleton.A = 1;
            _removeState.SetAnimation(0, removeAnim, true);
            yield return time;
            _callbackUpdate?.Invoke();
            yield return time;
            remove.gameObject.SetActive(false);
            _callbackOff?.Invoke();
        }

        private IEnumerator NewBuild()
        {
            var time = Yield.GetTime(newBuildTime);
            newBuild.gameObject.SetActive(true);
            newBuildSke.Skeleton.A = 0;
            yield return Yield.EndFrame;
            if (_newBuildState == null)
                _newBuildState = newBuildSke.AnimationState;
            newBuildSke.Skeleton.A = 1;
            _newBuildState.SetAnimation(0, newBuildAnim, true);
            yield return time;
            _callbackUpdate?.Invoke();
            yield return time;
            newBuild.gameObject.SetActive(false);
            _callbackOff?.Invoke();
        }
    }
}
