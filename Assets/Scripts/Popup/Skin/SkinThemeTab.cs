﻿using System.Collections.Generic;
using Do.Scripts.Tools.Audio;
using Do.Scripts.Tools.Dialog;
using Do.Scripts.Tools.Language;
using Do.Scripts.Tools.Other;
using EnhancedUI.EnhancedScroller;
using Loading;
using UnityEngine;

namespace Popup.Skin
{
    public class SkinThemeTab : MonoBehaviour, IEnhancedScrollerDelegate
    {
        #region Variable

		[SerializeField] private PopupManager popupManager;
		[SerializeField] private List<SkinThemeData> listSkin;
		[SerializeField] private EnhancedScroller scroller;
		[SerializeField] private EnhancedScrollerCellView cellViewPrefab;
		[SerializeField] private int target;
		[SerializeField] private RectTransform selectedBounder;
		
		private readonly List<ListSkinThemeData> _listSkinThemeData = new List<ListSkinThemeData>();
		private SkinTheme _selectedSkin; // none Use
		private bool _scroll;
		private bool _isOpenAll;
		
		private int _reloadScrollerFrameCountLeft = -1;

		#endregion

		private void OnEnable()
		{
			if (!_scroll)
				return;
			scroller.RefreshActiveCellViews();
			scroller.JumpToDataIndex(target, 0, 0);
			target = 0;
			if (target > 0)
				target = 0;
		}

		private void Start()
		{
			scroller.Delegate = this;
			LoadDataScroll();
			if (!_scroll)
			{
				_scroll = true;
				if (target > 0)
				{
					scroller.JumpToDataIndex(target, 0, 0);
					target = 0;
				}
			}

			var content = scroller.ScrollRect.content;
			var pos = content.localPosition;
			pos.z = 0;
			content.localPosition = pos;
		}

		private void OnDisable()
		{
			target = TargetScroll();
		}

		#region Data
		
		public void Initialize()
		{
			_isOpenAll = true;
			_selectedSkin = RootManager.Instance.CurrentSkinTheme;
			listSkin[0].skinState = SkinState.Unlock;
			for (var i = 1; i < listSkin.Count; i++)
			{
				var skinShop = listSkin[i];
				var skinState = GetSkinState(skinShop.skinSpecies);
				skinShop.skinState = skinState == SkinState.Locked ? SkinState.Locked : SkinState.Unlock;
				if (_isOpenAll && skinState == SkinState.Locked)
					_isOpenAll = false;
				var skinPrice = skinShop.skinPrice;
				if (skinPrice.buySpecies == BuySpecies.Ads)
					skinPrice.currentValue = GetCurrentAdsSkin(skinShop.skinSpecies);
				else if (skinPrice.buySpecies == BuySpecies.InApp)
				{
//					var inAppSkinPrice = RootManager.Instance.GetInAppSkinPrice(skinShop.skinSpecies);
//					skinPrice.inAppValue = inAppSkinPrice != null ? inAppSkinPrice.targetValue : "";
				}
			}
			listSkin[(int) RootManager.Instance.CurrentSkinTheme].skinState = SkinState.Equipped;
			
			// add scroll
			var current = 0;
			var listSkinThemeData = new ListSkinThemeData
			{
				listSkinThemeData = new List<SkinThemeData>()
			};
			foreach (var skinThemeData in listSkin)
			{
				listSkinThemeData.listSkinThemeData.Add(skinThemeData);
				if (skinThemeData.skinState == SkinState.Equipped)
					target = _listSkinThemeData.Count;
				current++;
				if(current < 3)
					continue;
				current = 0;
				_listSkinThemeData.Add(new ListSkinThemeData
				{
					listSkinThemeData = listSkinThemeData.listSkinThemeData
				});
				listSkinThemeData = new ListSkinThemeData
				{
					listSkinThemeData = new List<SkinThemeData>()
				};
			}

			if (current > 0)
			{
				_listSkinThemeData.Add(new ListSkinThemeData
				{
					listSkinThemeData = listSkinThemeData.listSkinThemeData
				});
			}
		}

		public void OpenSkin(SkinTheme skinTheme)
		{
			if (GetSkinState(skinTheme) != SkinState.Locked) 
				return;
			SetSkinState(skinTheme, SkinState.Unlock);
			listSkin[(int) skinTheme].skinState = SkinState.Unlock;
		}

		private SkinState GetSkinState(SkinTheme skinTheme)
		{
			return (SkinState) MyPref.GetInt(RootManager.KEY_SKIN_THEME + skinTheme);
		}

		private void SetSkinState(SkinTheme skinTheme, SkinState skinState)
		{
			MyPref.SetInt(RootManager.KEY_SKIN_THEME + skinTheme, (int) skinState);
		}

		private int GetCurrentAdsSkin(SkinTheme skinTheme)
		{
			return MyPref.GetInt(RootManager.KEY_SKIN_THEME + skinTheme + "Current_Ads");
		}

		private void SetCurrentAdsSkin(SkinTheme skinTheme, int ads)
		{
			MyPref.SetInt(RootManager.KEY_SKIN_THEME + skinTheme + "Current_Ads", ads);
		}

		public bool IsBounderParent(RectTransform compare)
		{
			return selectedBounder.parent == compare;
		}

		public void SelectedBounderFollowParent(RectTransform parent, bool active)
		{
			selectedBounder.gameObject.SetActive(active);
			if (!active) 
				return;
			selectedBounder.SetParent(parent);
			selectedBounder.SetSiblingIndex(0);
			selectedBounder.localPosition = Vector3.zero;
			selectedBounder.localScale = Vector3.one;
		}

		private int TargetScroll()
		{
			for (var i = 0; i < _listSkinThemeData.Count; i++)
			{
				var listSkinThemeData = _listSkinThemeData[i];
				foreach (var skin in listSkinThemeData.listSkinThemeData)
				{
					if (skin.skinState == SkinState.Equipped)
						return i;
				}
			}

			return 0;
		}

		private SkinThemeData GetCurrentSkinShop => listSkin[(int) RootManager.Instance.CurrentSkinTheme];

		#endregion
		
		/// <summary>
		/// ////////
		/// </summary>

		#region Control

		public void HandleClick(SkinThemeData skinThemeData, RectTransform parent)
		{
			AudioManager.Instance.Play(SoundType.Click);
			_selectedSkin = skinThemeData.skinSpecies;
			SelectedBounderFollowParent(parent, true);
		}
		public void BuyWithItem(SkinThemeData skinThemeData)
		{
			AudioManager.Instance.Play(SoundType.Click);
			if (skinThemeData.skinState == SkinState.Locked)
			{
				var skinPrice = skinThemeData.skinPrice;
				if (skinPrice.buySpecies == BuySpecies.Coin)
				{
					if (RootManager.Instance.Coin >= skinPrice.targetValue)
					{
						RootManager.Instance.Coin = -Mathf.RoundToInt(skinPrice.targetValue);
						popupManager.ShowCoin();
						OpenSkin(skinThemeData);
					}
					else
						DialogManager.Instance.Message_Dialog(
							LanguageManager.Instance.Language == Language.Eng
								? "Not Enough Coin !!! \n Go To Shop If You Want More Coin".ToUpper()
								: "Không đủ tiền !!! \n Hãy tới cửa hàng nếu bạn muốn có nhiều tiền hơn.".ToUpper());
				}
				else if (skinPrice.buySpecies == BuySpecies.Ads)
				{
					if (skinPrice.currentValue < skinPrice.targetValue)
					{
						skinPrice.currentValue++;
						SetCurrentAdsSkin(skinThemeData.skinSpecies, (int) skinPrice.currentValue);
						scroller.RefreshActiveCellViews();
					}

					if (skinPrice.currentValue >= skinPrice.targetValue)
						OpenSkin(skinThemeData);
				}
				else if (skinPrice.buySpecies == BuySpecies.InApp)
				{
					OpenSkin(skinThemeData);
				}
			}
			else
				OpenSkin(skinThemeData);
		}

		private void OpenSkin(SkinThemeData skinThemeData)
		{
			GetCurrentSkinShop.skinState = SkinState.Unlock;
			skinThemeData.skinState = SkinState.Equipped;
			SetSkinState(skinThemeData.skinSpecies, skinThemeData.skinState);
			_selectedSkin = RootManager.Instance.CurrentSkinTheme = skinThemeData.skinSpecies;
			scroller.RefreshActiveCellViews();
		}

		public void OpenAllSkin()
		{
			_isOpenAll = true;
			for (var i = 1; i < listSkin.Count; i++)
			{
				var skinThemeData = listSkin[i];
				if (skinThemeData.skinState != SkinState.Locked) 
					continue;
				skinThemeData.skinState = SkinState.Unlock;
				SetSkinState(skinThemeData.skinSpecies, skinThemeData.skinState);
			}
		}

		public bool IsOpenAll => _isOpenAll;

		#endregion
		
		/// <summary>
		/// /////////
		/// </summary>

		#region EnhancedScroller Control

		private void LoadDataScroll()
		{
			// capture the scroller dimensions so that we can reset them when we are done
			var rectTransform = scroller.GetComponent<RectTransform>();
			var size = rectTransform.sizeDelta;

			// set the dimensions to the largest size possible to acommodate all the cells
			rectTransform.sizeDelta = new Vector2(size.x, float.MaxValue);

			// First Pass: reload the scroller so that it can populate the text UI elements in the cell view.
			// The content size fitter will determine how big the cells need to be on subsequent passes
			scroller.ReloadData();

			// reset the scroller size back to what it was originally
			rectTransform.sizeDelta = size;

			// set up our frame countdown so that we can reload the scroller on subsequent frames
			_reloadScrollerFrameCountLeft = 1;
		}

		void LateUpdate()
		{
			// only process if we have a countdown left
			if (_reloadScrollerFrameCountLeft != -1)
			{
				// skip the first frame (frame countdown 1) since it is the one where we set up the scroller text.
				if (_reloadScrollerFrameCountLeft < 1)
				{

					// reload two times, the first to put the newly set content size fitter values into the model,
					// the second to set the scroller's cell sizes based on the model.
					scroller.ReloadData();
				}

				// decrement the frame count
				_reloadScrollerFrameCountLeft--;
			}
		}

		#endregion
		
		/// <summary>
		/// /////////
		/// </summary>

		#region EnhancedScroller Handlers

		public int GetNumberOfCells(EnhancedScroller scroll)
		{
			return _listSkinThemeData.Count;
		}

		public float GetCellViewSize(EnhancedScroller scroll, int dataIndex)
		{
			// we pull the size of the cell from the model.
			// First pass (frame countdown 2): this size will be zero as set in the LoadData function
			// Second pass (frame countdown 1): this size will be set to the content size fitter in the cell view
			// Third pass (frmae countdown 0): this set value will be pulled here from the scroller
			return cellViewPrefab.GetComponent<RectTransform>().sizeDelta.y;
		}

		public EnhancedScrollerCellView GetCellView(EnhancedScroller scroll, int dataIndex, int cellIndex)
		{
			var cellView = scroll.GetCellView(cellViewPrefab) as SkinThemeCellView;
			if (cellView == null) 
				return null;
			cellView.SetData(this, _listSkinThemeData[dataIndex]);
			return cellView;

		}

		#endregion
    }
}
