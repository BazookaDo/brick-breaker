﻿using System.Collections.Generic;
using EnhancedUI.EnhancedScroller;
using UnityEngine;

namespace Popup.Skin
{
    public class SkinBallCellView : EnhancedScrollerCellView
    {
        [SerializeField] private List<SkinBallItem> skinBallItems = new List<SkinBallItem>();
        private SkinBallTab _skinBallTab;
        private ListSkinBallData _listSkinBallData;
        
        public void SetData(SkinBallTab skinBallTab, ListSkinBallData listSkinBallData)
        {
            _skinBallTab = skinBallTab;
            _listSkinBallData = listSkinBallData;
            var listLevelDataCount = _listSkinBallData.listSkinBallData.Count;
            var levelItemsCount = skinBallItems.Count;
            for (var i = 0; i < listLevelDataCount; i++)
            {
                skinBallItems[i].SetDataLevel(_skinBallTab, _listSkinBallData.listSkinBallData[i]);
            }

            if (listLevelDataCount >= levelItemsCount)
                return;
            for (var i = listLevelDataCount; i < levelItemsCount; i++)
            {
                skinBallItems[i].gameObject.SetActive(false);
            }
        }
        
        public override void RefreshCellView()
        {
            var listLevelDataCount = _listSkinBallData.listSkinBallData.Count;
            for (var i = 0; i < listLevelDataCount; i++)
            {
                skinBallItems[i].SetDataLevel(_skinBallTab, _listSkinBallData.listSkinBallData[i]);
            }
        }
    }
}
