﻿using System;
using System.Collections.Generic;
using Do.Scripts.Tools.Language;
using Loading;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Popup.Skin
{
    public class SkinThemeItem : MonoBehaviour
    {
        private SkinThemeTab _skinThemeTab;
        private SkinThemeData _skinThemeData;

        [Header("UI")]
        [SerializeField] private TextMeshProUGUI nameText;
        [SerializeField] private Image icon, button;
        [SerializeField] private List<Sprite> buttonSprites;
        [SerializeField] private GameObject lockCoin, lockAds;
        [SerializeField] private TextMeshProUGUI buttonText;
        private RectTransform _parent;
        private string _title;
        
        public void SetDataLevel(SkinThemeTab skinThemeTab, SkinThemeData skinThemeData)
        {
            gameObject.SetActive(true);
            _skinThemeTab = skinThemeTab;
            _skinThemeData = skinThemeData;
            
            if (_parent == null)
                _parent = GetComponent<RectTransform>();
            UpdateUI();
            
            if (RootManager.Instance.CurrentSkinTheme == skinThemeData.skinSpecies)
                _skinThemeTab.SelectedBounderFollowParent(_parent, true);
            else if (_skinThemeTab.IsBounderParent(_parent))
                _skinThemeTab.SelectedBounderFollowParent(null, false);
        }

        private void UpdateUI()
        {
            nameText.text = _skinThemeData.skinSpecies.ToString();
            icon.sprite = RootManager.Instance.GetSpriteSkinTheme(_skinThemeData.skinSpecies);
            var skinState = _skinThemeData.skinState;
            if (skinState == SkinState.Locked)
            {
                var skinPrice = _skinThemeData.skinPrice;
                switch (skinPrice.buySpecies)
                {
                    case BuySpecies.Coin:
                        button.sprite = buttonSprites[2];
                        lockCoin.SetActive(true);
                        lockAds.SetActive(false);
                        buttonText.text = skinPrice.targetValue + "  .";
                        break;
                    case BuySpecies.Ads:
                        button.sprite = buttonSprites[3];
                        lockCoin.SetActive(false);
                        lockAds.SetActive(true);
                        var content = LanguageManager.Instance.Language == Language.Eng ? "WATCH " : "XEM ";
                        buttonText.text = content + skinPrice.currentValue + "/" + skinPrice.targetValue;
                        break;
                    case BuySpecies.InApp:
                        button.sprite = buttonSprites[4];
                        lockCoin.SetActive(false);
                        lockAds.SetActive(false);
                        buttonText.text = skinPrice.inAppValue;
                        break;
                }
            }
            else if (skinState == SkinState.Unlock)
            {
                button.sprite = buttonSprites[1];
                lockCoin.SetActive(false);
                lockAds.SetActive(false);
                var content = LanguageManager.Instance.Language == Language.Eng ? "USE" : "DÙNG";
                buttonText.text = content;
            }
            else
            {
                button.sprite = buttonSprites[0];
                lockCoin.SetActive(false);
                lockAds.SetActive(false);
                var content = LanguageManager.Instance.Language == Language.Eng ? "USING" : "ĐANG DÙNG";
                buttonText.text = content;
            }
        }

        public void HandleClick()
        {
//            if (_skinBallData.skinState == SkinState.Unlock)
//                _skinBallTab.BuyWithItem(_skinBallData);
//            else
                _skinThemeTab.HandleClick(_skinThemeData, _parent);
        }

        public void BuySkin()
        {
            if (_skinThemeData.skinState == SkinState.Equipped)
                return;
            _skinThemeTab.BuyWithItem(_skinThemeData);
        }
    }

    [Serializable]
    public class ListSkinThemeData
    {
        public List<SkinThemeData> listSkinThemeData;
    }

    [Serializable]
    public class SkinThemeData
    {
        public SkinTheme skinSpecies;
        public SkinState skinState;
        public SkinPrice skinPrice;
    }
}
