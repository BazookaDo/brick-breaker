﻿using System.Collections.Generic;
using EnhancedUI.EnhancedScroller;
using UnityEngine;

namespace Popup.Skin
{
    public class SkinThemeCellView : EnhancedScrollerCellView
    {
        [SerializeField] private List<SkinThemeItem> skinThemeItems = new List<SkinThemeItem>();
        private SkinThemeTab _skinThemeTab;
        private ListSkinThemeData _listSkinThemeData;
        
        public void SetData(SkinThemeTab skinThemeTab, ListSkinThemeData listSkinThemeData)
        {
            _skinThemeTab = skinThemeTab;
            _listSkinThemeData = listSkinThemeData;
            var listLevelDataCount = _listSkinThemeData.listSkinThemeData.Count;
            var levelItemsCount = skinThemeItems.Count;
            for (var i = 0; i < listLevelDataCount; i++)
            {
                skinThemeItems[i].SetDataLevel(_skinThemeTab, _listSkinThemeData.listSkinThemeData[i]);
            }

            if (listLevelDataCount >= levelItemsCount)
                return;
            for (var i = listLevelDataCount; i < levelItemsCount; i++)
            {
                skinThemeItems[i].gameObject.SetActive(false);
            }
        }
        
        public override void RefreshCellView()
        {
            var listLevelDataCount = _listSkinThemeData.listSkinThemeData.Count;
            for (var i = 0; i < listLevelDataCount; i++)
            {
                skinThemeItems[i].SetDataLevel(_skinThemeTab, _listSkinThemeData.listSkinThemeData[i]);
            }
        }
    }
}
