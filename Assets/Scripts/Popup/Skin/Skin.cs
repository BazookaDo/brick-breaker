﻿using System;

namespace Popup.Skin
{
    public enum SkinState
    {
        Locked,
        Unlock,
        Equipped
    }

    [Serializable]
    public class SkinPrice
    {
        public BuySpecies buySpecies;
        public float targetValue;
        public float currentValue;
        public string inAppValue;
    }

    public enum BuySpecies
    {
        Coin,
        Ads,
        InApp,
        Daily,
        Spin
    }
}
