﻿using System.Collections.Generic;
using EnhancedUI.EnhancedScroller;
using UnityEngine;

namespace Popup.Skin
{
    public class SkinCharacterCellView : EnhancedScrollerCellView
    {
        [SerializeField] private List<SkinCharacterItem> skinCharacterItems = new List<SkinCharacterItem>();
        private SkinCharacterTab _skinCharacterTab;
        private ListSkinCharacterData _listSkinCharacterData;
        
        public void SetData(SkinCharacterTab skinCharacterTab, ListSkinCharacterData listSkinCharacterData)
        {
            _skinCharacterTab = skinCharacterTab;
            _listSkinCharacterData = listSkinCharacterData;
            var listLevelDataCount = _listSkinCharacterData.listSkinCharacterData.Count;
            var levelItemsCount = skinCharacterItems.Count;
            for (var i = 0; i < listLevelDataCount; i++)
            {
                skinCharacterItems[i].SetDataLevel(_skinCharacterTab, _listSkinCharacterData.listSkinCharacterData[i]);
            }

            if (listLevelDataCount >= levelItemsCount)
                return;
            for (var i = listLevelDataCount; i < levelItemsCount; i++)
            {
                skinCharacterItems[i].gameObject.SetActive(false);
            }
        }
        
        public override void RefreshCellView()
        {
            var listLevelDataCount = _listSkinCharacterData.listSkinCharacterData.Count;
            for (var i = 0; i < listLevelDataCount; i++)
            {
                skinCharacterItems[i].SetDataLevel(_skinCharacterTab, _listSkinCharacterData.listSkinCharacterData[i]);
            }
        }
    }
}
