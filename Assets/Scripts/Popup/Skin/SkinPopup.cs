﻿using System.Collections.Generic;
using Do.Scripts.Tools.Audio;
using UnityEngine;

namespace Popup.Skin
{
    public class SkinPopup : MonoBehaviour
    {
        public SkinBallTab skinBallTab;
        public SkinCharacterTab skinCharacterTab;
        public SkinThemeTab skinThemeTab;
        [SerializeField] private List<GameObject> fogs;

        [SerializeField]
        private int _currentTab = -1;

        public void Initialize()
        {
            skinBallTab.Initialize();
            skinCharacterTab.Initialize();
            skinThemeTab.Initialize();
        }

        public void ActiveTab(int tab)
        {
            AudioManager.Instance.Play(SoundType.Click);
            if (tab == _currentTab)
                return;
            UnActive();
            _currentTab = tab;
            switch (_currentTab)
            {
                default:
                    skinBallTab.gameObject.SetActive(true);
                    break;
                case 1:
                    skinCharacterTab.gameObject.SetActive(true);
                    break;
                case 2:
                    skinThemeTab.gameObject.SetActive(true);
                    break;
            }
            fogs[_currentTab].SetActive(false);
        }

        private void UnActive()
        {
            if (_currentTab < 0)
                return;
            fogs[_currentTab].SetActive(true);
            switch (_currentTab)
            {
                default:
                    skinBallTab.gameObject.SetActive(false);
                    break;
                case 1:
                    skinCharacterTab.gameObject.SetActive(false);
                    break;
                case 2:
                    skinThemeTab.gameObject.SetActive(false);
                    break;
            }
        }

        public void ResetTab()
        {
            UnActive();
            _currentTab = -1;
        }
    }
}
