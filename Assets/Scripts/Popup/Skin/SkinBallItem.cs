﻿using System;
using System.Collections.Generic;
using Do.Scripts.Tools.Language;
using Loading;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Popup.Skin
{
    public class SkinBallItem : MonoBehaviour
    {
        private SkinBallTab _skinBallTab;
        private SkinBallData _skinBallData;

        [Header("UI")]
        [SerializeField] private TextMeshProUGUI nameText;
        [SerializeField] private Image icon, button;
        [SerializeField] private List<Sprite> buttonSprites;
        [SerializeField] private GameObject lockCoin, lockAds;
        [SerializeField] private TextMeshProUGUI buttonText;
        private RectTransform _parent;
        private string _title;
        
        public void SetDataLevel(SkinBallTab skinBallTab, SkinBallData skinBallData)
        {
            gameObject.SetActive(true);
            _skinBallTab = skinBallTab;
            _skinBallData = skinBallData;
            
            if (_parent == null)
                _parent = GetComponent<RectTransform>();
            UpdateUI();
            
            if (RootManager.Instance.CurrentSkinBall == skinBallData.skinSpecies)
                _skinBallTab.SelectedBounderFollowParent(_parent, true);
            else if (_skinBallTab.IsBounderParent(_parent))
                _skinBallTab.SelectedBounderFollowParent(null, false);
        }

        private void UpdateUI()
        {
            nameText.text = _skinBallData.skinSpecies.ToString();
            icon.sprite = RootManager.Instance.GetSpriteSkinBall(_skinBallData.skinSpecies);
            var size = icon.sprite.pivot;
            size *= 2;
            size = size / 64 * 100;
            icon.GetComponent<RectTransform>().sizeDelta = size;
            var skinState = _skinBallData.skinState;
            if (skinState == SkinState.Locked)
            {
                var skinPrice = _skinBallData.skinPrice;
                switch (skinPrice.buySpecies)
                {
                    case BuySpecies.Coin:
                        button.sprite = buttonSprites[2];
                        lockCoin.SetActive(true);
                        lockAds.SetActive(false);
                        buttonText.text = skinPrice.targetValue + "  .";
                        break;
                    case BuySpecies.Ads:
                        button.sprite = buttonSprites[3];
                        lockCoin.SetActive(false);
                        lockAds.SetActive(true);
                        var content = LanguageManager.Instance.Language == Language.Eng ? "WATCH " : "XEM ";
                        buttonText.text = content + skinPrice.currentValue + "/" + skinPrice.targetValue;
                        break;
                    case BuySpecies.InApp:
                        button.sprite = buttonSprites[4];
                        lockCoin.SetActive(false);
                        lockAds.SetActive(false);
                        buttonText.text = skinPrice.inAppValue;
                        break;
                }
            }
            else if (skinState == SkinState.Unlock)
            {
                button.sprite = buttonSprites[1];
                lockCoin.SetActive(false);
                lockAds.SetActive(false);
                var content = LanguageManager.Instance.Language == Language.Eng ? "USE" : "DÙNG";
                buttonText.text = content;
            }
            else
            {
                button.sprite = buttonSprites[0];
                lockCoin.SetActive(false);
                lockAds.SetActive(false);
                var content = LanguageManager.Instance.Language == Language.Eng ? "USING" : "ĐANG DÙNG";
                buttonText.text = content;
            }
        }

        public void HandleClick()
        {
//            if (_skinBallData.skinState == SkinState.Unlock)
//                _skinBallTab.BuyWithItem(_skinBallData);
//            else
                _skinBallTab.HandleClick(_skinBallData, _parent);
        }

        public void BuySkin()
        {
            if (_skinBallData.skinState == SkinState.Equipped)
                return;
            _skinBallTab.BuyWithItem(_skinBallData);
        }
    }

    [Serializable]
    public class ListSkinBallData
    {
        public List<SkinBallData> listSkinBallData;
    }

    [Serializable]
    public class SkinBallData
    {
        public SkinBall skinSpecies;
        public SkinState skinState;
        public SkinPrice skinPrice;
    }
}
