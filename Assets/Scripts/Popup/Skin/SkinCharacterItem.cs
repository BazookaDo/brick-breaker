﻿using System;
using System.Collections.Generic;
using Do.Scripts.Tools.Language;
using Loading;
using Other;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Popup.Skin
{
    public class SkinCharacterItem : MonoBehaviour
    {
        private SkinCharacterTab _skinCharacterTab;
        private SkinCharacterData _skinCharacterData;

        [Header("UI")]
        [SerializeField] private TextMeshProUGUI nameText;
        [SerializeField] private SkeletonSkin skeletonSkin;
        [SerializeField] private Image icon, button;
        [SerializeField] private List<Sprite> buttonSprites;
        [SerializeField] private GameObject lockCoin, lockAds;
        [SerializeField] private TextMeshProUGUI buttonText;
        private RectTransform _parent;
        private string _title;
        
        public void SetDataLevel(SkinCharacterTab skinCharacterTab, SkinCharacterData skinCharacterData)
        {
            gameObject.SetActive(true);
            _skinCharacterTab = skinCharacterTab;
            _skinCharacterData = skinCharacterData;
            
            if (_parent == null)
                _parent = GetComponent<RectTransform>();
            UpdateUI();
            
            if (RootManager.Instance.CurrentSkinCharacter == skinCharacterData.skinSpecies)
                _skinCharacterTab.SelectedBounderFollowParent(_parent, true);
            else if (skinCharacterTab.IsBounderParent(_parent))
                _skinCharacterTab.SelectedBounderFollowParent(null, false);
        }

        private void UpdateUI()
        {
            nameText.text = _skinCharacterData.skinSpecies.ToString();
            skeletonSkin.SetSkin((int) _skinCharacterData.skinSpecies);
//            icon.sprite = _skinCharacterData.icon;
            var skinState = _skinCharacterData.skinState;
            if (skinState == SkinState.Locked)
            {
                var skinPrice = _skinCharacterData.skinPrice;
                switch (skinPrice.buySpecies)
                {
                    case BuySpecies.Coin:
                        button.sprite = buttonSprites[2];
                        lockCoin.SetActive(true);
                        lockAds.SetActive(false);
                        buttonText.text = skinPrice.targetValue + "   .";
                        break;
                    case BuySpecies.Ads:
                        button.sprite = buttonSprites[3];
                        lockCoin.SetActive(false);
                        lockAds.SetActive(true);
                        var content = LanguageManager.Instance.Language == Language.Eng ? "WATCH " : "XEM ";
                        buttonText.text = content + skinPrice.currentValue + "/" + skinPrice.targetValue;
                        break;
                    case BuySpecies.InApp:
                        button.sprite = buttonSprites[4];
                        lockCoin.SetActive(false);
                        lockAds.SetActive(false);
                        buttonText.text = skinPrice.inAppValue;
                        break;
                }
            }
            else if (skinState == SkinState.Unlock)
            {
                button.sprite = buttonSprites[1];
                lockCoin.SetActive(false);
                lockAds.SetActive(false);
                var content = LanguageManager.Instance.Language == Language.Eng ? "USE" : "DÙNG";
                buttonText.text = content;
            }
            else
            {
                button.sprite = buttonSprites[0];
                lockCoin.SetActive(false);
                lockAds.SetActive(false);
                var content = LanguageManager.Instance.Language == Language.Eng ? "USING" : "ĐANG DÙNG";
                buttonText.text = content;
            }
        }

        public void HandleClick()
        {
//            if (_skinBallData.skinState == SkinState.Unlock)
//                _skinBallTab.BuyWithItem(_skinBallData);
//            else
                _skinCharacterTab.HandleClick(_skinCharacterData, _parent);
        }

        public void BuySkin()
        {
            if (_skinCharacterData.skinState == SkinState.Equipped)
                return;
            _skinCharacterTab.BuyWithItem(_skinCharacterData);
        }
    }

    [Serializable]
    public class ListSkinCharacterData
    {
        public List<SkinCharacterData> listSkinCharacterData;
    }

    [Serializable]
    public class SkinCharacterData
    {
        public SkinCharacter skinSpecies;
//        public Sprite icon;
        public SkinState skinState;
        public SkinPrice skinPrice;
    }
}
