﻿using System;
using InGame;
using InGame.Data;
using InGame.Obstacle;
using Tool;
using UnityEditor;
using UnityEngine;


[CustomEditor (typeof (ObstacleTool))] 
[CanEditMultipleObjects]
public class ObstacleToolEditor : Editor
{
    private static ObstacleTool Instance;
    
    private SerializedProperty obstacleType;
    // Brick
    private SerializedProperty brickType;
    private SerializedProperty brickColor;
    private SerializedProperty brickEffectType;
    private SerializedProperty size;
    private SerializedProperty rotation;
    private SerializedProperty isKey;
    private SerializedProperty keyState;
    // Item
    private SerializedProperty itemType;
    private SerializedProperty itemRotation;
    // value
    private SerializedProperty value;
    private SerializedProperty turnDelay;

    private int _obstacleType;
    
    private int _brickType;
    private int _brickColor;
    private int _brickEffectType;
    private Vector2 _size;
    private Vector3 _rotation;
    private bool _isKey;
    private int _keyState;

    private int _itemType;
    private Vector3 _itemRotation;
    
    private int _value, _turnDelay;

    private readonly string[] _obstacleTypes =
    {
        ObstacleType.None.ToString(),
        ObstacleType.Brick.ToString(),
        ObstacleType.Item.ToString()
    };
    private readonly string[] _brickTypes =
    {
        BrickType.Square.ToString(),
        BrickType.Triangle.ToString(),
        BrickType.Stone.ToString()
    };
    private readonly string[] _SquareColors =
    {
        BrickColor.AutoGreen.ToString(),
        BrickColor.AutoBrown.ToString(),
        BrickColor.White.ToString(),
        BrickColor.Yellow.ToString(),
        BrickColor.Gray.ToString(),
        BrickColor.Blue.ToString(),
        BrickColor.Purple.ToString(),
        BrickColor.Pink.ToString(),
        BrickColor.Red.ToString(),
        BrickColor.Orange.ToString()
    };
    private readonly string[] _TriangleColors =
    {
        BrickColor.AutoGreen.ToString(),
        BrickColor.AutoBrown.ToString()
    };

    private readonly string[] _brickEffectTypes = new[]
    {
        BrickEffectType.None.ToString(),
        BrickEffectType.OneBlock.ToString(),
        BrickEffectType.StepBlock.ToString(),
        BrickEffectType.UnMove.ToString(),
        BrickEffectType.Lighting.ToString(),
        BrickEffectType.Freeze.ToString(),
        BrickEffectType.Fire.ToString(),
        BrickEffectType.Poison.ToString(),
        BrickEffectType.Rocket.ToString(),
        BrickEffectType.TwoDirectionGun.ToString(),
        BrickEffectType.FourDirectionGun.ToString(),
        BrickEffectType.MiniBomb.ToString(),
        BrickEffectType.NuclearBomb.ToString(),
        BrickEffectType.OneSaw.ToString(),
        BrickEffectType.Coin.ToString()
    };
    private readonly string[] _keyStates =
    {
        KeyState.Win.ToString(),
        KeyState.Lose.ToString()
    };
    private readonly string[] _itemTypes =
    {
        ItemType.BarDirection.ToString(),
        ItemType.GunDirection.ToString(),
        ItemType.WormHole.ToString(),
        ItemType.UpSideAutoDirection.ToString(),
        ItemType.LazeTwoDirection.ToString(),
        ItemType.IncreaseBall.ToString(),
        ItemType.LazeFourDirection.ToString(),
        ItemType.AcousticWaves.ToString()
    };

    private void OnEnable()
    {
        Instance = target as ObstacleTool;
        LoadProperties();
    }

    private void LoadProperties()
    {
        obstacleType = serializedObject.FindProperty("obstacleType");
        
        brickType = serializedObject.FindProperty("brickData").FindPropertyRelative("brickType");
        brickColor = serializedObject.FindProperty("brickData").FindPropertyRelative("brickColor");
        brickEffectType = serializedObject.FindProperty("brickData").FindPropertyRelative("brickEffectType");
        size = serializedObject.FindProperty("brickData").FindPropertyRelative("size");
        rotation = serializedObject.FindProperty("brickData").FindPropertyRelative("rotation");
        isKey = serializedObject.FindProperty("brickData").FindPropertyRelative("isKey");
        keyState = serializedObject.FindProperty("brickData").FindPropertyRelative("keyState");
        
        itemType = serializedObject.FindProperty("itemData").FindPropertyRelative("itemType");
        itemRotation = serializedObject.FindProperty("itemData").FindPropertyRelative("rotation");
        
        value = serializedObject.FindProperty("value");
        turnDelay = serializedObject.FindProperty("turnDelay");

        if (Instance == null)
            return;
        _obstacleType = (int) Instance.obstacleType;
        
        _brickType = (int) Instance.brickData.brickType;
        _brickColor = (int) Instance.brickData.brickColor;
        _brickEffectType = (int) Instance.brickData.brickEffectType;
        _size = Instance.brickData.size;
        _rotation = Instance.brickData.rotation;
        _isKey = Instance.brickData.isKey;
        _keyState = (int) Instance.brickData.keyState;

        _itemType = (int) Instance.itemData.itemType;
        _itemRotation = Instance.itemData.rotation;
        
        _value = Instance.value;
        _turnDelay = Instance.turnDelay;
        Instance.ResetEditor = LoadProperties;
    }
    public override void OnInspectorGUI()
    {
        if (Instance == null)
            return;
        serializedObject.Update();

        GUILayout.TextArea("DATA", new GUIStyle(EditorStyles.boldLabel));
        
        _obstacleType = EditorGUILayout.Popup(("Obstacle Type"), _obstacleType, _obstacleTypes);
        obstacleType.intValue = _obstacleType;
        if (Instance.obstacleType == ObstacleType.Brick)
        {
            _brickType = EditorGUILayout.Popup("Brick Type", _brickType, _brickTypes);
            brickType.intValue = _brickType;
            if (Instance.brickData.brickType == BrickType.Square)
            {
                _brickColor = EditorGUILayout.Popup("Color", _brickColor, _SquareColors);
                brickColor.intValue = _brickColor;
                _size.x = EditorGUILayout.IntSlider("Size X", (int) _size.x, 1, 10);
                _size.y = EditorGUILayout.IntSlider("Size Y", (int) _size.y, 1, 10);
                size.vector2Value = _size;
                _value = EditorGUILayout.IntField("Value", _value);
                value.intValue = _value;
                _turnDelay = EditorGUILayout.IntSlider("Turn Delay", _turnDelay, 0, 10);
                turnDelay.intValue = _turnDelay;
                
                GUILayout.TextArea("Effect", new GUIStyle(EditorStyles.boldLabel));
                _brickEffectType = EditorGUILayout.Popup(("Effect Type"), _brickEffectType, _brickEffectTypes);
                brickEffectType.intValue = _brickEffectType;
                var type = Instance.brickData.brickEffectType;
                if (type == BrickEffectType.OneBlock || type == BrickEffectType.OneSaw || type == BrickEffectType.TwoDirectionGun)
                {
                    EditorGUILayout.BeginHorizontal();
                    var z = _rotation.z;
                    if (GUILayout.Button("Rotate Left"))
                    {
                        z = NewRotateLeft(z);
                    }

                    if (GUILayout.Button("Rotate Right"))
                    {
                        z = NewRotateRight(z);
                    }

                    _rotation.z = z;
                    rotation.vector3Value = _rotation;
                    EditorGUILayout.EndHorizontal();
                }

                if (Instance.IsRescueMode())
                {
                    GUILayout.TextArea("Key Map", new GUIStyle(EditorStyles.boldLabel));
                    _isKey = EditorGUILayout.Toggle("Is Key", _isKey);
                    isKey.boolValue = _isKey;
                    if (Instance.brickData.isKey)
                    {
                        _keyState = EditorGUILayout.Popup("Key State", _keyState, _keyStates);
                        keyState.intValue = _keyState;
                    }
                }
            }
            else if (Instance.brickData.brickType == BrickType.Triangle)
            {
                _brickColor = EditorGUILayout.Popup("Color", _brickColor, _TriangleColors);
                brickColor.intValue = _brickColor;
                EditorGUILayout.BeginHorizontal();
                var z = _rotation.z;
                if (GUILayout.Button("Rotate Left"))
                {
                    z = NewRotateLeft(z);
                }

                if (GUILayout.Button("Rotate Right"))
                {
                    z = NewRotateRight(z);
                }

                _rotation.z = z;
                rotation.vector3Value = _rotation;
                EditorGUILayout.EndHorizontal();
                _value = EditorGUILayout.IntField("Value", _value);
                value.intValue = _value;
                _turnDelay = EditorGUILayout.IntSlider("Turn Delay", _turnDelay, 0, 10);
                turnDelay.intValue = _turnDelay;
            }
        }
        else if (Instance.obstacleType == ObstacleType.Item)
        {
            _itemType = EditorGUILayout.Popup(("Item Type"), _itemType, _itemTypes);
            itemType.intValue = _itemType;
            if (Instance.itemData.itemType == ItemType.LazeTwoDirection)
            {
                EditorGUILayout.BeginHorizontal();
                var z = _itemRotation.z;
                if (GUILayout.Button("Rotate Left"))
                {
                    z = NewRotateLeft(z);
                }

                if (GUILayout.Button("Rotate Right"))
                {
                    z = NewRotateRight(z);
                }

                _itemRotation.z = z;
                itemRotation.vector3Value = _itemRotation;
                EditorGUILayout.EndHorizontal();
            }
            else if (Instance.itemData.itemType == ItemType.BarDirection ||
                     Instance.itemData.itemType == ItemType.GunDirection)
            {
                EditorGUILayout.BeginHorizontal();
                var z = _itemRotation.z;
                GUILayout.Label("Clockwise : Is " + (z < 0 ? "Left" : "Right"));
                
                if (GUILayout.Button("Left"))
                {
                    z = -90;
                }

                if (GUILayout.Button("Right"))
                {
                    z = 90;
                }

                _itemRotation.z = z;
                itemRotation.vector3Value = _itemRotation;
                EditorGUILayout.EndHorizontal();
            }
            _turnDelay = EditorGUILayout.IntSlider("Turn Delay", _turnDelay, 0, 10);
            turnDelay.intValue = _turnDelay;
        }
        serializedObject.ApplyModifiedProperties();
        Instance.OnChangeValue();

        GUILayout.Space(50);
        
        GUILayout.TextArea("RESET DATA", new GUIStyle(EditorStyles.boldLabel));
        if (GUILayout.Button("Warning Reset"))
        {
            Instance.ResetData();
        }
    }

    private float NewRotateLeft(float z)
    {
        if (z == 0)
            z = -90;
        else if (Math.Abs(z + 90) <= 0)
            z = 180;
        else if (Math.Abs(z - 180) <= 0)
            z = 90;
        else if (Math.Abs(z - 90) <= 0)
            z = 0;
        return z;
    }

    private float NewRotateRight(float z)
    {
        if (z == 0)
            z = 90;
        else if (Math.Abs(z - 90) <= 0)
            z = 180;
        else if (Math.Abs(z - 180) <= 0)
            z = -90;
        else if (Math.Abs(z + 90) <= 0)
            z = 0;
        return z;
    }
}
