﻿using InGame;
using Tool;
using UnityEditor;
using UnityEngine;

[CustomEditor (typeof (ToolManager))] 
[CanEditMultipleObjects]
public class ToolEditor : Editor
{
    private static ToolManager Instance;
    private SerializedProperty gameMode;
    private SerializedProperty gameStep;
    private SerializedProperty rescueState;
    private SerializedProperty cameraSize;
    private SerializedProperty openLevel;

    private int _gameMode;
    private int _gameStep;
    private int _rescueState;
    private int _cameraSize;
    private int _openLevel;

    private readonly string[] _gameModes = 
    {
        GameMode.Normal.ToString(),
        GameMode.Rescue.ToString()
    };

    private readonly string[] _gameSteps =
    {
        GameStep.Move.ToString(),
        GameStep.Freeze.ToString(),
    };

    private readonly string[] _rescueStates =
    {
        RescueState.Water.ToString(),
        RescueState.Puzzle.ToString(),
    };
    private void OnEnable()
    {
        gameMode = serializedObject.FindProperty("gameMode");
        gameStep = serializedObject.FindProperty("gameStep");
        rescueState = serializedObject.FindProperty("rescueState");
        cameraSize = serializedObject.FindProperty("cameraSize");
        openLevel = serializedObject.FindProperty("openLevel");
        Instance = target as ToolManager;
        if (Instance == null)
            return;
        _gameMode = (int) Instance.gameMode;
        _gameStep = (int) Instance.gameStep;
        _rescueState = (int) Instance.rescueState;
        _cameraSize = Instance.cameraSize;
        _openLevel = Instance.openLevel;
        Instance.ResetEditor = () =>
        {
            _gameMode = (int) Instance.gameMode;
            _cameraSize = Instance.cameraSize;
            _openLevel = Instance.openLevel;
            _gameStep = (int) Instance.gameStep;
            _rescueState = (int) Instance.rescueState;
        };
    }

    public override void OnInspectorGUI()
    {
        if (Instance == null)
            return;
        serializedObject.Update();
        var normal = Instance.obstacleDataNormal.obstacleLevels.Count;
        var rescue = Instance.obstacleDataRescue.obstacleLevels.Count;
        EditorGUILayout.BeginHorizontal();
        GUILayout.TextArea("NORMAL LEVEL MAX :  " + normal, new GUIStyle(EditorStyles.boldLabel));
        GUILayout.TextArea("RESCUE LEVEL MAX :  " + rescue, new GUIStyle(EditorStyles.boldLabel));
        EditorGUILayout.EndHorizontal();
        GUILayout.Space(5);

        _gameMode = EditorGUILayout.Popup("Game Mode", _gameMode, _gameModes);
        gameMode.intValue = _gameMode;
        GUILayout.Space(10);
        _gameStep = EditorGUILayout.Popup("Game Step", _gameStep, _gameSteps);
        gameStep.intValue = _gameStep;
        if (Instance.gameMode == GameMode.Rescue)
        {
            _rescueState = EditorGUILayout.Popup("Rescue State", _rescueState, _rescueStates);
            rescueState.intValue = _rescueState;
        }

        _cameraSize = EditorGUILayout.IntSlider("Size Camera", _cameraSize, 7, 21);
        cameraSize.intValue = _cameraSize;
        Instance.SetCameraSize();
        GUILayout.Space(10);
        
        var maxLevel = Instance.gameMode == GameMode.Normal ? normal : rescue;
        if (_openLevel < 0)
        {
            _openLevel = 0;
            openLevel.intValue = _openLevel;
        }
        else if (_openLevel > maxLevel)
        {
            _openLevel = maxLevel;
            openLevel.intValue = _openLevel;
        }

        if (Instance.IsNull())
        {
            _openLevel = EditorGUILayout.IntField("Level Fix", _openLevel);
            openLevel.intValue = _openLevel;
            if (GUILayout.Button("Open This Level"))
            {
                Instance.LoadLevel();
            }
        }
        else
        {
            GUILayout.TextArea("SAVE AS NEW LEVEL IF YOU WANT CREATE A NEW LEVEL", new GUIStyle(EditorStyles.boldLabel));
            if (GUILayout.Button("Save As New"))
            {
                Instance.SaveAsNewLevel();
            }
            GUILayout.Space(5);
            
            GUILayout.TextArea("SAVE AS FIX OLD LEVEL", new GUIStyle(EditorStyles.boldLabel));
            _openLevel = EditorGUILayout.IntField("Level Fix", _openLevel);
            openLevel.intValue = _openLevel;
            if(_openLevel > 0)
                if (GUILayout.Button("Save As Old"))
                {
                    Instance.SaveAsOldLevel();
                }
        }
        
        GUILayout.Space(25);
        GUILayout.TextArea("RESET DATA", new GUIStyle(EditorStyles.boldLabel));
        if (GUILayout.Button("Warning Reset All"))
        {
            Instance.ForceReset();
        }

        serializedObject.ApplyModifiedProperties();
    }
}
