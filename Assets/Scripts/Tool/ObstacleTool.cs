﻿using System.Linq;
using InGame;
using InGame.Data;
using InGame.Obstacle;
using Other;
using TMPro;
using UnityEditor;
using UnityEngine;

namespace Tool
{
    [SelectionBase] [ExecuteInEditMode]
    public class ObstacleTool : MonoBehaviour
    {
        public ObstacleType obstacleType;
        public BrickData brickData;
        public ItemData itemData;
        public int value;
        public int turnDelay;

        public Callback ResetEditor;
        private GameObject _brick, _item;
        private SpriteRenderer _iconBrick, _effectBrick, _iconItem, _effectItem;
        private TextMeshPro _textMeshPro, _turnText;

        private ObstacleSprites _obstacleSprites;
        private ToolManager _toolManager;

        public Vector2 effectPosition => _effectItem != null ? (Vector2 ) _effectItem.transform.position : Vector2.zero;

        public void SetData(ObstacleItem obstacleItem, ToolManager toolManager)
        {
            _toolManager = toolManager;
            transform.position = obstacleItem.position;
            obstacleType = obstacleItem.obstacleType;
            brickData = obstacleItem.brickData;
            itemData = obstacleItem.itemData;
            value = obstacleItem.value;
            turnDelay = obstacleItem.turnDelay;
            OnChangeValue();
            var childCount = transform.childCount;
            _textMeshPro = transform.GetChild(childCount - 2).GetComponent<TextMeshPro>();
            _turnText = transform.GetChild(childCount - 1).GetComponent<TextMeshPro>();
        }

        public void OnChangeValue()
        {
            if (_obstacleSprites == null)
                _obstacleSprites = FindObjectOfType<ObstacleSprites>();
            if (_brick == null)
                _brick = transform.GetChild(0).gameObject;
            if (_item == null)
                _item = transform.GetChild(1).gameObject;
            if (_iconBrick == null)
                _iconBrick = _brick.transform.GetChild(0).GetComponent<SpriteRenderer>();
            if (_effectBrick == null)
                _effectBrick = _brick.transform.GetChild(1).GetComponent<SpriteRenderer>();
            if (_iconItem == null)
                _iconItem = _item.transform.GetChild(0).GetComponent<SpriteRenderer>();
            if(_textMeshPro == null)
                _textMeshPro = transform.GetChild(transform.childCount - 2).GetComponent<TextMeshPro>();
            if (_turnText == null)
                _turnText = transform.GetChild(transform.childCount - 1).GetComponent<TextMeshPro>();
            if (obstacleType == ObstacleType.Brick)
            {
                if (_item.activeSelf)
                    _item.SetActive(false);
                if (!_brick.activeSelf)
                    _brick.SetActive(true);
                if (!_iconBrick.gameObject.activeSelf)
                    _iconBrick.gameObject.SetActive(true);
                if (!_turnText.gameObject.activeSelf && turnDelay > 0)
                    _turnText.gameObject.SetActive(true);
                else if (turnDelay <= 0)
                    _turnText.gameObject.SetActive(false);

                if (brickData.brickType == BrickType.Square)
                {
                    _iconBrick.sprite = brickData.brickColor == BrickColor.AutoGreen
                        ? _obstacleSprites.bricks[(int) brickData.brickType]
                        : _obstacleSprites.squareColors[(int) brickData.brickColor - 1];
                    if (!_textMeshPro.gameObject.activeSelf)
                        _textMeshPro.gameObject.SetActive(true);
                    _brick.transform.eulerAngles = Vector3.zero;
                    _iconBrick.drawMode = SpriteDrawMode.Sliced;
                    _iconBrick.size = brickData.size;
                    if (brickData.brickEffectType != BrickEffectType.None)
                    {
                        if (!_effectBrick.gameObject.activeSelf)
                            _effectBrick.gameObject.SetActive(true);
                        _effectBrick.sprite = _obstacleSprites.brickEffects[(int) brickData.brickEffectType - 1];
                        _effectBrick.transform.localScale = Vector3.one;
                        if (!_effectBrick.gameObject.activeSelf)
                            _effectBrick.gameObject.SetActive(true);
                        if (brickData.brickEffectType == BrickEffectType.OneBlock ||
                            brickData.brickEffectType == BrickEffectType.OneSaw ||
                            brickData.brickEffectType == BrickEffectType.UnMove)
                        {
                            _effectBrick.drawMode = SpriteDrawMode.Sliced;
                            var size =
                                brickData.brickEffectType == BrickEffectType.OneSaw
                                    ? new Vector2(brickData.size.x * 1.44f, brickData.size.y * 1.44f)
                                    : brickData.brickEffectType == BrickEffectType.OneBlock
                                        ? new Vector2(brickData.size.x * 1.08f, brickData.size.y * 1.05f)
                                        : brickData.size;
                            _effectBrick.size = size;
                            _effectBrick.transform.eulerAngles = brickData.rotation;
                        }
                        else if (brickData.brickEffectType == BrickEffectType.TwoDirectionGun)
                        {
                            _effectBrick.size = Vector2.one;
                            _effectBrick.drawMode = SpriteDrawMode.Simple;
                            _effectBrick.transform.eulerAngles = brickData.rotation;
                        }
                        else
                        {
                            _effectBrick.size = Vector2.one;
                            _effectBrick.drawMode = SpriteDrawMode.Simple;
                            _effectBrick.transform.eulerAngles = Vector3.zero;
                        }
                    }
                    else
                    {
                        if (_effectBrick.gameObject.activeSelf)
                            _effectBrick.gameObject.SetActive(false);
                    }
                }
                else if (brickData.brickType == BrickType.Triangle)
                {
                    _iconBrick.sprite = brickData.brickColor == BrickColor.AutoGreen
                        ? _obstacleSprites.bricks[(int) brickData.brickType]
                        : _obstacleSprites.triangleColors[(int) brickData.brickColor - 1];
                    if (!_textMeshPro.gameObject.activeSelf)
                        _textMeshPro.gameObject.SetActive(true);
                    _brick.transform.eulerAngles = brickData.rotation;
                    _iconBrick.size = Vector2.one;
                    _iconBrick.drawMode = SpriteDrawMode.Simple;
                    if (_effectBrick.gameObject.activeSelf)
                        _effectBrick.gameObject.SetActive(false);
                }
                else
                {
                    _iconBrick.sprite = _obstacleSprites.bricks[(int) brickData.brickType];
                    if (_textMeshPro.gameObject.activeSelf)
                        _textMeshPro.gameObject.SetActive(false);
                    if (_effectBrick.gameObject.activeSelf)
                        _effectBrick.gameObject.SetActive(false);
                    _iconBrick.size = Vector2.one;
                    _iconBrick.drawMode = SpriteDrawMode.Simple;
                    _brick.transform.eulerAngles = Vector3.zero;
                }
            }
            else if (obstacleType == ObstacleType.Item)
            {
                if (_brick.activeSelf)
                    _brick.SetActive(false);
                if (!_item.activeSelf)
                    _item.SetActive(true);
                if (_textMeshPro.gameObject.activeSelf)
                    _textMeshPro.gameObject.SetActive(false);
                if (!_turnText.gameObject.activeSelf && turnDelay > 0)
                    _turnText.gameObject.SetActive(true);
                else if (turnDelay <= 0)
                    _turnText.gameObject.SetActive(false);
                _iconItem.sprite = _obstacleSprites.items[(int) itemData.itemType];
                _item.transform.eulerAngles = itemData.itemType == ItemType.LazeTwoDirection ? itemData.rotation : Vector3.zero;
                if (!_iconItem.gameObject.activeSelf)
                    _iconItem.gameObject.SetActive(true);
                if (itemData.itemType == ItemType.WormHole)
                {
                    if (_effectItem == null)
                    {
                        if (_iconItem.transform.parent.childCount <= 1)
                        {
                            _effectItem = Instantiate(_iconItem, _iconItem.transform.parent);
                            _effectItem.name = "WormHoleTarget";
                            _effectItem.gameObject.AddComponent<Selected>();
                            _effectItem.color = new Color(1, 1, 1, 0.6f);
                        }
                        else
                            _effectItem = _iconItem.transform.parent.GetChild(1).GetComponent<SpriteRenderer>();
                    }

                    if (!_effectItem.gameObject.activeSelf)
                    {
                        _effectItem.gameObject.SetActive(true);
                    }
                    _effectItem.transform.position = itemData.wormHoleTarget;
                }
                else
                {
                    if (_effectItem != null && _effectItem.gameObject.activeSelf)
                        _effectItem.gameObject.SetActive(false);
                }
            }
            else
            {
                if (_brick.activeSelf)
                    _brick.SetActive(false);
                if (_item.activeSelf)
                    _item.SetActive(false);
                if (_textMeshPro.gameObject.activeSelf)
                    _textMeshPro.gameObject.SetActive(false);
                if (_turnText.gameObject.activeSelf)
                    _turnText.gameObject.SetActive(false);
            }
            _textMeshPro.text = value.ToString();
            _turnText.text = turnDelay.ToString();
        }


        public void ResetData( )
        {
            var Origin = GameObject.Find("Origin").transform;
            var index = transform.GetSiblingIndex();
            transform.localPosition = Origin.GetChild(index).localPosition;
            transform.localScale = Vector3.one;
            obstacleType = ObstacleType.None;
            brickData.brickType = BrickType.Square;
            brickData.brickColor = BrickColor.AutoGreen;
            brickData.brickEffectType = BrickEffectType.None;
            brickData.size = Vector2.one;
            brickData.rotation = Vector3.zero;
            brickData.isKey = false;
            brickData.keyState = KeyState.Lose;
            itemData.itemType = ItemType.BarDirection;
            itemData.rotation = Vector3.zero;
            itemData.wormHoleTarget = Vector2.zero;
            value = 0;
            turnDelay = 0;
            ResetEditor?.Invoke();
            OnChangeValue();
        }
        private void Update()
        {
            if (obstacleType == ObstacleType.Item && itemData.itemType == ItemType.WormHole && _effectItem != null)
            {
                var pos = _effectItem.transform.position;
                pos.x = Mathf.RoundToInt(pos.x * 2) / 2f;
                pos.y = Mathf.RoundToInt(pos.y * 2) / 2f;
                _effectItem.transform.position = pos;
            }
            #if UNITY_EDITOR
            if (Selection.objects.Length <= 0) 
                return;
            if (Selection.objects.Any(objects => objects == gameObject))
            {
                ReflectTransform();
                OnChangeValue();
            }
            #endif
        }

        private void ReflectTransform()
        {
            var pos = transform.position;
            pos.x = Mathf.RoundToInt(pos.x * 2) / 2f;
            pos.y = Mathf.RoundToInt(pos.y * 2) / 2f;
            transform.position = pos;
        }

        public bool IsRescueMode()
        {
            if (_toolManager == null)
                _toolManager = FindObjectOfType<ToolManager>();
            return _toolManager.gameMode == GameMode.Rescue;
        }
    }
}
