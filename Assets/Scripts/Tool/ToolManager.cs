﻿using System;
using System.Collections.Generic;
using System.Linq;
using Do.Scripts.Tools.Control;
using InGame;
using InGame.Data;
using InGame.Obstacle;
using UnityEditor;
using UnityEngine;

namespace Tool
{
    #if UNITY_EDITOR
    [SelectionBase] [ExecuteInEditMode]
    #endif
    public class ToolManager : MonoBehaviour
    {
        public ObstacleData obstacleDataNormal, obstacleDataRescue;
        public List<ObstacleTool> _obstacleTools;

        public Transform bounder;
        public GameMode gameMode;
        public GameStep gameStep;
        public RescueState rescueState;
        public int cameraSize;
        public Transform rootTransform;
        public int openLevel;

        public Callback ResetEditor;

        public void LoadLevel()
        {
            var count =
                gameMode == GameMode.Normal
                    ? obstacleDataNormal.obstacleLevels.Count
                    : obstacleDataRescue.obstacleLevels.Count;
            if (openLevel <= 0 || openLevel > count) 
                return;
            var obstacleLevel = gameMode == GameMode.Normal
                ? obstacleDataNormal.obstacleLevels[openLevel - 1]
                : obstacleDataRescue.obstacleLevels[openLevel - 1];
            rescueState = obstacleLevel.rescueState;
            cameraSize = obstacleLevel.cameraSize;
            SetCameraSize();
            rootTransform.position = obstacleLevel.rootPosition;
            foreach (var obstacleItem in obstacleLevel.obstacleItems)
            {
                var newObstacleItem = new ObstacleItem
                {
                    id = obstacleItem.id,
                    position = obstacleItem.position,
                    obstacleType = obstacleItem.obstacleType,
                    brickData = new BrickData
                    {
                        brickType = obstacleItem.brickData.brickType,
                        brickColor = obstacleItem.brickData.brickColor,
                        brickEffectType = obstacleItem.brickData.brickEffectType,
                        size = obstacleItem.brickData.size,
                        rotation = obstacleItem.brickData.rotation,
                        isKey = obstacleItem.brickData.isKey,
                        keyState = obstacleItem.brickData.keyState
                    },
                    itemData = new ItemData
                    {
                        itemType = obstacleItem.itemData.itemType,
                        rotation = obstacleItem.itemData.rotation,
                        wormHoleTarget = obstacleItem.itemData.wormHoleTarget
                    },
                    value = obstacleItem.value,
                    turnDelay = obstacleItem.turnDelay
                };
                _obstacleTools[obstacleItem.id].SetData(newObstacleItem, this);
            }
            ResetEditor?.Invoke();
        }

        public void SaveAsOldLevel()
        {
            var count =
                gameMode == GameMode.Normal
                    ? obstacleDataNormal.obstacleLevels.Count
                    : obstacleDataRescue.obstacleLevels.Count;
            if (openLevel <= 0 || openLevel > count) 
                return;
            if (gameMode == GameMode.Normal)
                obstacleDataNormal.obstacleLevels[openLevel - 1] = GetSceneObstacleLevel();
            else
                obstacleDataRescue.obstacleLevels[openLevel - 1] = GetSceneObstacleLevel();
            ForceReset();
        }

        public void SaveAsNewLevel()
        {
            if (gameMode == GameMode.Normal)
                obstacleDataNormal.obstacleLevels.Add(GetSceneObstacleLevel());
            else
                obstacleDataRescue.obstacleLevels.Add(GetSceneObstacleLevel());
            ForceReset();
        }

        private ObstacleLevel GetSceneObstacleLevel()
        {
            var obstacleLevel = new ObstacleLevel
            {
                gameMode = gameMode,
                gameStep = gameStep,
                rescueState = rescueState,
                cameraSize = cameraSize,
                rootPosition = rootTransform.position,
                obstacleItems = new List<ObstacleItem>()
            };
            for (var i = 0; i < _obstacleTools.Count; i++)
            {
                var obstacleTool = _obstacleTools[i];
                if (obstacleTool.obstacleType == ObstacleType.None)
                    continue;
                var brickData = obstacleTool.brickData;
                obstacleLevel.obstacleItems.Add(new ObstacleItem
                {
                    id = i,
                    position = obstacleTool.transform.position,
                    obstacleType = obstacleTool.obstacleType,
                    brickData = obstacleTool.obstacleType == ObstacleType.Brick
                        ? new BrickData
                        {
                            brickType = brickData.brickType,
                            brickColor = brickData.brickColor,
                            brickEffectType = brickData.brickEffectType,
                            size = brickData.size,
                            rotation = brickData.rotation,
                            isKey = brickData.isKey,
                            keyState = brickData.keyState
                        }
                        : new BrickData(),
                    itemData = obstacleTool.obstacleType == ObstacleType.Item
                        ? new ItemData
                        {
                            itemType = obstacleTool.itemData.itemType,
                            rotation = obstacleTool.itemData.rotation,
                            wormHoleTarget = obstacleTool.effectPosition
                        }
                        : new ItemData(),
                    value = obstacleTool.value,
                    turnDelay = obstacleTool.turnDelay
                });
            }

            return obstacleLevel;
        }

        public void SetCameraSize()
        {
            // Camera.main.orthographicSize = cameraSize;
            var screen = new Vector2(1080, 1920);
            var unitPixel = cameraSize / screen.x;
            var clamp = 0.5f * unitPixel * screen.y;
            Camera.main.orthographicSize = clamp;
            var scale = clamp / 20;
            // bounder.position = new Vector3(0, 0.83f - (20 - cameraSize) / 10, 0);
            bounder.localScale = new Vector3(scale, scale, 1);
        }

        public void ForceReset()
        {
            #if UNITY_EDITOR
            EditorUtility.SetDirty(obstacleDataNormal);
            EditorUtility.SetDirty(obstacleDataRescue);
            #endif
            gameMode = GameMode.Normal;
            gameStep = GameStep.Move;
            rescueState = RescueState.Water;
            cameraSize = 20;
            SetCameraSize();
            rootTransform.position = Vector2.zero;
            openLevel = obstacleDataNormal.obstacleLevels.Count + 1;
            foreach (var obstacleTool in _obstacleTools)
            {
                obstacleTool.ResetData();
            }
            ResetEditor?.Invoke();
        }

        public bool IsNull()
        {
            return _obstacleTools.All(obstacleTool => obstacleTool.obstacleType == ObstacleType.None);
        }

        #if UNITY_EDITOR
        private void Update()
        {
            if (Selection.activeGameObject != rootTransform.gameObject)
                return;
            var position = rootTransform.position;
            position.x = Mathf.RoundToInt(position.x * 2) / 2f;
            position.y = Mathf.RoundToInt(position.y * 2) / 2f;
            rootTransform.position = position;
        }
        #endif
    }
}
