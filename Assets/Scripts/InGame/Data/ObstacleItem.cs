﻿using System;
using InGame.Obstacle;
using UnityEngine;

namespace InGame.Data
{
    [Serializable]
    public class ObstacleItem
    {
        public int id;
        public Vector2 position;
        public ObstacleType obstacleType;
        public BrickData brickData;
        public ItemData itemData;
        public int value;
        public int turnDelay;
    }

    public enum ObstacleType
    {
        None,
        Brick,
        Item
    }
}
