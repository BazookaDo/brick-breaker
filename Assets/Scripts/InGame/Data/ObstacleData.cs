﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace InGame.Data
{
    // [CreateAssetMenu(fileName = "Obstacle Data", order = 1)]
    public class ObstacleData : ScriptableObject
    {
        public List<ObstacleLevel> obstacleLevels;
    }

    [Serializable]
    public class ObstacleLevel
    {
        public GameMode gameMode;
        public GameStep gameStep;
        public RescueState rescueState;
        public int cameraSize;
        public Vector2 rootPosition;
        public List<ObstacleItem> obstacleItems;
    }
}
