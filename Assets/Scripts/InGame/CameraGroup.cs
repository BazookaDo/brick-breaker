﻿using UnityEngine;

namespace InGame
{
    public class CameraGroup : MonoBehaviour
    {
        [SerializeField] private new Camera camera;
        [SerializeField] private new Camera extraCamera;

        public Camera GetCameraActive()
        {
            if (!gameObject.activeSelf)
                gameObject.SetActive(true);
            return camera;
        }

        public Camera ExtraCamera => extraCamera;

        public void DisableCamera()
        {
            if (gameObject.activeSelf)
                gameObject.SetActive(false);
        }
    }
}
