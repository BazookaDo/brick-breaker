﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using Do.Scripts.Tools.Audio;
using Do.Scripts.Tools.Control;
using Do.Scripts.Tools.Dialog;
using Do.Scripts.Tools.Language;
using Do.Scripts.Tools.Other;
using Do.Scripts.Tools.Pool;
using Do.Scripts.Tools.Tween;
using InGame.Data;
using InGame.Obstacle;
using InGame.Rescue;
using Loading;
using Other;
using Popup;
using Spine.Unity;
using TMPro;
using Tutorials;
using UnityEngine;
using UnityEngine.UI;
using Water2D;

namespace InGame
{
    public class GameController : MonoBehaviour
    {
        public static GameController Instance { get; private set; }
        private GameState GameState { get; set; }
        private CameraModeState CameraState { get; set; }
        private GameStep GameStep { get; set; }
        private RescueState RescueState { get; set; }

        [Header("Controller")]
        [SerializeField] private int levelTest;
        [SerializeField] private GameMode gameMode;
        [SerializeField] private CameraGroup normalCamera;
        [SerializeField] private CameraGroup waterCamera;
        [SerializeField] private Camera cameraCanvas;
        [SerializeField] private Transform quadWater;
        [SerializeField] private ObstacleData obstacleDataNormal, obstacleDataRescue;
        [SerializeField] private List<RescueBase> _rescueBases;
        private Camera _mainCamera;
        [SerializeField] private float timeFireRate = 0.1f;
        [Range(3f, 30f)][SerializeField] private float edgeFire = 10f;
        [SerializeField] private LayerMask mask, wallMask;
        [SerializeField] private GameObject controller_Btn_Cancel;
        [SerializeField] private SpriteRenderer background;
        [SerializeField] private List<Image> uiBounders, uiBoosters;
        [SerializeField] private List<Sprite> backgroundSkins, uiBounderSkins, uiBoosterSkins;
        [SerializeField] private Transform bounder;
        private RescueBase _rescueBase;
        private ObstacleLevel _obstacleLevel;
        private ObjectPoolManager _pool;
        private IEnumerator _routine;
        private int _oldCount, _oldDoubleCount;
        private bool _isTouch, _isCancel;
        
        private Vector2 _direction, _doubleDirection;
        private Vector3 _sizeOrigin = new Vector3(0.6f, 0.6f, 1f);
        private Vector3 _sizeForward = new Vector3(0.5f, 0.5f, 1f);

        [Header("Score")]
        [SerializeField] private int score;
        [SerializeField] private GameObject scoreObject;
        [SerializeField] private TextMeshProUGUI scoreText;
        [SerializeField] private Image scoreFill;
        [SerializeField] private List<Image> stars;
        [SerializeField] private Sprite starOn;

        private float _scoreMax, _scoreCurrent;
        private int _star, _brickBreak, _brickMax;

        [Header("Barrel")] 
        [SerializeField] private Transform barrelTransform;
        [SerializeField] private Transform iconTransform;
        [SerializeField] private Transform directionTransform, directionDoubleTransform;
        [SerializeField] private Transform iconOldTransform, iconNewTransform, iconDoubleTransform;
        [SerializeField] private List<SpriteRenderer> ballIcons;
        [SerializeField] private TextMeshPro countBallText;
        private readonly List<Transform> _iconNewTransforms = new List<Transform>();
        private readonly List<Transform> _iconDoubleTransforms = new List<Transform>();
        
        public Vector2 TargetToRecall => iconNewTransform.position;
        
        [Header("Ball")]
        [Range(0.5f, 5f)] [SerializeField] private float ballSpeed = 1f;
        [Range(5f, 20f)] [SerializeField] private float ballTimeToEuler;
        [Range(0.25f, 1f)] [SerializeField] private float ballTimeToRecall;
        [Range(1.25f, 2.5f)] [SerializeField] private float ballAccelerate = 1.5f;
        [Range(1f, 10f)] [SerializeField] private float ballTimeToAccelerate;
        public float BallSpeed => _ballSpeed * 10f;
        
        private float _ballSpeed, _ballTimeToAccelerate;
        public float TimeToEuler => ballTimeToEuler * 1f;
        public float BallTimeToRecall => ballTimeToRecall;
        public int BallDamage { get; private set; }
        private int MaxBallCount { get; set; }
        
        private int _currentBallCount, _returnBallCount;
        private bool _isReturn;
        
        [SerializeField] private List<Ball> balls = new List<Ball>();

        [Header("Obstacle")] 
        [Range(0.25f, 1f)] [SerializeField] private float brickTimeMove = 0.5f;
        [SerializeField] private List<Obstacle.Obstacle> obstacles = new List<Obstacle.Obstacle>();
        [SerializeField] private List<Brick> bricks = new List<Brick>();
        [SerializeField] private List<Brick> normalBricks = new List<Brick>();
        [SerializeField] private List<Brick> bottomBricks = new List<Brick>();
        [SerializeField] private List<Item> items = new List<Item>();
        [SerializeField] private List<Item> itemRemoves = new List<Item>();
        public List<Brick> ListBricks => bricks;
        private float _timeBrickEffect;

        [SerializeField] private bool fixTop;
        [SerializeField] private Transform topBounder;
        [SerializeField] private RectTransform top;
        [SerializeField]
        private Vector2 _configPosition = Vector2.zero;

        [Header("Water2D")] 
        [SerializeField] private Water2D_Spawner water_Spawner;
        private Transform _water_Root;
        private IEnumerator _routineWater;

        [Header("Booster UI")] 
        [Space(25)]
        [SerializeField] private bool boosterTrial;
        [Range(10, 50)] [SerializeField] private int boosterBombDamagePercent = 35;
        [SerializeField] private LayerMask brickMask;
        [SerializeField] private bool boosterDouble;
        [SerializeField] private int boosterCounter;
        [SerializeField] private RectTransform btnMoreBall, btnBoosterAim;
        [SerializeField] private RectTransform btnBoosterBomb, btnBoosterDouble, btnBoosterCounter, btnBoosterSaw;
        [SerializeField] private TextMeshProUGUI textBoosterBomb, textBoosterDouble, textBoosterCounter, textBoosterSaw;
        [SerializeField] private RectTransform effectBoosterBomb, effectBoosterSaw;
        [SerializeField] private Transform doubleGun;
        [SerializeField] private GameObject boundaryObject;
        [SerializeField] private SkeletonAnimation counter;
        [SerializeField] private SkeletonGraphic bomb;
        [SerializeField] private AnimationReferenceAsset bombAnim, counterAnim;
        [SerializeField] private TextMeshPro counterText;

        [Header("UI Canvas")] 
        [SerializeField] private InGameCanvasController inGameCanvasController;
        [SerializeField] private GameObject uI_beforeGroup;
        [SerializeField] private GameObject uI_afterGroup;

        [Header("Tutorial")] [SerializeField]
        private Tutorial tutorial;
        private Tutorial _tutorial;

        private void Awake()
        {
            Instance = this;
            GameState = GameState.Began;
            if (GameConfig.Instance == null)
            {
                Utils.CurrentLevel = levelTest - 1;
                Utils.GameMode = gameMode;
            }
            _obstacleLevel = Utils.GameMode == GameMode.Normal
                ? obstacleDataNormal.obstacleLevels[Utils.CurrentLevel]
                : obstacleDataRescue.obstacleLevels[Utils.CurrentLevel];
            GameStep = _obstacleLevel.gameStep;
            RescueState = _obstacleLevel.rescueState;
            if (Utils.GameMode == GameMode.Rescue && RescueState == RescueState.Water)
                CameraState = CameraModeState.Water;
            else
                CameraState = CameraModeState.Normal;
        }
        private void OnDestroy()
        {
            Instance = null;
        }

        private void Start()
        {
            LoadingManager.Instance.AddCallBackLoadData(delegate
            {
                _pool = ObjectPoolManager.Instance;
                Controller_Set_Camera();
                LoadData();
                UI_BeforeGroup_Active();
                _iconNewTransforms.Add(iconNewTransform);
                _iconDoubleTransforms.Add(iconDoubleTransform);
                BallDamage = 1;
                MaxBallCount = 50;
                countBallText.text = "x" + MaxBallCount;
                if (RootManager.Instance.MoreBall)
                {
                    RootManager.Instance.MoreBall = false;
                    btnMoreBall.GetComponent<CanvasGroup>().alpha = 0.6f;
                    countBallText.text = "x" + MaxBallCount + " +10";
                    MaxBallCount += 10;
                }
                _returnBallCount = 0;
                if (Utils.GameMode == GameMode.Normal)
                    bottomBricks = Brick_Get_Bottom_Column();
                Load_Skin_Ball();
                Load_Skin_Theme();
            });
            LoadingManager.Instance.AddCallBackEndLoading(delegate
            {
                var newTop = top.position;
                newTop.y = topBounder.position.y;
                top.position = newTop;
                if (Utils.GameMode == GameMode.Rescue)
                {
                    if (RescueState == RescueState.Water)
                    {
                        _rescueBase.Init(_water_Root);
                    }
                    else 
                        _rescueBase.Init(null);
                    scoreObject.SetActive(false);
                }
                inGameCanvasController.AlignTop();
                inGameCanvasController.Reset = () =>
                {
                    if (_routine != null)
                        StopCoroutine(_routine);
                    GameState = GameState.Over;
                    ResetAllObject();
                    Time.timeScale = 1;
                };
                Booster_Info();
                GameState = GameState.Playing;
                ActionController.Instance.Play_Start(delegate
                {
                    GameState = GameState.Began;
                });
                AudioManager.Instance.Play(Utils.GameMode == GameMode.Normal ? MusicType.InGameNormal : MusicType.InGameRescue);
                if (RootManager.Instance.IsTutorial(0))
                {
                    _tutorial = Instantiate(tutorial);
                    _tutorial.ShowWithPosition(barrelTransform.position);
                }
            });
        }
        
        /// <summary>
        /// //////////
        /// </summary>

        #region Data

        private void LoadData()
        {
            var maxHealth = 0;
            foreach (var obstacleItem in _obstacleLevel.obstacleItems)
            {
                if (obstacleItem.value > maxHealth)
                    maxHealth = obstacleItem.value;
                GameObject obstacle;
                var position = obstacleItem.position + _configPosition;
                // var position = obstacleItem.position;
                if (obstacleItem.obstacleType == ObstacleType.Brick)
                {
                    var brickData = obstacleItem.brickData;
                    var brickType = brickData.brickType;
                    if (brickType == BrickType.Square)
                    {
                        obstacle = _pool.brickSquarePools.GetPooledObject();
                        obstacle.GetComponent<BrickSquare>().SetData(position, GetBrickDataFrom(brickData),
                            obstacleItem.value, obstacleItem.turnDelay);
                    }
                    else if (brickType == BrickType.Triangle)
                    {
                        obstacle = _pool.brickTrianglePools.GetPooledObject();
                        obstacle.GetComponent<BrickTriangle>().SetData(position, GetBrickDataFrom(brickData),
                            obstacleItem.value, obstacleItem.turnDelay);
                    }
                    else
                    {
                        obstacle = _pool.brickStonePools.GetPooledObject();
                        obstacle.GetComponent<Brick>().SetData(position, GetBrickDataFrom(brickData),
                            obstacleItem.value, obstacleItem.turnDelay);
                    }
                }
                else
                {
                    var itemData = obstacleItem.itemData;
                    var itemType = itemData.itemType;
                    if (itemType == ItemType.BarDirection)
                    {
                        obstacle = _pool.itemBarDirectionPools.GetPooledObject();
                        obstacle.GetComponent<ItemBarDirection>()
                            .SetData(position, GetItemDataFrom(itemData), obstacleItem.turnDelay);
                    }
                    else if (itemType == ItemType.GunDirection)
                    {
                        obstacle = _pool.itemGunDirectionPools.GetPooledObject();
                        obstacle.GetComponent<ItemGunDirection>()
                            .SetData(position, GetItemDataFrom(itemData), obstacleItem.turnDelay);
                    }
                    else if (itemType == ItemType.WormHole)
                    {
                        obstacle = _pool.itemWormHoldPools.GetPooledObject();
                        obstacle.GetComponent<ItemWormhole>()
                            .SetData(position, GetItemDataFrom(itemData), obstacleItem.turnDelay);
                    }
                    else if (itemType == ItemType.UpSideAutoDirection)
                    {
                        obstacle = _pool.itemUpSideAutoPools.GetPooledObject();
                        obstacle.GetComponent<ItemAutoDirection>().SetData(position, GetItemDataFrom(itemData),
                            obstacleItem.turnDelay);
                    }
                    else if (itemType == ItemType.LazeTwoDirection)
                    {
                        obstacle = _pool.itemLaze2Pools.GetPooledObject();
                        obstacle.GetComponent<ItemLaze>()
                            .SetData(position, GetItemDataFrom(itemData), obstacleItem.turnDelay);
                    }
                    else if (itemType == ItemType.IncreaseBall)
                    {
                        obstacle = _pool.itemIncreasePools.GetPooledObject();
                        obstacle.GetComponent<ItemIncreaseBall>()
                            .SetData(position, GetItemDataFrom(itemData), obstacleItem.turnDelay);
                    }
                    else if (itemType == ItemType.LazeFourDirection)
                    {
                        obstacle = _pool.itemLaze4Pools.GetPooledObject();
                        obstacle.GetComponent<ItemLaze>()
                            .SetData(position, GetItemDataFrom(itemData), obstacleItem.turnDelay);
                    }
                    else
                    {
                        obstacle = _pool.itemAcousticWavesPools.GetPooledObject();
                        obstacle.GetComponent<ItemAcousticWaves>().SetData(position, GetItemDataFrom(itemData),
                            obstacleItem.turnDelay);
                    }
                }

                obstacle.SetActive(true);
            }
            var count = 0;
            _brickMax = 0;
            foreach (var brick in bricks)
            {
                brick.SetSpriteIcon(maxHealth);
                if (brick.IsStoneBrick) 
                    continue;
                _brickMax++;
                if (brick.IsActive)
                    count++;
            }
            _scoreMax = (float) score * (count * (count + 1)) / 2;
            if (Utils.GameMode == GameMode.Normal)
                return;
            _rescueBase = Instantiate(_rescueBases[Utils.CurrentLevel]);
            _rescueBase.transform.position = _configPosition;
        }

        private BrickData GetBrickDataFrom(BrickData brickData)
        {
            var newBrickData = new BrickData
            {
                brickType = brickData.brickType,
                brickColor = brickData.brickColor,
                brickEffectType = brickData.brickType == BrickType.Square ? brickData.brickEffectType : BrickEffectType.None,
                size = brickData.size,
                rotation = brickData.rotation,
                isKey = brickData.isKey,
                keyState = brickData.keyState
            };
            return newBrickData;
        }

        private ItemData GetItemDataFrom(ItemData itemData)
        {
            var newItemData = new ItemData
            {
                itemType = itemData.itemType,
                rotation = itemData.rotation,
                wormHoleTarget = itemData.wormHoleTarget
            };
            return newItemData;
        }

        #endregion
        
        /// <summary>
        /// //////////
        /// </summary>

        private void Update()
        {
#if UNITY_EDITOR

            if(Input.GetKey(KeyCode.P))
                Debug.Break();
            if(Input.GetKey(KeyCode.K))
                Water_Instance(2f);
            
#endif
            if (GameState == GameState.Began)
            {
                if (_isTouch)
                    Controller_Set_Direction();
            }
            else if (GameState == GameState.Playing)
            {
                if (_timeBrickEffect > 0)
                    _timeBrickEffect -= Time.deltaTime;
                if (_ballTimeToAccelerate <= 0)
                    return;
                _ballTimeToAccelerate -= Time.deltaTime;
                if (_ballTimeToAccelerate <= 0)
                {
                    _ballSpeed *= ballAccelerate;
                }
            }
        }

        /// <summary>
        /// /////////
        /// </summary>

        #region Controller

        private void Controller_Set_Camera()
        {
            if (CameraState == CameraModeState.Normal)
            {
                _mainCamera = normalCamera.GetCameraActive();
                waterCamera.DisableCamera();
            }
            else
            {
                _mainCamera = waterCamera.GetCameraActive();
                normalCamera.DisableCamera();
            }
            var cameraSize = _obstacleLevel.cameraSize;
            var screen = GameConfig.ScreenSize;
            var unitPixel = cameraSize / screen.x;
            var clamp = 0.5f * unitPixel * screen.y;
            _mainCamera.orthographicSize = clamp;
            cameraCanvas.orthographicSize = clamp;
            
            var ratio = clamp / GameConfig.RatioScaleScreen;
            bounder.localScale = Vector3.one * ratio;
            
            //
            var compare = (topBounder.position.y + Mathf.Abs(boundaryObject.transform.position.y));
            var intCompare = Mathf.RoundToInt(compare);
            //
            if (fixTop)
            {
                if (intCompare % 2 == 0)
                {
                    if (intCompare > compare)
                        intCompare -= 1;
                    else
                        intCompare += 1;
                }

                var moveRange = intCompare - compare;
                if (Mathf.Abs(moveRange) > 0)
                {
                    var topPos = topBounder.position;
                    topPos.y += moveRange;
                    topBounder.position = topPos;
                }
            }

            compare = ratio * 0.625f;
            intCompare = Mathf.RoundToInt(compare);
            _configPosition.y = (intCompare - compare);

            //
            var pos = barrelTransform.position;
            var range = countBallText.transform.position.y - pos.y;
            pos.y = boundaryObject.transform.position.y + 0.15f;
            barrelTransform.position = iconTransform.position = pos;
            pos.y += range;
            countBallText.transform.position = pos;
            
            var scaleSize = ratio / 10;
            var bgSize = background.size;
            bgSize *= scaleSize;
            background.size = bgSize;
            if (CameraState != CameraModeState.Water) 
                return;
            waterCamera.ExtraCamera.orthographicSize = _mainCamera.orthographicSize;
            var scale = quadWater.localScale;
            scale *= scaleSize;
            quadWater.localScale = scale;
            pos = quadWater.position;
            pos.y += Mathf.Abs(_configPosition.y);
            quadWater.position = pos;
            _water_Root = water_Spawner.transform;
        }

        private IEnumerator Controller_Instance_Ball()
        {
            _isReturn = false;
            var rate = timeFireRate / BallSpeed;
            if (rate < 0.05f)
                rate = 0.05f;
            var waitForSecond = Yield.GetTime(rate);
            for (var i = 0; i < _currentBallCount; i++)
            {
                if (GameState != GameState.Playing)
                    break;
                Ball_Instance(_direction, i == 0);
                if (boosterDouble)
                {
                    i++;
                    Ball_Instance(_doubleDirection);
                }

                yield return waitForSecond;
            }
            _ballTimeToAccelerate = ballTimeToAccelerate;
            iconOldTransform.gameObject.SetActive(false);
        }

        private void Controller_Check_Auto_Step()
        {
            if (Utils.GameMode == GameMode.Normal)
            {
                Obstacle_Step_Update();
                var maxHealth = 0;
                foreach (var brick in bricks)
                {
                    if (brick.CurrentHp > maxHealth)
                        maxHealth = brick.CurrentHp;
                }
                foreach (var brick in bricks)
                {
                    brick.SetSpriteIcon(maxHealth);
                }
                Controller_Check_State();
            }
            else
            {
                _rescueBase.Check_Step(Controller_Check_State);
            }
        }

        public void Rescue_Key_Brick(KeyState keyState)
        {
            if (GameState != GameState.Playing) 
                return;
            GameState = keyState == KeyState.Lose ? GameState.Over : GameState.Ended;
            if (_routine != null)
                StopCoroutine(_routine);
            else 
                iconOldTransform.gameObject.SetActive(true);
            foreach (var ball in balls)
            {
                ball.StopAndDisable();
            }
            balls.Clear();
            if (keyState == KeyState.Win)
            {
                AudioManager.Instance.Play(SoundType.RescueDone);
                _rescueBase.State_Win();
            }
            else
                _rescueBase.State_Lose();
            if (uI_afterGroup.activeSelf)
                uI_afterGroup.SetActive(false);
        }

        private void Obstacle_Step_Update()
        {
            if (itemRemoves.Count > 0)
            {
                foreach (var item in itemRemoves)
                {
                    item.Remove();
                }
                itemRemoves.Clear();
            }
            for (var i = obstacles.Count - 1; i >= 0; i--)
            {
                obstacles[i].UpdateStep(brickTimeMove);
            }
        }

        private void Controller_Check_State()
        {
            MyTween.Instance.MyTween_Float(brickTimeMove + 0.2f, delegate
            {
                var check = Utils.GameMode == GameMode.Rescue;
                if (!check)
                {
                    foreach (var brick in normalBricks)
                    {
                        if (!brick.IsActive)
                            continue;
                        check = true;
                        break;
                    }
                }
                if (check)
                {
                    if (GameState != GameState.Over)
                    {
                        GameState = GameState.Began;
                        Ball_Speed_Reset();
                        bottomBricks = Brick_Get_Bottom_Column();
                    }
                    else
                    {
                        Invoke(nameof(State_Game_Over), 1f);
                    }
                }
                else 
                    Controller_Check_Auto_Step();
            });
        }
        
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        #region Event_Pointer

        public void Controller_Point_Down()
        {
            if (GameState != GameState.Began) 
                return;
            if (RootManager.Instance.IsTutorial(0))
            {
                if (_tutorial != null)
                {
                    Destroy(_tutorial.gameObject);
                    _tutorial = null;
                }
            }
            _isTouch = true;
            _oldCount = 0;
            Ball_Speed_Reset();
            directionTransform.gameObject.SetActive(true);
            _isCancel = false;
            controller_Btn_Cancel.SetActive(true);
            controller_Btn_Cancel.transform.GetChild(1).gameObject.SetActive(false);
        }

        public void Controller_Point_Up()
        {
            if (!_isTouch) 
                return;
            _isTouch = false;
            directionTransform.gameObject.SetActive(false);
            iconNewTransform.gameObject.SetActive(false);
            iconNewTransform.position = iconOldTransform.position;
            iconNewTransform.localScale = _sizeOrigin;
            if (boosterTrial)
            {
                for (var i = 1; i < _iconNewTransforms.Count; i++)
                {
                    _iconNewTransforms[i].gameObject.SetActive(false);
                }
            }

            if (boosterDouble)
            {
                directionDoubleTransform.gameObject.SetActive(false);
                foreach (var icon in _iconDoubleTransforms)
                {
                    icon.gameObject.SetActive(false);
                }
            }
            
            controller_Btn_Cancel.SetActive(false);
            if (_isCancel)
            {
                _isCancel = false;
                controller_Btn_Cancel.transform.GetChild(1).gameObject.SetActive(false);
                return;
            }

            GameState = GameState.Playing;
            _brickBreak = 0;
            if (boosterTrial)
                Booster_Event_Aim();
            UI_AfterGroup_Active();
            _currentBallCount = boosterDouble ? MaxBallCount * 2 : MaxBallCount;
            countBallText.text = "";
            _routine = Controller_Instance_Ball();
            StartCoroutine(_routine);
        }

        public void Controller_Point_Enter()
        {
            _isCancel = true;
            controller_Btn_Cancel.transform.GetChild(1).gameObject.SetActive(true);
        }

        public void Controller_Point_Exit()
        {
            _isCancel = false;
            controller_Btn_Cancel.transform.GetChild(1).gameObject.SetActive(false);
        }

        private void Controller_Set_Direction()
        {
            Vector2 origin = barrelTransform.position;
            Vector2 mousePosition = _mainCamera.ScreenToWorldPoint(Input.mousePosition);
            var startDirection = mousePosition - origin;
            startDirection.Normalize();
            var hit = Physics2D.Raycast(origin, origin + startDirection * 100, wallMask);
            var fireHitPoint = hit.point;
            var edge = Mathf.Sin(edgeFire / Mathf.Rad2Deg);
            var maxFire = origin.y + Vector2.Distance(fireHitPoint, origin) * edge;
            if (fireHitPoint.y < maxFire)
            {
                fireHitPoint.y = maxFire;
                // return;
            }

            startDirection = fireHitPoint - origin;
            startDirection.Normalize();
            
            // set direction

            if (!directionTransform.gameObject.activeSelf)
                directionTransform.gameObject.SetActive(true);
            var newDirection = _direction = startDirection;
            const int rangeRay = 2;
            var maxCount = 0;
            var listPos = new List<Vector2>();

            var max = boosterTrial ? 3 : 1;

            if (!boosterTrial && _iconNewTransforms.Count > 1)
                for (var i = 1; i < _iconNewTransforms.Count; i++)
                {
                    _iconNewTransforms[i].gameObject.SetActive(false);
                }

            var fixedDeltaTime = Time.fixedDeltaTime;
            for (var i = 0; i < max; i++)
            {
                hit = Physics2D.Linecast(origin, (Vector2) origin + newDirection * 100, mask);
                fireHitPoint = hit.point;

                if (i >= _iconNewTransforms.Count)
                {
                    _iconNewTransforms.Add(Instantiate(iconNewTransform, iconNewTransform.parent));
                }

                fireHitPoint -= BallSpeed * 0.5f * fixedDeltaTime * newDirection;
                var tf = _iconNewTransforms[i];
                if (!tf.gameObject.activeSelf)
                {
                    tf.gameObject.SetActive(true);
                    tf.localScale = _sizeForward;
                }

                tf.position = fireHitPoint;
                // fireHitPoint -= BallSpeed * 0.5f * fixedDeltaTime * newDirection;
                hit.point = fireHitPoint;

                var count = Vector2.Distance(origin, fireHitPoint);
                var intCount = Mathf.RoundToInt(count);
//                if (intCount > count)
//                    intCount--;
                intCount *= rangeRay;
                maxCount += intCount;

                for (var j = 0; j < intCount; j++)
                {
                    listPos.Add((Vector2) origin + newDirection * (j + 1) / rangeRay);
                }

                origin = fireHitPoint;

                newDirection = Vector3.Reflect(newDirection, hit.normal);
                newDirection.Normalize();

                if (hit.collider.CompareTag("Boundary"))
                {
                    if (i + 1 < _iconNewTransforms.Count)
                        for (var j = i + 1; j < _iconNewTransforms.Count; j++)
                        {
                            _iconNewTransforms[j].gameObject.SetActive(false);
                        }

                    break;
                }

                if (i < max - 1)
                    continue;
                const int more = 3 * rangeRay;
                for (var j = 0; j < more; j++)
                {
                    listPos.Add((Vector2) origin + newDirection * (j + 1) / rangeRay);
                }

                maxCount += more;
            }

            if (Mathf.Abs(maxCount - _oldCount) > 0)
            {
                var childCount = directionTransform.childCount;
                if (childCount < maxCount)
                {
                    for (var i = 0; i < maxCount; i++)
                    {
                        if (i >= childCount)
                            Instantiate(directionTransform.GetChild(0), directionTransform);
                        directionTransform.GetChild(i).gameObject.SetActive(true);
                    }
                }
                else
                {
                    for (var i = 0; i < childCount; i++)
                    {
                        directionTransform.GetChild(i).gameObject.SetActive(i < maxCount);
                    }
                }
            }

            for (var i = 0; i < maxCount; i++)
            {
                directionTransform.GetChild(i).position = listPos[i];
            }

            _oldCount = maxCount;

            // Double /////////////////////////////////////////////////////////////////////////////////////////////

            if (!boosterDouble)
            {
                if (_iconDoubleTransforms.Count <= 0 || !_iconDoubleTransforms[0].gameObject.activeSelf)
                    return;
                foreach (var icon in _iconDoubleTransforms)
                {
                    icon.gameObject.SetActive(false);
                }
            }
            else
            {
                if (!directionDoubleTransform.gameObject.activeSelf)
                    directionDoubleTransform.gameObject.SetActive(true);

                origin = barrelTransform.position;
                if (mousePosition.x > origin.x)
                    mousePosition.x -= Mathf.Abs(mousePosition.x - origin.x) * 2;
                else if (mousePosition.x < origin.x)
                    mousePosition.x += Mathf.Abs(mousePosition.x - origin.x) * 2;
                _doubleDirection = mousePosition - origin;
                _doubleDirection.Normalize();
                //
                startDirection = _doubleDirection;
                hit = Physics2D.Raycast(origin, origin + startDirection * 100, wallMask);
                fireHitPoint = hit.point;
                maxFire = origin.y + Mathf.Abs(fireHitPoint.x - origin.x) * edge;
                if (fireHitPoint.y < maxFire)
                    fireHitPoint.y = maxFire;
                startDirection = fireHitPoint - origin;
                startDirection.Normalize();
                //
                newDirection = _doubleDirection = startDirection;
                //
                var rota = Quaternion.LookRotation(_direction, Vector3.back);
                rota.x = 0f;
                rota.y = 0f;
                doubleGun.GetChild(1).rotation = rota;
                rota = Quaternion.LookRotation(_doubleDirection, Vector3.back);
                rota.x = 0f;
                rota.y = 0f;
                doubleGun.GetChild(0).rotation = rota;
                //
                maxCount = 0;
                listPos = new List<Vector2>();

                max = boosterTrial ? 3 : 1;
                for (var i = 0; i < max; i++)
                {
                    hit = Physics2D.Linecast(origin, (Vector2) origin + newDirection * 100, mask);
                    fireHitPoint = hit.point;
                    if (i >= _iconDoubleTransforms.Count)
                        _iconDoubleTransforms.Add(Instantiate(iconDoubleTransform, iconDoubleTransform.parent));

                    fireHitPoint -= BallSpeed * 0.5f * fixedDeltaTime * newDirection;
                    var tf = _iconDoubleTransforms[i];
                    if (!tf.gameObject.activeSelf)
                    {
                        tf.gameObject.SetActive(true);
                        tf.localScale = _sizeForward;
                    }
                    tf.position = fireHitPoint;
                    // fireHitPoint -= BallSpeed * 0.2f * fixedDeltaTime * newDirection;
                    hit.point = fireHitPoint;

                    var count = Vector2.Distance(origin, fireHitPoint);
                    var intCount = Mathf.RoundToInt(count);
//                if (intCount > count)
//                    intCount--;
                    intCount *= rangeRay;
                    maxCount += intCount;

                    for (var j = 0; j < intCount; j++)
                    {
                        listPos.Add((Vector2) origin + newDirection * (j + 1) / rangeRay);
                    }

                    origin = fireHitPoint;

                    newDirection = Vector3.Reflect(newDirection, hit.normal);
                    newDirection.Normalize();

                    if (hit.collider.CompareTag("Boundary"))
                    {
                        if (i + 1 < _iconDoubleTransforms.Count)
                            for (var j = i + 1; j < _iconDoubleTransforms.Count; j++)
                            {
                                _iconDoubleTransforms[j].gameObject.SetActive(false);
                            }

                        break;
                    }

                    if (i < max - 1)
                        continue;
                    const int more = 3 * rangeRay;
                    for (var j = 0; j < more; j++)
                    {
                        listPos.Add((Vector2) origin + newDirection * (j + 1) / rangeRay);
                    }

                    maxCount += more;
                }

                if (Mathf.Abs(maxCount - _oldDoubleCount) > 0)
                {
                    var childCount = directionDoubleTransform.childCount;
                    if (childCount < maxCount)
                    {
                        for (var i = 0; i < maxCount; i++)
                        {
                            if (i >= childCount)
                                Instantiate(directionDoubleTransform.GetChild(0), directionDoubleTransform);
                            directionDoubleTransform.GetChild(i).gameObject.SetActive(true);
                        }
                    }
                    else
                    {
                        for (var i = 0; i < childCount; i++)
                        {
                            directionDoubleTransform.GetChild(i).gameObject.SetActive(i < maxCount);
                        }
                    }
                }

                for (var i = 0; i < maxCount; i++)
                {
                    directionDoubleTransform.GetChild(i).position = listPos[i];
                }

                _oldDoubleCount = maxCount;
            }
        }

        #endregion
        
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////

        #endregion

        /// <summary>
        /// /////////
        /// </summary>

        #region Skin

        private SkinBall _skinBall;
        private SkinTheme _skinTheme;

        private void Load_Skin_Ball()
        {
            if (_skinBall ==  RootManager.Instance.CurrentSkinBall)
                return;
            _skinBall = RootManager.Instance.CurrentSkinBall;
            var skin = RootManager.Instance.GetCurrentSkinBall;
            foreach (var ballIcon in ballIcons)
            {
                ballIcon.sprite = skin;
            }
        }

        private void Load_Skin_Theme()
        {
            if (_skinTheme ==  RootManager.Instance.CurrentSkinTheme)
                return;
            _skinTheme = RootManager.Instance.CurrentSkinTheme;
            var skin = (int) _skinTheme;
            var bgSize = background.size;
            background.sprite = backgroundSkins[skin];
            background.size = bgSize;
            var bounderSkin = uiBounderSkins[skin];
            foreach (var uiBounder in uiBounders)
            {
                uiBounder.sprite = bounderSkin;
            }
            var boosterSkin = uiBoosterSkins[skin];
            foreach (var uiBooster in uiBoosters)
            {
                uiBooster.sprite = boosterSkin;
            }
        }

        #endregion

        /// <summary>
        /// /////////
        /// </summary>
        
        #region Ball

        private void Ball_Instance(Vector3 direction, bool isFirstBall = false)
        {
            var ballObject = _pool.ballPools.GetPooledObject();
            ballObject.transform.position = barrelTransform.position;
            var ball = ballObject.GetComponent<Ball>();
            ball.Direction = direction;
            if (isFirstBall)
                ball.IsFirstBall = true;
            ballObject.SetActive(value: true);
            balls.Add(ball);
        }

        private void Ball_Speed_Reset()
        {
            var size = _mainCamera.orthographicSize;
            _ballSpeed = ballSpeed * (0.15f * size + 4f) / 6f;
        }

        public void Ball_Return(Ball ball)
        {
            if (GameState != GameState.Playing)
                return;
            if (!_isReturn)
            {
                _isReturn = true;
                var pos = ball.MyTransform.position;
                pos.y = iconNewTransform.position.y;
                iconNewTransform.position = pos;
                iconNewTransform.gameObject.SetActive(true);
                pos.y = countBallText.transform.position.y;
                countBallText.transform.position = pos;
            }

            var newPos = ball.MyTransform.position;
            newPos.y = iconNewTransform.position.y;
            ball.MyTransform.position = newPos;
            ball.RecallToTarget();
            RemoveActiveBall(ball);
        }

        public void Ball_Destroy(Ball ball)
        {
            RemoveActiveBall(ball, false);
        }

        private void RemoveActiveBall(Ball ball, bool show = true)
        {
            balls.Remove(ball);
            _returnBallCount++;
            if (show)
                countBallText.text = "x" + _returnBallCount;
            if (GameState != GameState.Playing || _returnBallCount < _currentBallCount) 
                return;
            Reset_Info_Ball();
        }

        private void Reset_Info_Ball()
        {
            if (boosterDouble)
                Booster_Event_Double_Shoot();
            _returnBallCount = 0;
            _currentBallCount = MaxBallCount;
            countBallText.text = "x"+ _currentBallCount;
            _ballTimeToAccelerate = 0;
            barrelTransform.position = iconOldTransform.position = iconNewTransform.position;
            iconOldTransform.gameObject.SetActive(true);
            iconNewTransform.gameObject.SetActive(false);
            if (_timeBrickEffect > 0)
            {
                MyTween.Instance.MyTween_Float(_timeBrickEffect, delegate
                {
                    UI_BeforeGroup_Active();
                    UI_Check_Score();
                    Controller_Check_Auto_Step();
                });
            }
            else
            {
                UI_BeforeGroup_Active();
                UI_Check_Score();
                Controller_Check_Auto_Step();
            }
        }

        public void Ball_Increase()
        {
            MaxBallCount++;
        }

        private void RecallAllOnBall()
        {
            if (balls.Count <= 0)
                return;
            foreach (var ball in balls)
            {
                ball.RecallToTarget();
            }
            balls.Clear();
            _returnBallCount = 0;
            _currentBallCount = MaxBallCount;
            countBallText.text = "x"+ _currentBallCount;
            if (boosterTrial)
                Booster_Event_Aim();
            if (boosterDouble)
                Booster_Event_Double_Shoot();
        }

        #endregion

        /// <summary>
        /// /////////
        /// </summary>
        
        #region Brick
        public void Brick_Add(Brick brick)
        {
            bricks.Add(brick);
            if (brick.IsStoneBrick) 
                return;
            obstacles.Add(brick);
            if (Utils.GameMode == GameMode.Normal)
            {
                normalBricks.Add(brick);
            }
        }

        public void Brick_Remove(Brick brick)
        {
            obstacles.Remove(brick);
            bricks.Remove(brick);
            if (Utils.GameMode == GameMode.Normal && !brick.IsStoneBrick)
            {
                bricks.Remove(brick);
                _brickBreak++;
                _scoreCurrent += _brickBreak * 10;
                UI_Set_Score();
            }
            if (bricks.Count > 0)
                return;
            RecallAllOnBall();
            if (Utils.GameMode == GameMode.Rescue)
                return;
            if (GameState == GameState.Over)
                return;
            GameState = GameState.Ended;
            Invoke(nameof(State_Game_Win), 1f);
            if (uI_afterGroup.activeSelf)
                uI_afterGroup.SetActive(false);
        }

        public void Brick_Collision_Boundary()
        {
            if (GameState != GameState.Over)
                GameState = GameState.Over;
        }

        public void Brick_Time_Effect(float timer)
        {
            if (timer > _timeBrickEffect)
                _timeBrickEffect = timer;
        }

        private List<Brick> Brick_Get_Bottom_Column()
        {
            var list = new List<Brick>();
            var min = bricks[0].MinPosition;
            min = bricks.Select(brick => brick.MinPosition).Prepend(min).Min();
            var left = new Vector2(-20, min);
            var right = left;
            right.x = 20;
            var hits = Physics2D.LinecastAll(left, right, brickMask);
            if (hits.Length <= 0) 
                return list;
            list.AddRange(hits.Select(hit => hit.collider.GetComponent<Brick>()));
            return list;
        }
        
        
        #endregion

        /// <summary>
        /// /////////
        /// </summary>

        #region Item

        public void Item_Add(Item item)
        {
            items.Add(item);
            obstacles.Add(item);
        }

        public void Item_Remove(Item item)
        {
            items.Remove(item);
            obstacles.Remove(item);
        }

        public void Item_Add_Remove(Item itemRemove)
        {
            var isExist = false;
            var type = itemRemove.ItemType;
            if (itemRemoves.Count > 0)
            {
                if (itemRemoves.Any(item => item.ItemType == type))
                    isExist = true;
            }
            if (isExist)
                return;
            foreach (var item in items.Where(item => item.ItemType == type))
            {
                itemRemoves.Add(item);
            }
        }

        #endregion
        
        /// <summary>
        /// /////////
        /// </summary>
        
        #region Water2D
        public void Water_Instance(float time, Callback callback = null)
        {
            water_Spawner.LifeTime = 9999999;
            _routineWater = Water_Spawn_With_Time(0f, time, callback);
            StartCoroutine(_routineWater);
        }

        private IEnumerator Water_Spawn_With_Time(float delay = 0f, float loop = 1f, Callback callback = null)
        {
            if (delay > 0f)
                yield return Yield.GetTime(delay);
            water_Spawner.Spawn();
            yield return Yield.GetTime(loop);
            water_Spawner.StopSpawning();
            _routineWater = null;
            callback?.Invoke();
        }

        public void Water_Remove()
        {
            MyTween.Instance.MyTween_Float(0.75f, water_Spawner.Restore);
        }

        #endregion

        /// <summary>
        /// /////////
        /// </summary>

        #region BoosterUI

        private void Booster_Info()
        {
            var item = RootManager.Instance.GetItem(ItemBooster.Bomb);
            textBoosterBomb.text = item > 0 ? item.ToString() : "+";
            item = RootManager.Instance.GetItem(ItemBooster.DoubleGun);
            textBoosterDouble.text = item > 0 ? item.ToString() : "+";
            item = RootManager.Instance.GetItem(ItemBooster.Counter);
            textBoosterCounter.text = item > 0 ? item.ToString() : "+";
            item = RootManager.Instance.GetItem(ItemBooster.Saw);
            textBoosterSaw.text = item > 0 ? item.ToString() : "+";
        }

        public void Booster_More_Ball()
        {
            AudioManager.Instance.Play(SoundType.Click);
            if (btnMoreBall.GetComponent<CanvasGroup>().alpha < 1)
                return;
            btnMoreBall.GetComponent<CanvasGroup>().alpha = 0.6f;
            countBallText.text = "x" + MaxBallCount + " +10";
            MaxBallCount += 10;
        }
        public void Booster_Aim_Shoot()
        {
            AudioManager.Instance.Play(SoundType.Click);
            if (GameState != GameState.Began)
                return;
            if (!boosterTrial)
            {
                inGameCanvasController.Popup_Aim(delegate
                {
                    boosterTrial = true;
                    btnBoosterAim.GetComponent<CanvasGroup>().alpha = 0.6f;
                });
            }
        }

        private void Booster_Event_Aim()
        {
            boosterTrial = false;
            btnBoosterAim.GetComponent<CanvasGroup>().alpha = 1f;
        }

        private void Booster_Buy_More(ItemBooster itemType)
        {
            var item = RootManager.Instance.GetItem(itemType);
            const int value = 1000;
            inGameCanvasController.Popup_BuyBooster(0, value, delegate
            {
                if (RootManager.Instance.Coin >= value)
                {
                    RootManager.Instance.Coin -= value;
                    PopupManager.Instance.ShowCoin();
                    item++;
                    RootManager.Instance.SetItem(itemType, item);
                    Booster_Info();
                }
                else
                {
                    var content = LanguageManager.Instance.Language == Language.Eng
                        ? "NOT ENOUGH COIN"
                        : "KHÔNG ĐỦ TIỀN";
                    DialogManager.Instance.Message_Dialog(content);
                }
            });
        }

        public void Booster_Bomb_All_Brick()
        {
            AudioManager.Instance.Play(SoundType.Click);
            if (GameState != GameState.Began)
                return;
            var item = RootManager.Instance.GetItem(ItemBooster.Bomb);
            if (item > 0)
            {
                var canvasGroup = btnBoosterBomb.GetComponent<CanvasGroup>();
                if (canvasGroup.alpha < 1)
                    return;
                canvasGroup.alpha = 0.6f;
                item--;
                RootManager.Instance.SetItem(ItemBooster.Bomb, item);
                Booster_Info();
                effectBoosterBomb.gameObject.SetActive(true);
                effectBoosterBomb.position = btnBoosterBomb.position;
                bomb.Clear();
                bomb.Initialize(true);
                bomb.AnimationState.SetAnimation(0, bombAnim, false);
                bomb.timeScale = 0;
                MyTween.Instance.MyTween_Float(0.5f, delegate
                {
                    effectBoosterBomb.DOAnchorPos(Vector2.zero, 1f).SetEase(Ease.InOutSine).OnComplete(delegate
                    {
                        bomb.timeScale = 1;
                        MyTween.Instance.MyTween_Float(0.5f, delegate
                        {
                            effectBoosterBomb.gameObject.SetActive(false);
                            var fx = _pool.boosterBombPools.GetPooledObject();
                            fx.transform.position = Vector3.zero;
                            fx.SetActive(true);
                            Booster_Event_Bomb();
                        });
                    });
                });
            }
            else
                Booster_Buy_More(ItemBooster.Bomb);
        }

        private void Booster_Event_Bomb()
        {
            for (var i = bricks.Count - 1; i >= 0; i--)
            {
                var brick = bricks[i];
                var extraHealth = brick.CurrentHp * boosterBombDamagePercent / 100;
                if (extraHealth < 1)
                    extraHealth = 1;
                brick.OnEffectHit(extraHealth, true);
            }
            btnBoosterBomb.GetComponent<CanvasGroup>().alpha = 1f;
        }

        public void Booster_Double_Shoot()
        {
            AudioManager.Instance.Play(SoundType.Click);
            if (GameState != GameState.Began)
                return;
            var item = RootManager.Instance.GetItem(ItemBooster.DoubleGun);
            if (item > 0)
            {
                var canvasGroup = btnBoosterDouble.GetComponent<CanvasGroup>();
                if (canvasGroup.alpha < 1)
                    return;
                canvasGroup.alpha = 0.6f;
                item--;
                RootManager.Instance.SetItem(ItemBooster.DoubleGun, item);
                Booster_Info();
                boosterDouble = true;
                doubleGun.localScale = Vector3.zero;
                doubleGun.gameObject.SetActive(true);
                doubleGun.DOScale(Vector3.one, 0.5f).SetEase(Ease.InSine);
                var c1 = doubleGun.GetChild(0);
                var c2 = doubleGun.GetChild(1);
                c1.eulerAngles = c2.eulerAngles = Vector3.zero;
                c1.DORotate(new Vector3(0, 0, -45), 0.5f).SetEase(Ease.InSine);
                c2.DORotate(new Vector3(0, 0, 45), 0.5f).SetEase(Ease.InSine);
                AudioManager.Instance.Play(SoundType.BoosterDoubleGun);
            }
            else
                Booster_Buy_More(ItemBooster.DoubleGun);
        }

        private void Booster_Event_Double_Shoot()
        {
            boosterDouble = false;
            btnBoosterDouble.GetComponent<CanvasGroup>().alpha = 1f;
            doubleGun.gameObject.SetActive(false);
        }

        public void Booster_Boundary_Counter()
        {
            AudioManager.Instance.Play(SoundType.Click);
            if (GameState != GameState.Began)
                return;
            var item = RootManager.Instance.GetItem(ItemBooster.Counter);
            if (item > 0)
            {
                var canvasGroup = btnBoosterCounter.GetComponent<CanvasGroup>();
                if (canvasGroup.alpha < 1)
                    return;
                canvasGroup.alpha = 0.6f;
                item--;
                RootManager.Instance.SetItem(ItemBooster.Counter, item);
                Booster_Info();
                boundaryObject.tag = "Counter";
                boosterCounter = MaxBallCount;
                counterText.text = "+" + boosterCounter;
                StartCoroutine(CounterPlay());
            }
            else
                Booster_Buy_More(ItemBooster.Counter);
        }

        private IEnumerator CounterPlay()
        {
            AudioManager.Instance.Play(SoundType.BoosterCounter);
            counter.gameObject.SetActive(true);
            counterText.text = "";
            var tf = counter.transform;
            tf.localPosition = Vector2.zero;
            counter.Skeleton.A = 0;
            yield return Yield.EndFrame;
            counter.Skeleton.A = 1;
            counter.AnimationState.SetAnimation(0, counterAnim, false);
            yield return new WaitForSpineAnimationComplete(counter.AnimationState.GetCurrent(0));
            tf.localPosition = new Vector2(0, -0.06f);
            counterText.text = "+" + boosterCounter;
        }

        public void Booster_Event_Counter()
        {
            boosterCounter--;
            counterText.text = "+" + boosterCounter;
            if (boosterCounter > 0) 
                return;
            boundaryObject.tag = "Boundary";
            btnBoosterCounter.GetComponent<CanvasGroup>().alpha = 1;
            counter.gameObject.SetActive(false);
        }
        public void Booster_Saw_Bottom_Brick()
        {
            AudioManager.Instance.Play(SoundType.Click);
            if (GameState != GameState.Began)
                return;
            if (bottomBricks.Count <= 0)
                return;
            var item = RootManager.Instance.GetItem(ItemBooster.Saw);
            if (item > 0)
            {
                var canvasGroup = btnBoosterSaw.GetComponent<CanvasGroup>();
                if (canvasGroup.alpha < 1)
                    return;
                canvasGroup.alpha = 0.6f;
                item--;
                RootManager.Instance.SetItem(ItemBooster.Saw, item);
                Booster_Info();
                
                var min = Vector3.zero;
                var max = Vector3.zero;
                min.y = max.y = bottomBricks[0].Position.y;
                foreach (var brick in bottomBricks)
                {
                    var pos = brick.Position;
                    if (pos.x < min.x)
                        min.x = pos.x;
                    else if (pos.x > max.x)
                        max.x = pos.x;
                }

                min.x += 25;
                max.x += 25;


                effectBoosterSaw.gameObject.SetActive(true);
                effectBoosterSaw.position = btnBoosterSaw.position;
                effectBoosterSaw.DOMove(max, 0.5f).SetEase(Ease.OutSine).SetDelay(0.5f).OnComplete(delegate
                {
                    StartCoroutine(Booster_Event_Saw(0.5f / (bottomBricks.Count)));
                    effectBoosterSaw.DOMove(min, 0.5f).SetEase(Ease.InSine).OnComplete(delegate
                    {
                        effectBoosterSaw.gameObject.SetActive(false);
                    });
                });
                AudioManager.Instance.Play(SoundType.BoosterSaw);
            }
            else
                Booster_Buy_More(ItemBooster.Saw);
        }

        private IEnumerator Booster_Event_Saw(float time)
        {
//            bottomBricks.Reverse();
            var yieldTime = Yield.GetTime(time);
            for (var i = bottomBricks.Count - 1; i >= 0; i--)
            {
                yield return yieldTime;
                var brick = bottomBricks[i];
                brick.OnEffectHit(1000, true);
            }

            if (GameState != GameState.Began) 
                yield break;
            bottomBricks = Brick_Get_Bottom_Column();
            btnBoosterSaw.GetComponent<CanvasGroup>().alpha = 1f;
        }

        #endregion

        /// <summary>
        /// /////////
        /// </summary>

        #region UI

        private void UI_AfterGroup_Active()
        {
            if (uI_beforeGroup.activeSelf)
                uI_beforeGroup.SetActive(false);
            if (!uI_afterGroup.activeSelf)
                uI_afterGroup.SetActive(true);
        }

        private void UI_BeforeGroup_Active()
        {
            if (Utils.GameMode == GameMode.Normal)
            {
                if (!uI_beforeGroup.activeSelf)
                    uI_beforeGroup.SetActive(true);
            }
            else
            {
                if (uI_beforeGroup.activeSelf)
                    uI_beforeGroup.SetActive(false);
            }

            if (uI_afterGroup.activeSelf)
                uI_afterGroup.SetActive(false);
        }

        private void UI_Set_Score()
        {
            scoreText.text = ((int) _scoreCurrent).ToString();
            var ratio = _scoreCurrent / _scoreMax;
            scoreFill.fillAmount = ratio / 0.6f;
            if (ratio > 0.6f)
            {
                if (_star >= 3) 
                    return;
                AudioManager.Instance.Play(SoundType.StarCollect);
                _star = 3;
                stars[2].sprite = starOn;
            }
            else if (ratio > 0.3f)
            {
                if (_star >= 2) 
                    return;
                AudioManager.Instance.Play(SoundType.StarCollect);
                _star = 2;
                stars[1].sprite = starOn;
            }
            else if (ratio > 0)
            {
                if (_star >= 1) 
                    return;
                AudioManager.Instance.Play(SoundType.StarCollect);
                _star = 1;
                stars[0].sprite = starOn;
            }
        }

        private void UI_Check_Score()
        {
            var ratio = (float) _brickBreak / _brickMax;
            if(ratio >= 0.75f)
                ActionController.Instance.Play_Action(ActionType.Amazing);
            else if (ratio >= 0.5f)
                ActionController.Instance.Play_Action(ActionType.Cool);
        }

        public void Button_ReCall_All()
        {
            AudioManager.Instance.Play(SoundType.Click);
            if (GameState != GameState.Playing)
                return;
            if(balls.Count <= 0)
                return;
            if (_routine != null)
                StopCoroutine(_routine);
            foreach (var ball in balls)
            {
                ball.RecallToTarget();
            }
            balls.Clear();
            Reset_Info_Ball();
        }

        public void Button_Pause_Game()
        {
            AudioManager.Instance.Play(SoundType.Click);
            if (RootManager.Instance.IsTutorial(0))
                return;
            if (GameState == GameState.Ended || GameState == GameState.Over)
                return;
            Time.timeScale = 0;
            inGameCanvasController.OnPause(() =>
            {
                Time.timeScale = 1;
            });
        }

        public void Button_Skin_Popup()
        {
            AudioManager.Instance.Play(SoundType.Click);
            if (GameState != GameState.Began)
                return;
            inGameCanvasController.UnActiveRay();
            PopupManager.Instance.Skin_Popup(0, true, () =>
            {
                Load_Skin_Ball();
                Load_Skin_Theme();
                inGameCanvasController.ActiveRay();
            });
        }
        
        #if !UNITY_EDITOR

        private void OnApplicationFocus(bool hasFocus)
        {
            if (hasFocus)
                return;
            Button_Pause_Game();
        }

        private void OnApplicationPause(bool pauseStatus)
        {
            if (!pauseStatus)
                return;
            Button_Pause_Game();
        }
#endif

        #endregion
        
        /// <summary>
        /// /////////
        /// </summary>

        #region State

        public void State_Game_Win()
        {
            AudioManager.Instance.StopMusic();
            if (RootManager.Instance.IsTutorial(0))
                RootManager.Instance.PassTutorial();
            inGameCanvasController.OnWin(_star);
            if (Utils.GameMode == GameMode.Normal)
                RootManager.Instance.OpenCurrentNormalLevel(_star);
            else
                RootManager.Instance.OpenCurrentRescueLevel();
        }

        public void State_Game_Over()
        {
            AudioManager.Instance.StopMusic();
            if (Utils.GameMode == GameMode.Rescue)
                RootManager.Instance.OpenRescueMode = true;
            inGameCanvasController.OnLose();
        }

        private void ResetAllObject()
        {
            if (balls.Count > 0)
            {
                balls.Clear();
                _pool.ballPools.DisableAllObject();
            }
            _pool.DisableAllObject();
            if (Utils.GameMode != GameMode.Rescue)
                return;
            if(RescueState == RescueState.Water)
                water_Spawner.StopSpawning();
        }

        #endregion

        private enum CameraModeState
        {
            Normal,
            Water
        }
    }

    public enum GameMode
    {
        Normal,
        Rescue
    }

    public enum GameStep
    {
        Move,
        Freeze
    }

    public enum RescueState
    {
        Water,
        Puzzle
    }

    public enum GameState
    {
        Began,
        Playing,
        Ended,
        Over
    }
}
