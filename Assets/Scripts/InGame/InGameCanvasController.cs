﻿using System.Collections.Generic;
using Do.Scripts.Tools.Audio;
using Do.Scripts.Tools.Dialog;
using Do.Scripts.Tools.Language;
using Home.LevelRescue;
using Loading;
using Other;
using Popup;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace InGame
{
    public class InGameCanvasController : MonoBehaviour
    {
        public static InGameCanvasController Instance { get; private set; }
        [SerializeField] private RectTransform btnPause, btnHome;
        [SerializeField] private GraphicRaycaster graphicRay;
        [SerializeField] private GameObject gamePlayPopup, pausePopup, finalPopup;
        [SerializeField] private RectTransform previewRoot;
        [SerializeField] private TextMeshProUGUI levelText;
        [SerializeField] private SpineGraphic lose, win;
        [SerializeField] private GameObject btnRestart;
        [SerializeField] private GameObject btnNext;
        [SerializeField] private GameObject giftCoin, giftInsert;
        [SerializeField] private TextMeshProUGUI coinText;

        [Header("Booster")]
        [SerializeField] private GameObject popupBooster;
        [SerializeField] private Image icon;
        [SerializeField] private TextMeshProUGUI valueText;
        [SerializeField] private Button buttonBuy, buttonExit;
        [SerializeField] private List<Sprite> icons;

        [Header("Aim")] 
        [SerializeField] private GameObject popupAim;
        [SerializeField] private Button buttonFree, buttonExitAim;
        
        public Callback Reset;
        private Callback _resumePause;

        private void Awake()
        {
            Instance = this;
        }

        private void OnDestroy()
        {
            Instance = null;
        }

        #region Booster

        public void Popup_BuyBooster(int iconType, int value, Callback callback)
        {
            popupBooster.SetActive(true);
            icon.sprite = icons[iconType];
            valueText.text = value.ToString();
            buttonBuy.onClick.AddListener(() =>
            {
                callback?.Invoke();
                popupBooster.SetActive(false);
                buttonBuy.onClick.RemoveAllListeners();
                buttonExit.onClick.RemoveAllListeners();
            });
            buttonExit.onClick.AddListener(() =>
            {
                popupBooster.SetActive(false);
                buttonBuy.onClick.RemoveAllListeners();
                buttonExit.onClick.RemoveAllListeners();
            });
        }

        public void Popup_Aim(Callback callback)
        {
            popupAim.SetActive(true);
            buttonFree.onClick.AddListener(() =>
            {
                callback?.Invoke();
                popupAim.SetActive(false);
                buttonFree.onClick.RemoveAllListeners();
                buttonExitAim.onClick.RemoveAllListeners();
            });
            buttonExitAim.onClick.AddListener(() =>
            {
                popupAim.SetActive(false);
                buttonFree.onClick.RemoveAllListeners();
                buttonExitAim.onClick.RemoveAllListeners();
            });
        }

        #endregion

        public void AlignTop()
        {
            btnHome.position = btnPause.position;
        }
        public void ActiveRay()
        {
            graphicRay.enabled = true;
            gamePlayPopup.SetActive(true);
        }

        public void UnActiveRay()
        {
            graphicRay.enabled = false;
            gamePlayPopup.SetActive(false);
        }
        public void OnPause(Callback resume)
        {
            pausePopup.SetActive(true);
            DialogManager.Instance.Preview_Map(previewRoot.anchoredPosition);
            _resumePause = resume;
        }

        public void OnWin(int star)
        {
            AudioManager.Instance.Play(SoundType.Win);
            var isEng = LanguageManager.Instance.Language == Language.Eng;
            FinalPopup(isEng);
            win.Play(Utils.GameMode == GameMode.Normal ? star - 1 : 2);
            btnNext.SetActive(true);
            if (Utils.GameMode == GameMode.Normal)
            {
                giftCoin.SetActive(true);
                coinText.text = "+" + RootManager.Instance.Gift_Normal();
            }
            else
            {
                var gift = RootManager.Instance.Gift_Rescue();
                if (gift.giftType == LevelRescueGiftType.Coin)
                {
                    giftCoin.SetActive(true);
                    coinText.text = "+" + gift.giftValue;
                }
                else 
                    giftInsert.SetActive(true);
            }
        }

        public void OnLose()
        {
            AudioManager.Instance.Play(SoundType.Lose);
            var isEng = LanguageManager.Instance.Language == Language.Eng;
            FinalPopup(isEng);
            lose.Play();
            btnRestart.SetActive(true);
            giftCoin.SetActive(true);
            coinText.text = "0";
        }

        private void FinalPopup(bool isEng)
        {
            finalPopup.SetActive(true);
            var content = isEng ? "LEVEL " : "CẤP ĐỘ ";
            levelText.text = content + (Utils.CurrentLevel + 1);
        }

        public void Btn_Setting()
        {
            AudioManager.Instance.Play(SoundType.Click);
            pausePopup.SetActive(false);
            DialogManager.Instance.Disable_Preview_Map();
            DialogManager.Instance.Setting_Dialog(() =>
            {
                pausePopup.SetActive(true);
                DialogManager.Instance.Preview_Map(previewRoot.anchoredPosition);
            });
        }

        public void Btn_Resume()
        {
            AudioManager.Instance.Play(SoundType.Click);
            pausePopup.SetActive(false);
            DialogManager.Instance.Disable_Preview_Map();
            _resumePause?.Invoke();
        }

        public void Btn_BackHome_Request()
        {
            AudioManager.Instance.Play(SoundType.Click);
            var title = LanguageManager.Instance.Language == Language.Eng ? "QUIT GAME?" : "THOÁT GAME?";
            var message = LanguageManager.Instance.Language == Language.Eng? "ARE YOU SURE?": "BẠN CHĂC CHẮN CHỨ?";
            DialogManager.Instance.Yes_No_Dialog(message, title, delegate
            {
                DialogManager.Instance.Disable_Preview_Map();
                Reset?.Invoke();
                RootManager.Instance.Home();
            });
        }

        public void Btn_BackHome()
        {
            AudioManager.Instance.Play(SoundType.Click);
            if (giftInsert.activeSelf)
            {
                AudioManager.Instance.Play(SoundType.NewBuildOpen);
                PopupManager.Instance.Building_Popup(false);
                PopupManager.Instance.Building_Open_Current(delegate
                {
                    Reset?.Invoke();
                    PopupManager.Instance.Building_UnActive();
                    RootManager.Instance.Home();
                });
            }
            else
            {
                Reset?.Invoke();
                RootManager.Instance.Home();
            }
        }

        public void Btn_ResetGame()
        {
            if(pausePopup.activeSelf)
                DialogManager.Instance.Disable_Preview_Map();
            AudioManager.Instance.Play(SoundType.Click);
            Reset?.Invoke();
            RootManager.Instance.PlayGame();
        }

        public void Btn_Next()
        {
            AudioManager.Instance.Play(SoundType.Click);
            if (giftInsert.activeSelf)
            {
                AudioManager.Instance.Play(SoundType.NewBuildOpen);
                PopupManager.Instance.Building_Popup(false);
                PopupManager.Instance.Building_Open_Current(delegate
                {
                    Reset?.Invoke();
                    PopupManager.Instance.Building_UnActive();
                    if (Utils.GameMode == GameMode.Normal && RootManager.Instance.IsCanNext())
                        RootManager.Instance.PlayGame();
                    else 
                        RootManager.Instance.Home();
                });
            }
            else
            {
                Reset?.Invoke();
                if (Utils.GameMode == GameMode.Normal && RootManager.Instance.IsCanNext())
                    RootManager.Instance.PlayGame();
                else 
                    RootManager.Instance.Home();
            }
        }
    }
}
