﻿using System.Collections.Generic;
using UnityEngine;

namespace InGame
{
    public class ObstacleSprites : MonoBehaviour
    {
        public List<Sprite> bricks;
        public List<Sprite> brickEffects;
        public List<Sprite> items;
        public List<Sprite> squareColors;
        public List<Sprite> triangleColors;
    }
}
