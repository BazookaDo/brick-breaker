﻿using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using Do.Scripts.Tools.Audio;
using Do.Scripts.Tools.Pool;
using Do.Scripts.Tools.Text_Effect;
using Do.Scripts.Tools.Tween;
using InGame.Effect;
using Loading;
using Other;
using Popup;
using UnityEngine;

namespace InGame.Obstacle
{
    public class BrickSquare : Brick
    {
        [SerializeField] private SpriteRenderer actionEffect;
        [SerializeField] private List<Sprite> actionEffects;
        [SerializeField] private GameObject block;
        private Sprite _stepBlock, _stepOpen;
        private readonly List<Brick> _bricks = new List<Brick>();
        private readonly List<GameObject> _listFx = new List<GameObject>();
        private bool _isAction;
        
        public override void SetData(Vector2 position, BrickData brickData, int healthValue, int turnDelay)
        {
            base.SetData(position, brickData, healthValue, turnDelay);
            var size = _brickData.size;
            icon.size = effect.size = size;
            var box = (BoxCollider2D) collider2D;
            // box.size = new Vector2(size.x - 0.025f, size.y - 0.025f);
            box.size = size;
            if (block != null)
                block.SetActive(false);
            var brickEffectType = _brickData.brickEffectType;
            if (brickEffectType == BrickEffectType.None)
            {
                actionEffect.gameObject.SetActive(false);
                return;
            }

            size =
                brickEffectType == BrickEffectType.OneSaw
                    ? new Vector2(size.x * 1.44f, size.y * 1.44f)
                    : brickEffectType == BrickEffectType.OneBlock
                        ? new Vector2(size.x * 1.08f, size.y * 1.05f)
                        : size;
            actionEffect.size = size;
            actionEffect.sprite = actionEffects[(int) _brickData.brickEffectType - 1];
            switch (brickEffectType)
            {
                case BrickEffectType.OneBlock:
                    block.tag = "Untagged";
                    block.SetActive(true);
                    break;
                case BrickEffectType.OneSaw:
                    block.tag = "Destroyer";
                    block.SetActive(true);
                    break;
                case BrickEffectType.StepBlock:
                    _stepOpen = actionEffect.sprite;
                    _stepBlock = actionEffects[actionEffects.Count - 1];
                    actionEffect.transform.eulerAngles = Vector3.zero;
                    _brickData.rotation = Vector3.zero;
                    break;
                case BrickEffectType.NuclearBomb:
                    var minSize = Width;
                    if (Height < Width)
                        minSize = Height;
                    if (minSize > 1)
                        minSize--;
                    actionEffect.size = new Vector2(minSize, minSize);
                    _brickData.rotation = Vector3.zero;
                    break;
                case BrickEffectType.TwoDirectionGun:
                    break;
                default:
                    _brickData.rotation = Vector3.zero;
                    break;
            }
            actionEffect.transform.eulerAngles = block.transform.eulerAngles = _brickData.rotation;
            _isAction = false;
        }

        public override void UpdateStep(float speed)
        {
            base.UpdateStep(speed);
            if (_brickData.brickEffectType == BrickEffectType.StepBlock)
            {
                if (IsBlock)
                {
                    IsBlock = false;
                    actionEffect.sprite = _stepOpen;
                    actionEffect.sortingOrder = 1;
                }
                else
                {
                    IsBlock = true;
                    actionEffect.sprite = _stepBlock;
                    actionEffect.sortingOrder = 4;
                }
            }
        }

        protected override void ActiveDisplay()
        {
            base.ActiveDisplay();
            var brickEffectType = _brickData.brickEffectType;
            if (brickEffectType == BrickEffectType.None)
                return;
            actionEffect.gameObject.SetActive(true);
            if (brickEffectType == BrickEffectType.OneBlock || brickEffectType == BrickEffectType.OneSaw)
                block.SetActive(true);
            else if (_brickData.brickEffectType == BrickEffectType.StepBlock)
            {
                if (IsBlock)
                {
                    IsBlock = false;
                    actionEffect.sprite = _stepOpen;
                    actionEffect.sortingOrder = 1;
                }
            }
        }

        protected override void UnActiveDisplay()
        {
            base.UnActiveDisplay();
            var brickEffectType = _brickData.brickEffectType;
            if (brickEffectType == BrickEffectType.None)
                return;
            actionEffect.gameObject.SetActive(false);
            if (brickEffectType == BrickEffectType.OneBlock || brickEffectType == BrickEffectType.OneSaw)
                block.SetActive(false);
        }
        public override void OnBallHit()
        {
            base.OnBallHit();
            if(_brickData.brickEffectType == BrickEffectType.Lighting)
                Lighting();
            if (CurrentHp <= 0 && !_isAction)
            {
                _isAction = true;
                PlayActionEffect();
            }
        }

        public override void OnEffectHit(int damage, bool skip = false)
        {
            base.OnEffectHit(damage, skip);
            if(_brickData.brickEffectType == BrickEffectType.Lighting)
                Lighting();
            if (CurrentHp <= 0 && !_isAction)
            {
                _isAction = true;
                PlayActionEffect();
            }
        }

        private void PlayActionEffect()
        {
            switch (_brickData.brickEffectType)
            {
                case BrickEffectType.Freeze:
                    Freeze();
                    break;
                case BrickEffectType.Fire:
                    Fire();
                    break;
                case BrickEffectType.Poison:
                    Poison();
                    break;
                case BrickEffectType.Rocket:
                    Rocket();
                    break;
                case BrickEffectType.TwoDirectionGun:
                    TwoDirectionGun();
                    break;
                case BrickEffectType.FourDirectionGun:
                    FourDirectionGun();
                    break;
                case BrickEffectType.MiniBomb:
                    MiniBomb();
                    break;
                case BrickEffectType.NuclearBomb:
                    NuclearBomb();
                    break;
                case BrickEffectType.Coin:
                    Coin();
                    break;
            }
        }

        private void Lighting()
        {
            if (_listFx.Count > 0)
            {
                foreach (var fx in _listFx)
                {
                    if (fx.activeSelf)
                        fx.SetActive(false);
                }
                _listFx.Clear();
            }
            if (_bricks.Count > 0)
            {
                for (var i = _bricks.Count - 1; i >= 0; i--)
                {
                    if (_bricks[i].CurrentHp <= 0)
                        _bricks.Remove(_bricks[i]);
                }
            }

            var count = Utils.GameMode == GameMode.Rescue ? 10 : 4;
            if (_bricks.Count < count)
            {
                var dataBrick = GameController.Instance.ListBricks;
                var brickActives = new List<Brick>();
                foreach (var brick in dataBrick)
                {
                    if (Utils.GameMode == GameMode.Rescue)
                    {
                        if (brick.IsSpecialRescue)
                            continue;
                    }
                    if (brick.IsActive && brick.IsDefaultSquareBrick)
                        brickActives.Add(brick);
                }

                if (_bricks.Count > 0)
                {
                    foreach (var brick in _bricks)
                    {
                        brickActives.Remove(brick);
                    }
                }

                var more = count - _bricks.Count;

                if (brickActives.Count > more)
                {
                    var listRandom = new List<int>();
                    for (var i = 0; i < brickActives.Count; i++)
                    {
                        listRandom.Add(i);
                    }

                    for (var i = 0; i < more; i++)
                    {
                        var u = listRandom[Random.Range(0, listRandom.Count)];
                        listRandom.Remove(u);
                        _bricks.Add(brickActives[u]);
                        // brickActives[u].OnEffectHit(1);
                    }
                }
                else if (brickActives.Count > 0)
                {
                    foreach (var brick in brickActives)
                    {
                        _bricks.Add(brick);
                    }
                }
            }

            if (_bricks.Count <= 0)
                return;
            AudioManager.Instance.Play(SoundType.Lighting);
            foreach (var brick in _bricks)
            {
                var fx = ObjectPoolManager.Instance.effectLightingPools.GetPooledObject();
                fx.transform.position = Position;
                fx.GetComponent<EffectSpineAnimationTarget>().SetTarget(brick.Position);
                fx.SetActive(true);
                _listFx.Add(fx);
                brick.OnEffectHit(1);
            }
        }

        private void Freeze()
        {
            var fx = ObjectPoolManager.Instance.effectFreezePools.GetPooledObject();
            fx.transform.position = Position;
            fx.SetActive(true);
            
            var bricks = GameController.Instance.ListBricks;
            var brickNormal = new List<Brick>();
            foreach (var brick in bricks)
            {
                if (brick.IsDefaultSquareBrick && brick.IsActive)
                {
                    brickNormal.Add(brick);
                }
            }
            if (brickNormal.Count > 4)
            {
                GameController.Instance.Brick_Time_Effect(0.5f);
                var listRandom = new List<int>();
                for (var i = 0; i < brickNormal.Count; i++)
                {
                    listRandom.Add(i);
                }
                for (var i = 0; i < 4; i++)
                {
                    var u = listRandom[Random.Range(0, listRandom.Count)];
                    listRandom.Remove(u);
                    brickNormal[u].SetActionEffect(MoreEffect.Freeze);
                    brickNormal[u].OnActionEffect();
                    
                    fx = ObjectPoolManager.Instance.trialFreezePools.GetPooledObject();
                    fx.transform.position = Position;
                    fx.SetActive(true);
                    var fx1 = fx;
                    fx.transform.DOMove(brickNormal[u].Position, 0.4f).SetEase(Ease.OutSine).OnComplete(delegate
                    {
                        fx1.SetActive(false);
                        fx = ObjectPoolManager.Instance.effectFreezePools.GetPooledObject();
                        fx.transform.position = brickNormal[u].Position;
                        fx.SetActive(true);
                    });
                }
            }
            else if(brickNormal.Count > 0)
            {
                GameController.Instance.Brick_Time_Effect(0.5f);
                foreach (var brick in brickNormal)
                {
                    brick.SetActionEffect(MoreEffect.Freeze);
                    brick.OnActionEffect();
                    
                    fx = ObjectPoolManager.Instance.trialFreezePools.GetPooledObject();
                    fx.transform.position = Position;
                    fx.SetActive(true);
                    var fx1 = fx;
                    fx.transform.DOMove(brick.Position, 0.4f).SetEase(Ease.OutSine).OnComplete(delegate
                    {
                        fx1.SetActive(false);
                        fx = ObjectPoolManager.Instance.effectFreezePools.GetPooledObject();
                        fx.transform.position = brick.Position;
                        fx.SetActive(true);
                    });
                }  
            }
        }

        private void Fire()
        {
            var fx = ObjectPoolManager.Instance.effectFirePools.GetPooledObject();
            fx.transform.position = Position;
            fx.SetActive(true);
            
            var bricks = GameController.Instance.ListBricks;
            var brickNormal = new List<Brick>();
            foreach (var brick in bricks)
            {
                if (brick.IsDefaultSquareBrick && brick.IsActive)
                    brickNormal.Add(brick);
            }
            if (brickNormal.Count > 4)
            {
                GameController.Instance.Brick_Time_Effect(0.5f);
                var listRandom = new List<int>();
                for (var i = 0; i < brickNormal.Count; i++)
                {
                    listRandom.Add(i);
                }
                for (var i = 0; i < 4; i++)
                {
                    var u = listRandom[Random.Range(0, listRandom.Count)];
                    listRandom.Remove(u);
                    brickNormal[u].SetActionEffect(MoreEffect.Fire, 5);
                    
                    fx = ObjectPoolManager.Instance.trialFirePools.GetPooledObject();
                    fx.transform.position = Position;
                    fx.SetActive(true);
                    var fx1 = fx;
                    fx.transform.DOMove(brickNormal[u].Position, 0.4f).SetEase(Ease.OutSine).OnComplete(delegate
                    {
                        fx1.SetActive(false);
                        fx = ObjectPoolManager.Instance.effectFirePools.GetPooledObject();
                        fx.transform.position = brickNormal[u].Position;
                        fx.SetActive(true);
                        brickNormal[u].OnActionEffect();
                    });
                }
            }
            else if(brickNormal.Count > 0)
            {
                GameController.Instance.Brick_Time_Effect(0.5f);
                for (var i = brickNormal.Count - 1; i >= 0 ; i--)
                {
                    var brick = brickNormal[i];
                    brick.SetActionEffect(MoreEffect.Fire, 5);
                    
                    fx = ObjectPoolManager.Instance.trialFirePools.GetPooledObject();
                    fx.transform.position = Position;
                    fx.SetActive(true);
                    var fx1 = fx;
                    fx.transform.DOMove(brick.Position, 0.4f).SetEase(Ease.OutSine).OnComplete(delegate
                    {
                        fx1.SetActive(false);
                        fx = ObjectPoolManager.Instance.effectFirePools.GetPooledObject();
                        fx.transform.position = brick.Position;
                        fx.SetActive(true);
                        brick.OnActionEffect();
                    });
                }
            }
        }

        private void Poison()
        {
            var fx = ObjectPoolManager.Instance.effectPoisonPools.GetPooledObject();
            fx.transform.position = Position;
            fx.SetActive(true);

            var bricks = GameController.Instance.ListBricks;
            var brickNormal = new List<Brick>();
            foreach (var brick in bricks)
            {
                if (brick.IsDefaultSquareBrick && brick.IsActive)
                    brickNormal.Add(brick);
            }
            if (brickNormal.Count > 5)
            {
                GameController.Instance.Brick_Time_Effect(0.5f);
                var listRandom = new List<int>();
                for (var i = 0; i < brickNormal.Count; i++)
                {
                    listRandom.Add(i);
                }
                for (var i = 0; i < 5; i++)
                {
                    var u = listRandom[Random.Range(0, listRandom.Count)];
                    listRandom.Remove(u);
                    brickNormal[u].SetActionEffect(MoreEffect.Poison);
                    
                    fx = ObjectPoolManager.Instance.trialPoisonPools.GetPooledObject();
                    fx.transform.position = Position;
                    fx.SetActive(true);
                    var fx1 = fx;
                    fx.transform.DOMove(brickNormal[u].Position, 0.4f).SetEase(Ease.OutSine).OnComplete(delegate
                    {
                        fx1.SetActive(false);
                        fx = ObjectPoolManager.Instance.effectPoisonPools.GetPooledObject();
                        fx.transform.position = brickNormal[u].Position;
                        fx.SetActive(true);
                        brickNormal[u].OnActionEffect();
                    });
                }
            }
            else if(brickNormal.Count > 0)
            {
                GameController.Instance.Brick_Time_Effect(0.5f);
                foreach (var brick in brickNormal)
                {
                    brick.SetActionEffect(MoreEffect.Poison, 0);
                    
                    fx = ObjectPoolManager.Instance.trialPoisonPools.GetPooledObject();
                    fx.transform.position = Position;
                    fx.SetActive(true);
                    var fx1 = fx;
                    fx.transform.DOMove(brick.Position, 0.4f).SetEase(Ease.OutSine).OnComplete(delegate
                    {
                        fx1.SetActive(false);
                        fx = ObjectPoolManager.Instance.effectPoisonPools.GetPooledObject();
                        fx.transform.position = brick.Position;
                        fx.SetActive(true);
                        brick.OnActionEffect();
                    });
                }  
            }
        }

        private void Rocket()
        {
            var origin = Position;
//            var fx = ObjectPoolManager.Instance.effectRocketPools.GetPooledObject();
//            var tf = fx.transform;
//            tf.position = origin;
//            fx.SetActive(true);
            
            var bricks = GameController.Instance.ListBricks;
            var brickActives = new List<Brick>();
            foreach (var brick in bricks)
            {
                if (Utils.GameMode == GameMode.Rescue)
                {
                    if (brick.IsSpecialRescue)
                        continue;
                }
                if (brick.IsStoneBrick)
                    continue;
                if (brick.IsActive)
                    brickActives.Add(brick);
            }
            if (brickActives.Count > 3)
            {
                GameController.Instance.Brick_Time_Effect(0.55f);
                var listRandom = new List<int>();
                for (var i = 0; i < brickActives.Count; i++)
                {
                    listRandom.Add(i);
                }
                for (var i = 0; i < 3; i++)
                {
                    var u = listRandom[Random.Range(0, listRandom.Count)];
                    listRandom.Remove(u);
                    var brickTarget = brickActives[u];
                    var trial = ObjectPoolManager.Instance.trialRocketPools.GetPooledObject();
                    var tf = trial.transform;
                    tf.position = origin;
                    trial.SetActive(true);
                    var target = brickTarget.Position;
                    var direction = origin - target;
                    var rota = Quaternion.LookRotation(direction, Vector3.forward);
                    rota.x = 0f;
                    rota.y = 0f;
                    tf.rotation = rota;
                    var targetFx = ObjectPoolManager.Instance.targetRocketPools.GetPooledObject();
                    targetFx.transform.position = target;
                    targetFx.SetActive(true);
                    tf.DOMove(target, 0.5f).SetEase(Ease.InSine).OnComplete(delegate
                    {
                        trial.SetActive(false);
                        targetFx.SetActive(false);
                        var fx = ObjectPoolManager.Instance.effectRocketPools.GetPooledObject();
                        tf = fx.transform;
                        tf.position = target;
                        fx.SetActive(true);
                        brickTarget.OnEffectHit(1000);
                    });
                }
            }
            else if(brickActives.Count > 0)
            {
                GameController.Instance.Brick_Time_Effect(0.55f);
                for (var i = brickActives.Count - 1; i >= 0 ; i--)
                {
                    var brickTarget = brickActives[i];
                    var trial = ObjectPoolManager.Instance.trialRocketPools.GetPooledObject();
                    var tf = trial.transform;
                    tf.position = origin;
                    trial.SetActive(true);
                    var target = brickTarget.Position;
                    var direction = origin - target;
                    var rota = Quaternion.LookRotation(direction, Vector3.forward);
                    rota.x = 0f;
                    rota.y = 0f;
                    tf.rotation = rota;
                    var targetFx = ObjectPoolManager.Instance.targetRocketPools.GetPooledObject();
                    targetFx.transform.position = target;
                    targetFx.SetActive(true);
                    tf.DOMove(target, 0.5f).SetEase(Ease.InSine).OnComplete(delegate
                    {
                        trial.SetActive(false);
                        targetFx.SetActive(false);
                        var fx = ObjectPoolManager.Instance.effectRocketPools.GetPooledObject();
                        tf = fx.transform;
                        tf.position = target;
                        fx.SetActive(true);
                        brickTarget.OnEffectHit(1000);
                    });
                }
            }
        }

        private void TwoDirectionGun()
        {
            var fx = ObjectPoolManager.Instance.effectCannonPools.GetPooledObject();
            fx.transform.position = Position;
            fx.transform.eulerAngles = _brickData.rotation;
            fx.SetActive(true);
            var start = Position;
            var end = start;
            var z = _brickData.rotation.z;
            if (Mathf.Abs(z % 180) == 0)
            {
                start.x = -20;
                end.x = 20;
            }
            else
            {
                start.y = -25;
                end.y = 25;
            }
            GameController.Instance.Brick_Time_Effect(0.3f);
            MyTween.Instance.MyTween_Float(0.25f, delegate
            {
                var hits =  Physics2D.LinecastAll(start, end, brickMask);
                if (hits.Length <= 0)
                    return;
                for (var i = hits.Length - 1; i >= 0 ; i--)
                {
                    var brick = hits[i].collider.GetComponent<Brick>();
                    if (brick.IsStoneBrick)
                        continue;
                    brick.OnEffectHit(1000);
                }
            });
        }

        private void FourDirectionGun()
        {
            var fx = ObjectPoolManager.Instance.effectCannonPools.GetPooledObject();
            var tf = fx.transform;
            tf.position = Position;
            tf.eulerAngles = Vector3.zero;
            fx.SetActive(true);
            fx = ObjectPoolManager.Instance.effectCannonPools.GetPooledObject();
            tf = fx.transform;
            tf.position = Position;
            tf.eulerAngles = new Vector3(0, 0, 90);
            fx.SetActive(true);
            
            var left = new Vector2(-20, Position.y);
            var right = new Vector2(20, Position.y);
            var up = new Vector2(Position.x, 30);
            var down = new Vector2(Position.x, -30);
            GameController.Instance.Brick_Time_Effect(0.3f);
            MyTween.Instance.MyTween_Float(0.25f, delegate
            {
                var hits = Physics2D.LinecastAll(left, right, brickMask);
                if (hits.Length > 0)
                {
                    foreach (var hit in hits)
                    {
                        var brick = hit.collider.GetComponent<Brick>();
                        if (brick.IsStoneBrick)
                            continue;
                        brick.OnEffectHit(1000);
                    }
                }
                hits = Physics2D.LinecastAll(up, down, brickMask);
                if (hits.Length <= 0)
                    return;
                for (var i = hits.Length - 1; i >= 0 ; i--)
                {
                    var brick = hits[i].collider.GetComponent<Brick>();
                    if (brick.IsStoneBrick)
                        continue;
                    brick.OnEffectHit(1000);
                }
            });
        }

        private void MiniBomb()
        {
            var fx = ObjectPoolManager.Instance.effectMiniBombPools.GetPooledObject();
            fx.transform.position = Position;
            fx.SetActive(true);
            var cols = Physics2D.OverlapBoxAll(Position, new Vector2(2, 2), 0, brickMask);
            if (cols.Length <= 0)
                return;
            for (var i = cols.Length - 1; i >= 0 ; i--)
            {
                var col = cols[i];
                col.GetComponent<Brick>().OnEffectHit(1000, true);
            }
        }

        private void NuclearBomb()
        {
            var fx = ObjectPoolManager.Instance.effectNuclearBombPools.GetPooledObject();
            fx.transform.position = Position;
            fx.SetActive(true);
            GameController.Instance.Brick_Time_Effect(0.8f);
            MyTween.Instance.MyTween_Float(0.75f, delegate
            {
                var bricks = GameController.Instance.ListBricks.ToList();
                for (var i = bricks.Count - 1; i >= 0; i--)
                {
                    var brick = bricks[i];
                    if (brick.IsActive)
                        brick.OnEffectHit(1000, true);
                }
            });
        }

        private void Coin()
        {
            var coin = Random.Range(5, 9);
            RootManager.Instance.Coin += coin;
            PopupManager.Instance.ShowCoin();
            TextEffectManager.Instance.PlayEffectText(coin.ToString(), Position);
        }
    }
}
