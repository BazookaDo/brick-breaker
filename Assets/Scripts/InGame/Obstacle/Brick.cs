﻿using System;
using DG.Tweening;
using Other;
using TMPro;
using UnityEngine;
using System.Collections.Generic;
using Do.Scripts.Tools.Pool;
using Do.Scripts.Tools.Text_Effect;
using Do.Scripts.Tools.Tween;

namespace InGame.Obstacle
{
    public class Brick : Obstacle
    {
        [SerializeField] protected BrickData _brickData;
        private MoreEffect More_Effect { get; set; }
        
        public int CurrentHp
        {
            get => _currentHp;
            set
            {
                _currentHp = value;
                if (text != null)
                    text.text = _currentHp.ToString();
            }
        }
        
        protected bool IsBlock { get; set; }

        private int _levelMaxHealth;

        protected float Height => _brickData.size.y > 0 ? _brickData.size.y : 1;
        protected float Width => _brickData.size.x > 0 ? _brickData.size.x : 1;

        public bool IsActive => _turnDelay <= 0 && gameObject.activeSelf;

        public bool IsSpecialRescue => _brickData.isKey || _brickData.brickEffectType == BrickEffectType.OneSaw;

        public bool IsFreezeBrick => More_Effect == MoreEffect.Freeze;

        private bool IsIdleBrick => _brickData.brickEffectType == BrickEffectType.UnMove;

        public bool IsStoneBrick => _brickData.brickType == BrickType.Stone;

        public bool IsDefaultSquareBrick => _brickData.brickEffectType == BrickEffectType.None;
        public Vector2 Position => _myTransform.position;

        public float MinPosition => Position.y - Height / 2 + 0.5f;

        private Transform _myTransform;
        private int _currentHp, _moreDamage, _moreTurnDamage, _turnDelay;
        [SerializeField] protected SpriteRenderer icon, effect;
        [SerializeField] protected List<Sprite> greenColors, brownColors, mixColors, effects;
        [SerializeField] protected TextMeshPro text;
        [SerializeField] protected Transform iconTransform;
        [SerializeField] protected new Collider2D collider2D;
        [SerializeField] protected LayerMask brickMask, itemMask, boundary, wallMask;
        private Tween _tween;
        private bool _isTween;

        public virtual void SetData(Vector2 position, BrickData brickData, int healthValue, int turnDelay)
        {
            _myTransform = transform;
            _myTransform.position = position;
            _brickData = brickData;
            if (Utils.GameMode == GameMode.Normal)
                _brickData.isKey = false;
            _turnDelay = turnDelay;
            if (_turnDelay > 0)
                UnActiveDisplay();
            else
                ActiveDisplay();
            More_Effect = MoreEffect.None;
            if(!IsStoneBrick)
            {
                _currentHp = healthValue;
                text.text = _currentHp.ToString();
                if (effect.sprite != null)
                    effect.sprite = null;
                if (effect.gameObject.activeSelf)
                    effect.gameObject.SetActive(false);
            }
            else
            {
                var size = Vector2.one;
                icon.size = size;
                var box = (BoxCollider2D) collider2D;
                box.size = size;
            }
            if (IsStoneBrick && _turnDelay <= 0)
                return;
            GameController.Instance.Brick_Add(this);
        }

        protected virtual void ActiveDisplay()
        {
            collider2D.enabled = true;
            iconTransform.gameObject.SetActive(true);
            if (effect != null)
                effect.gameObject.SetActive(true);
            if (text != null)
                text.gameObject.SetActive(true);
        }

        protected virtual void UnActiveDisplay()
        {
            collider2D.enabled = false;
            iconTransform.gameObject.SetActive(false);
            if (text != null)
                text.gameObject.SetActive(false);
        }

        public void SetSpriteIcon(int maxHealth)
        {
            if (IsStoneBrick)
                return;
            _levelMaxHealth = maxHealth;
            if (_brickData.brickColor == BrickColor.AutoGreen || _brickData.brickColor == BrickColor.AutoBrown)
                _brickData.brickColor = _currentHp <= _levelMaxHealth / 2 ? BrickColor.AutoGreen : BrickColor.AutoBrown;
            var color = _brickData.brickColor;
            if (color == BrickColor.AutoGreen)
                UpdateGreenColor();
            else if (color == BrickColor.AutoBrown)
                UpdateBrownColor();
            else
            {
                icon.sprite = mixColors[(int) color - 2];
            }
        }

        private void UpdateGreenColor()
        {
            var ratio = (float) CurrentHp / _levelMaxHealth;
            if (ratio >= 0.34f)
                icon.sprite = greenColors[2];
            else if (ratio >= 0.17f)
                icon.sprite = greenColors[1];
            else
                icon.sprite = greenColors[0];
        }

        private void UpdateBrownColor()
        {
            var ratio = (float) CurrentHp / _levelMaxHealth;
            if (ratio >= 0.85f)
                icon.sprite = brownColors[2];
            else if (ratio >= 0.68f)
                icon.sprite = brownColors[1];
            else if(ratio >= 0.51f)
                icon.sprite = brownColors[0];
            else
            {
                _brickData.brickColor = BrickColor.AutoGreen;
                UpdateGreenColor();
            }
        }

        public override void UpdateStep(float speed)
        {
            base.UpdateStep(speed);
            if (IsActive)
            {
                if (IsStoneBrick)
                    return;
                if (IsDefaultSquareBrick)
                {
                    if (More_Effect == MoreEffect.Freeze)
                    {
                        _moreTurnDamage--;
                        var isPlay = false;
                        if (_moreTurnDamage == 2)
                        {
                            effect.sprite = effects[3];
                            isPlay = true;
                        }
                        else if (_moreTurnDamage == 1)
                        {
                            effect.sprite = effects[4];
                            isPlay = true;
                        }
                        else if (_moreTurnDamage == 0)
                        {
                            isPlay = true;
                            More_Effect = MoreEffect.None;
                            effect.gameObject.SetActive(false);
                        }

                        if (isPlay)
                        {
                            var fx = ObjectPoolManager.Instance.effectFreeze2Pools.GetPooledObject();
                            fx.transform.position = Position;
                            fx.SetActive(true);
                        }

                        return;
                    }

                    if (More_Effect == MoreEffect.Fire)
                    {
                        OnDamage(_moreDamage);
                        TextEffectManager.Instance.PlayEffectText("-" + _moreDamage, Position);
                    }
                }
                else if (IsIdleBrick)
                    return;
                if (CurrentHp <= 0)
                    return;
            }

            var pos = Position;
            var distance = Height / 2 + 0.5f;
            pos.y -= distance;
            var listPos = new List<Vector2>();
            if (Width > 1)
            {
                pos.x -= Width / 2;
                pos.x += 0.5f;
                listPos.Add(pos);
                for (var i = 0; i < Width - 1; i++)
                {
                    pos.x++;
                    listPos.Add(pos);
                }
            }
            else
                listPos.Add(pos);

            distance = 1;
            var oldTurn = Height;
            var turn = oldTurn;

            while (true)
            {
                var move = true;
                var jump = false;
                while (turn > 0)
                {
                    if (isDebug)
                        Debug.Log("Check");
                    var turnMove = false;
                    var turnJump = false;
                    foreach (var target in listPos)
                    {
                        var hit = Physics2D.Linecast(target, target, brickMask);
                        if (hit)
                        {
                            if (hit.collider.GetComponent<Obstacle>().IsMove)
                            {
                                if (isDebug)
                                    Debug.Log("Move");
                                turnMove = true;
                                continue;
                            }
                            HideOnIdle();
                            move = false;
                            break;
                        }

                        hit = Physics2D.Linecast(target, target, itemMask);
                        if (!hit)
                            continue;
                        if (hit.collider.GetComponent<Obstacle>().IsMove)
                            continue;
                        turnJump = true;
                        if (isDebug)
                            Debug.Log("Jump");
                    }

                    if (!move)
                        break;
                    if (turnMove && turnJump)
                    {
                        if (isDebug)
                            Debug.Log("Turn Double");
                        if (!jump)
                        {
                            turn += 2;
                            distance += Height - 1;
                        }
                        else
                        {
                            turn++;
                        }
                        distance += 2;
                        jump = true;
                    }
                    else
                    {
                        if (turnMove)
                        {
                            if (isDebug)
                                Debug.Log("Turn Move");
                            if (jump)
                            {
                                if (isDebug)
                                    Debug.Log("Turn Move Jump");
                                if (turn > 1)
                                {
                                    distance += 1;
                                    turn += 1;
                                }
                            }
                            else 
                            {
                                turn--;
                            }
                        }
                        else if (turnJump)
                        {
                            if (isDebug)
                                Debug.Log("Turn Jump");
                            if (!jump)
                            {
                                distance += Height - 1;
                            }
                            turn += 1;
                            distance += 1;
                            jump = true;
                        }
                        else
                        {
                            if (isDebug)
                                Debug.Log("None");
                            turn--;
                            continue;
                        }
                    }

                    if (jump)
                    {
                        if (!turnMove)
                            turn--;
                    }
                    else
                    {
                        turn--;
                    }

                    if (turn <= 0)
                        break;
                    for (var i = 0; i < listPos.Count; i++)
                    {
                        var newPos = listPos[i];
                        newPos.y--;
                        listPos[i] = newPos;
                    }
                }

                if (move)
                {
                    if (jump)
                    {
                        Move(Position.y - distance, speed);
                    }
                    else
                    {
                        Move(Position.y - 1, speed);
                    }
                }

                break;
            }
        }

        private void Move(float target, float speed)
        {
            if (_turnDelay > 0)
            {
                collider2D.enabled = true;
            }
            IsMove = true;
            _myTransform.DOMoveY(target, speed).OnComplete(delegate
            {
                IsMove = false;
                if (_turnDelay > 0)
                {
                    _turnDelay--;
                    if (_turnDelay <= 0)
                    {
                        ActiveDisplay();
                        if (IsStoneBrick)
                            GameController.Instance.Brick_Remove(this);
                        else
                        {
                            if (Physics2D.Linecast(Position, Position, wallMask))
                            {
                                gameObject.SetActive(false);
                                GameController.Instance.Brick_Remove(this);
                            }
                        }
                    }
                    else
                        collider2D.enabled = false;
                }
                var pos = Position;
                pos.y -= Height / 2;
                if (!Physics2D.Linecast(Position, pos, boundary)) 
                    return;
//                gameObject.SetActive(false);
                GameController.Instance.Brick_Collision_Boundary();
//                GameController.Instance.Brick_Remove(this);
            });
        }

        private void HideOnIdle()
        {
            if (_turnDelay <= 0)
                return;
            collider2D.enabled = true;
            MyTween.Instance.MyTween_Float(0.2f, delegate
            {
                _turnDelay--;
                if (_turnDelay <= 0)
                {
                    ActiveDisplay();
                    if (IsStoneBrick)
                        GameController.Instance.Brick_Remove(this);
                    else
                    {
                        if (Physics2D.Linecast(Position, Position, wallMask))
                        {
                            gameObject.SetActive(false);
                            GameController.Instance.Brick_Remove(this);
                        }
                    }
                }
                else
                    collider2D.enabled = false;
            });
        }

        public void SetActionEffect(MoreEffect moreEffect, int damage = 0)
        {
            if (moreEffect != MoreEffect.None)
                More_Effect = moreEffect;
            _moreDamage = damage;
        }

        public void OnActionEffect()
        {
            if (!effect.gameObject.activeSelf)
                effect.gameObject.SetActive(true);
            var size = effect.size;
            effect.sprite = effects[(int) More_Effect - 1];
            if (More_Effect == MoreEffect.Fire)
            {
                OnDamage(_moreDamage);
                if (Width > 1 || Height > 1)
                    effect.drawMode = SpriteDrawMode.Tiled;
                else
                    effect.drawMode = SpriteDrawMode.Simple;
                effect.size = size;
                return;
            }
            if(More_Effect == MoreEffect.Freeze)
                _moreTurnDamage = 4;
            if (Width > 1 || Height > 1)
                effect.drawMode = SpriteDrawMode.Sliced;
            else
                effect.drawMode = SpriteDrawMode.Simple;
            effect.size = size;
        }

        public virtual void OnBallHit()
        {
            if (IsStoneBrick)
                return;
            Play_Effect_Hit();
            var damage = More_Effect == MoreEffect.Poison
                ? GameController.Instance.BallDamage * 2
                : GameController.Instance.BallDamage;
            OnDamage(damage);
        }

        public virtual void OnEffectHit(int damage, bool skip = false)
        {
            if (!skip)
            {
                if (IsStoneBrick)
                    return;
            }
            if (More_Effect == MoreEffect.Poison)
                damage *= 2;
            OnDamage(damage, skip);
        }

        private void OnDamage(int damage, bool skip = false)
        {
            if (!gameObject.activeSelf)
                return;
            if (!skip)
            {
                if (IsBlock)
                    return;
            }

            CurrentHp -= damage;
            if (CurrentHp > 0)
            {
                var color = _brickData.brickColor;
                if (color == BrickColor.AutoGreen)
                    UpdateGreenColor();
                else if (color == BrickColor.AutoBrown)
                    UpdateBrownColor();
                if (_isTween)
                {
                    _isTween = false;
                    _tween.Kill();
                }
                Play_Animation_Hit();
            }
            else
            {
                Play_Effect_Break();
                gameObject.SetActive(false);
                GameController.Instance.Brick_Remove(this);
                if (Utils.GameMode == GameMode.Rescue && _brickData.isKey)
                {
                    GameController.Instance.Rescue_Key_Brick(_brickData.keyState);
                }
            }
        }

        private void Play_Animation_Hit()
        {
            _isTween = true;
            _tween = iconTransform.DOScale(new Vector3(0.95f, 0.95f, 1f), 0.1f).OnComplete(delegate
            {
                _tween = iconTransform.DOScale(Vector3.one, 0.1f).OnComplete(delegate
                {
                    _isTween = false;
                    _tween = null;
                });
            });
        }

        private void Play_Effect_Hit()
        {
            
        }

        private void Play_Effect_Break()
        {
            var fx = ObjectPoolManager.Instance.effectBrickBreakPools.GetPooledObject();
            fx.transform.position = Position;
            fx.SetActive(true);
        }
        
        public enum MoreEffect
        {
            None,
            Freeze,
            Fire,
            Poison
        }
    }

    [Serializable]
    public class BrickData
    {
        public BrickType brickType;
        public BrickColor brickColor;
        public BrickEffectType brickEffectType;
        public Vector2 size;
        public Vector3 rotation;
        public bool isKey;
        public KeyState keyState;
    }

    public enum BrickType
    {
        Square,
        Triangle,
        Stone
    }

    public enum BrickColor
    {
        AutoGreen,
        AutoBrown,
        White,
        Yellow,
        Gray,
        Blue,
        Purple,
        Pink,
        Red,
        Orange
    }

    public enum BrickEffectType
    {
        None,
        OneBlock,
        StepBlock,
        UnMove,
        Lighting,
        Freeze,
        Fire,
        Poison,
        Rocket,
        TwoDirectionGun,
        FourDirectionGun,
        MiniBomb,
        NuclearBomb,
        OneSaw,
        Coin
    }
    
    public enum KeyState
    {
        Win,
        Lose
    }
}
