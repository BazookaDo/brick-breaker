﻿using UnityEngine;

namespace InGame.Obstacle
{
    public class ItemAutoDirection : Item
    {
        [Range(1f, 5f)] [SerializeField] private float duration = 2.5f;

        private float _min, _max;
        private Vector2 _position, _target, _direction;
        private bool _isAction;
        public override void SetData(Vector2 position, ItemData itemData, int turnDelay)
        {
            base.SetData(position, itemData, turnDelay);
            _position = position;
            _target.y = _position.y + 0.5f;
            var _duration = duration / 10;
            _min = _position.x - _duration;
            _max = _position.x + _duration + 0.01f;
            _isAction = false;
        }

        protected override void OnHit(Ball ball)
        {
            base.OnHit(ball);
            _target.x = Random.Range(_min, _max);
            _direction = _target - _position;
            _direction.Normalize();
            ball.MyTransform.position = Position;
            ball.Direction = _direction;
            if (_isAction)
                return;
            _isAction = true;
            GameController.Instance.Item_Add_Remove(this);
        }
    }
}
