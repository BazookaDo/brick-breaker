﻿
using Do.Scripts.Tools.Text_Effect;

namespace InGame.Obstacle
{
    public class ItemIncreaseBall : Item
    {
        protected override void OnHit(Ball ball)
        {
            if (!gameObject.activeSelf)
                return;
            gameObject.SetActive(false);
            GameController.Instance.Item_Remove(this);
            GameController.Instance.Ball_Increase();
            TextEffectManager.Instance.PlayEffectText("+1", Position);
        }
    }
}
