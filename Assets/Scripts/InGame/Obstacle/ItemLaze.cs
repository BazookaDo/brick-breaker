﻿using System;
using System.Collections.Generic;
using Do.Scripts.Tools.Pool;
using InGame.Effect;
using Other;
using UnityEngine;

namespace InGame.Obstacle
{
    public class ItemLaze : Item
    {
        private Callback Shot;

        private Vector2 _up, _down, _right, _left;
        private bool _isFire;
        [SerializeField]
        private List<Brick> bricks;
        private bool _isAction;
        private EffectSpineAnimation2 _effect, _effectDouble;

        private float _duration;
        public override void SetData(Vector2 position, ItemData itemData, int turnDelay)
        {
            _effect = null;
            _effectDouble = null;
            base.SetData(position, itemData, turnDelay);
            if (_itemData.itemType == ItemType.LazeTwoDirection)
            {
                iconTransform.eulerAngles = _itemData.rotation;
                Shot = TwoDirection;
            }
            else
                Shot = FourDirection;
            _isAction = false;
            _isFire = false;
            bricks = new List<Brick>();
        }

        public override void UpdateStep(float speed)
        {
            base.UpdateStep(speed);
            _isFire = false;
            bricks = new List<Brick>();
        }

        private void Update()
        {
            if (_duration <= 0)
                return;
            _duration -= Time.deltaTime;
        }

        private void TwoDirection()
        {
            if (_effect != null)
            {
                if (_effect.IsActive)
                {
                    if (_duration <= 0)
                    {
                        _duration = 0.25f;
                        _effect.RebindAnim();
                    }
                }
                else
                    _effect = null;
            }
            if (_effect == null)
            {
                _duration = 0.2f;
                var fx = ObjectPoolManager.Instance.effectLazePools.GetPooledObject();
                var z = _itemData.rotation.z;
                fx.transform.eulerAngles = Mathf.Abs(z % 180) == 0 ? Vector3.zero : new Vector3(0, 0, -90);
                fx.transform.position = Position;
                fx.SetActive(true);
                _effect = fx.GetComponent<EffectSpineAnimation2>();
            }
            if (!_isFire)
            {
                _isFire = true;
                RaycastHit2D[] hits;
                var z = _itemData.rotation.z;
                var position = Position;
                if (Mathf.Abs(z % 180) == 0)
                {
                    _up.x = _down.x = position.x;
                    _up.y = 20;
                    _down.y = -20;
                    hits = Physics2D.LinecastAll(_up, _down,  brickMask);
                    if (hits.Length > 0)
                    {
                        foreach (var hit in hits)
                        {
                            var brick = hit.collider.GetComponent<Brick>();
                            if (Utils.GameMode == GameMode.Rescue)
                            {
                                if (brick.IsSpecialRescue)
                                    continue;
                            }

                            bricks.Add(brick);
                        }
                    }
                }
                else
                {
                    _left.y = _right.y = position.y;
                    _left.x = -20;
                    _right.x = 20;
                    hits  = Physics2D.LinecastAll(_left, _right, brickMask);
                    if (hits.Length > 0)
                    {
                        foreach (var hit in hits)
                        {
                            var brick = hit.collider.GetComponent<Brick>();
                            if (Utils.GameMode == GameMode.Rescue)
                            {
                                if (brick.IsSpecialRescue)
                                    continue;
                            }

                            bricks.Add(brick);
                        }
                    }
                }
            }

            if (bricks.Count <= 0)
                return;
            for (var i = bricks.Count - 1; i >= 0; i--)
            {
                var brick = bricks[i];
                brick.OnEffectHit(1);
            }
        }

        private void FourDirection()
        {
            if (_effect != null)
            {
                if (_effect.IsActive)
                {
                    if (_duration <= 0)
                    {
                        _duration = 0.25f;
                        _effect.RebindAnim();
                        _effectDouble.RebindAnim();
                    }
                }
                else
                {
                    _effect = null;
                    _effectDouble = null;
                }
            }
            if (_effect == null)
            {
                _duration = 0.2f;
                var fx = ObjectPoolManager.Instance.effectLazePools.GetPooledObject();
                var tf = fx.transform;
                tf.eulerAngles = Vector3.zero;
                tf.position = Position;
                fx.SetActive(true);
                _effect = fx.GetComponent<EffectSpineAnimation2>();
                
                fx = ObjectPoolManager.Instance.effectLazePools.GetPooledObject();
                var tf2 = fx.transform;
                tf2.eulerAngles = new Vector3(0, 0, -90);
                tf2.position = Position;
                fx.SetActive(true);
                _effectDouble = fx.GetComponent<EffectSpineAnimation2>();
            }

            if (!_isFire)
            {
                _isFire = true;
                var position = Position;
                _up.x = _down.x = position.x;
                _up.y = 20;
                _down.y = -20;
                var hits = Physics2D.LinecastAll(_up, _down, brickMask);
                if (hits.Length > 0)
                {
                    foreach (var hit in hits)
                    {
                        var brick = hit.collider.GetComponent<Brick>();
                        if (Utils.GameMode == GameMode.Rescue)
                        {
                            if (brick.IsSpecialRescue)
                                continue;
                        }

                        bricks.Add(brick);
                    }
                }

                _left.y = _right.y = position.y;
                _left.x = -20;
                _right.x = 20;
                hits = Physics2D.LinecastAll(_left, _right, brickMask);
                if (hits.Length > 0)
                {
                    foreach (var hit in hits)
                    {
                        var brick = hit.collider.GetComponent<Brick>();
                        if (Utils.GameMode == GameMode.Rescue)
                        {
                            if (brick.IsSpecialRescue)
                                continue;
                        }

                        bricks.Add(brick);
                    }
                }
            }

            if (bricks.Count <= 0)
                return;
            for (var i = bricks.Count - 1; i >= 0; i--)
            {
                var brick = bricks[i];
                brick.OnEffectHit(1);
            }
        }

        protected override void OnHit(Ball ball)
        {
            base.OnHit(ball);
            Shot.Invoke();
            if (_isAction)
                return;
            _isAction = true;
            GameController.Instance.Item_Add_Remove(this);
        }
    }
}
