﻿using System.Collections.Generic;
using UnityEngine;

namespace InGame.Obstacle
{
    public class ItemWormhole : Item
    {
        [SerializeField] private Transform target;
        [SerializeField] private List<Ball> _balls = new List<Ball>();
        private bool _isAction;
        
        public override void SetData(Vector2 position, ItemData itemData, int turnDelay)
        {
            base.SetData(position, itemData, turnDelay);
            target.position = _itemData.wormHoleTarget;
            _isAction = false;
            _balls.Clear();
        }

        protected override void OnHit(Ball ball)
        {
            base.OnHit(ball);
            if (_balls.Contains(ball))
                return;
            _balls.Add(ball);
            ball.MyTransform.position = target.position;
            if (_isAction)
                return;
            _isAction = true;
            GameController.Instance.Item_Add_Remove(this);
        }
    }
}
