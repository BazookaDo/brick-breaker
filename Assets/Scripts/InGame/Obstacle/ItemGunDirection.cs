﻿using UnityEngine;

namespace InGame.Obstacle
{
    public class ItemGunDirection : Item
    {
        [SerializeField] private float speed;
        [SerializeField] private Transform direction;
        private float _currentSpeed;
        private Vector3 _rotation;
        private bool _isAction;
        public override void SetData(Vector2 position, ItemData itemData, int turnDelay)
        {
            base.SetData(position, itemData, turnDelay);
            if (_itemData.rotation.z < 0)
                _currentSpeed = -speed;
            else
                _currentSpeed = speed;
            iconTransform.eulerAngles = _rotation = Vector3.zero;
        }

        private void Update()
        {
            if (!IsActive)
                return;
            _rotation.z += _currentSpeed * Time.deltaTime;
            iconTransform.eulerAngles = _rotation;
        }

        protected override void OnHit(Ball ball)
        {
            base.OnHit(ball);
            var position = direction.position;
            ball.MyTransform.position = position;
            var dir = position - (Vector3) Position;
            dir.Normalize();
            ball.Direction = dir;
            
        }
    }
}
