﻿using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace InGame.Obstacle
{
    public class BrickTriangle : Brick
    {
        public override void SetData(Vector2 position, BrickData brickData, int healthValue, int turnDelay)
        {
            base.SetData(position, brickData, healthValue, turnDelay);
            transform.eulerAngles = _brickData.rotation;
            text.transform.eulerAngles = Vector3.zero;
        }
    }
}
