﻿using System;
using UnityEngine;

namespace InGame.Obstacle
{
    public class ItemBarDirection : Item
    {
        [SerializeField] private float speed;
        private float _currentSpeed;
        private Vector3 _rotation;
        public override void SetData(Vector2 position, ItemData itemData, int turnDelay)
        {
            base.SetData(position, itemData, turnDelay);
            if (_itemData.rotation.z < 0)
                _currentSpeed = -speed;
            else
                _currentSpeed = speed;
            iconTransform.eulerAngles = _rotation = Vector3.zero;
        }

        private void Update()
        {
            if (!IsActive)
                return;
            _rotation.z += _currentSpeed * Time.deltaTime;
            iconTransform.eulerAngles = _rotation;
        }
    }
}
