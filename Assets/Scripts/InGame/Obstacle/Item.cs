﻿using System;
using DG.Tweening;
using UnityEngine;

namespace InGame.Obstacle
{
    public class Item : Obstacle
    {
        private const string BallTag = "Ball";
        [SerializeField] protected ItemData _itemData;

        public ItemType ItemType => _itemData.itemType;

        private bool IsFreezeItem => _itemData.itemType == ItemType.BarDirection ||
                                     _itemData.itemType == ItemType.GunDirection ||
                                     _itemData.itemType == ItemType.WormHole; 

        private int _turnDelay;
        private Transform _myTransform;
        protected Vector2 Position => _myTransform.position;
        protected bool IsActive => _turnDelay <= 0 && gameObject.activeSelf;
        
        [SerializeField] protected Transform iconTransform;
        [SerializeField] protected new Collider2D collider2D;
        [SerializeField] protected LayerMask brickMask, itemMask, boundary, wallMask;
        
        public virtual void SetData(Vector2 position, ItemData itemData, int turnDelay)
        {
            _myTransform = transform;
            _myTransform.position = position;
            _itemData = itemData;
            _turnDelay = turnDelay;
            if (_turnDelay > 0)
            {
                UnActiveDisplay();
            }
            else
            {
                ActiveDisplay();
            }

            GameController.Instance.Item_Add(this);
        }

        private void ActiveDisplay()
        {
            collider2D.enabled = true;
            iconTransform.gameObject.SetActive(true);
        }

        private void UnActiveDisplay()
        {
            collider2D.enabled = false;
            iconTransform.gameObject.SetActive(false);
        }

        public override void UpdateStep(float speed)
        {
            base.UpdateStep(speed);
            if (IsActive && IsFreezeItem)
                return;
            var target = Position;
            target.y--;
            var distance = 1;

            while (true)
            {
                var move = true;
                var jump = false;
                while (true)
                {
                    var hit = Physics2D.Linecast(target, target, brickMask);
                    if (hit)
                    {
                        if (hit.collider.GetComponent<Obstacle>().IsMove)
                            break;
                        move = false;
                        break;
                    }

                    hit = Physics2D.Linecast(target, target, itemMask);
                    if (hit)
                    {
                        if (!hit.collider.GetComponent<Obstacle>().IsMove)
                        {
                            distance++;
                            target.y--;
                            jump = true;
                            continue;
                        }
                    }
                    break;
                }

                if (move)
                {
                    if (jump)
                    {
                        // if (distance > 1)
                        //     distance += Height - 1;
                        Move(Position.y - distance, speed);
                    }
                    else
                    {
                        Move(Position.y - 1, speed);
                    }
                }

                break;
            }
        }

        private void Move(float target, float speed)
        {
            IsMove = true;
            collider2D.enabled = true;
            _myTransform.DOMoveY(target, speed).OnComplete(delegate
            {
                var hit = Physics2D.Linecast(Position, Position, brickMask + itemMask);
                if (hit)
                {
                    if (hit.collider.gameObject != gameObject)
                        Debug.LogError("Error");
                }
                IsMove = false;
                if (_turnDelay > 0)
                {
                    _turnDelay--;
                    if (_turnDelay <= 0)
                    {
                        ActiveDisplay();
                        if (Physics2D.Linecast(Position, Position, wallMask))
                        {
                            gameObject.SetActive(false);
                            GameController.Instance.Item_Remove(this);
                        }
                    }
                    else
                        collider2D.enabled = false;
                }

                var pos = Position;
                pos.y -= 0.5f;
                if (!Physics2D.Linecast(Position, pos, boundary))
                    return;
                gameObject.SetActive(false);
                GameController.Instance.Brick_Collision_Boundary();
                GameController.Instance.Item_Remove(this);
            });
        }

        protected virtual void OnHit(Ball ball)
        {
            if (ball.IsFirstBall)
                ActionController.Instance.Play_Action(ActionType.NiceShot);
        }

        public void Remove()
        {
            gameObject.SetActive(false);
            GameController.Instance.Item_Remove(this);
        }
        
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.CompareTag(BallTag))
                OnHit(collision.GetComponent<Ball>());
        }
    }

    [Serializable]
    public class ItemData
    {
        public ItemType itemType;
        public Vector3 rotation;
        public Vector2 wormHoleTarget;
    }

    public enum ItemType
    {
        BarDirection,
        GunDirection,
        WormHole,
        UpSideAutoDirection,
        LazeTwoDirection,
        IncreaseBall,
        LazeFourDirection,
        AcousticWaves
    }
}
