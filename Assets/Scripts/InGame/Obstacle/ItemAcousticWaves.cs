﻿using System.Collections.Generic;
using Do.Scripts.Tools.Pool;
using Other;
using UnityEngine;

namespace InGame.Obstacle
{
    public class ItemAcousticWaves : Item
    {
        private bool _isAction;
        private readonly List<GameObject> _effects = new List<GameObject>();
        public override void SetData(Vector2 position, ItemData itemData, int turnDelay)
        {
            base.SetData(position, itemData, turnDelay);
            _isAction = false;
            _effects.Clear();
        }
        protected override void OnHit(Ball ball)
        {
            base.OnHit(ball);
            if (!_isAction)
            {
                _isAction = true;
                GameController.Instance.Item_Add_Remove(this);
            }

            if (_effects.Count > 0)
            {
                for (var i = _effects.Count - 1; i >= 0 ; i--)
                {
                    if (!_effects[i].activeSelf)
                        _effects.Remove(_effects[i]);
                }
            }

            if (_effects.Count >= 3)
            {
                _effects[0].SetActive(false);
                _effects.Remove(_effects[0]);
            }
            var fx = ObjectPoolManager.Instance.effectAcousticWavesPools.GetPooledObject();
            fx.transform.position = Position;
            fx.SetActive(true);
            _effects.Add(fx);

            var cols = Physics2D.OverlapBoxAll(Position, new Vector2(2, 2), 0, brickMask);
            if (cols.Length <= 0)
                return;
            for (var i = cols.Length - 1; i >= 0; i--)
            {
                var col = cols[i];
                var brick = col.GetComponent<Brick>();
                if (Utils.GameMode == GameMode.Rescue)
                {
                    if (brick.IsSpecialRescue)
                        continue;
                }
                brick.OnEffectHit(1);
            }
        }
    }
}
