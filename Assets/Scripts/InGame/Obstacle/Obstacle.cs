﻿using UnityEngine;

namespace InGame.Obstacle
{
    public class Obstacle : MonoBehaviour
    {
        [SerializeField] protected bool isDebug;
        public bool IsMove { get; protected set; }
        public virtual void UpdateStep(float speed)
        {
            
        }
    }
}
