﻿using DG.Tweening;
using Do.Scripts.Tools.Audio;
using Do.Scripts.Tools.Tween;
using InGame.Obstacle;
using Loading;
using UnityEngine;

namespace InGame
{
    public class Ball : MonoBehaviour
    {
        private enum State
        {
            Auto,
            Recall
        }
        private State _state;
        
        private const string BrickTag = "Brick";
        private const string BoundaryTag = "Boundary";
        private const string CounterTag = "Counter";
        private const string Destroyer = "Destroyer";
        private const string ItemTag = "Item";

        [SerializeField] private SpriteRenderer icon;
        [SerializeField] private LayerMask mask;
        public Vector3 Direction { private get; set; }
        
        public bool IsFirstBall { get; set; }

        public Transform MyTransform => _myTransform;

        private Transform _myTransform;

        private float _speed, _timer;
        
        private readonly Vector3 _baseEulerDirection = Vector3.down * 0.1f;

        private bool _isCollision;
        private Collider2D _collider;

        private void Awake()
        {
            _myTransform = transform;
            _collider = GetComponent<Collider2D>();
        }

        private void OnEnable()
        {
            _timer = 0;
            _state = State.Auto;
            _isCollision = false;
            _collider.enabled = true;
            icon.sprite = RootManager.Instance.GetCurrentSkinBall;
            FollowDirection();
        }

        private void OnDisable()
        {
            if (IsFirstBall)
                IsFirstBall = false;
        }

        public void FixedUpdate()
        {
            if (_state != State.Auto)
                return;
            var fixedDeltaTime = Time.fixedDeltaTime;
            _speed = GameController.Instance.BallSpeed;
            var pos = _myTransform.position;

            var duration = _speed * fixedDeltaTime * Direction;
            var hit = Physics2D.Linecast(pos, duration + pos , mask);
            if (!hit)
            {
                pos += duration;
                _myTransform.position = pos;
                _timer += fixedDeltaTime;
                return;
            }

            pos = hit.point;
            pos -= 0.5f * fixedDeltaTime * Direction;
            hit.point = pos;
            _myTransform.position = pos;

            if (!_isCollision)
                _isCollision = true;
            if (IsFirstBall)
                IsFirstBall = false;
            
            var hitCollider = hit.collider;
            if (hitCollider.CompareTag(BrickTag))
            {
                hitCollider.GetComponent<Brick>().OnBallHit();
                AudioManager.Instance.Play(SoundType.BrickCollision);
                _timer = 0;
            }
            else if (hitCollider.CompareTag(Destroyer))
            {
                AudioManager.Instance.Play(SoundType.OneSaw);
                gameObject.SetActive(false);
                GameController.Instance.Ball_Destroy(this);
                return;
            }
            else if (hitCollider.CompareTag(BoundaryTag))
            {
                GameController.Instance.Ball_Return(this);
                return;
            }
            else if (hitCollider.CompareTag(CounterTag))
                GameController.Instance.Booster_Event_Counter();

            else if (hitCollider.CompareTag(ItemTag))
            {
                pos = hit.point;
                pos -= 0.5f * fixedDeltaTime * Direction;
                _myTransform.position = pos;
                ReturnDirection(hit);
                duration = _speed * fixedDeltaTime * Direction;
                pos += duration;
                _myTransform.position = pos;
                return;
            }
            ReturnDirection(hit);
        }

        private void ReturnDirection(RaycastHit2D hit)
        {
            var offsetDirection = Vector3.zero;

            if (_timer >= GameController.Instance.TimeToEuler)
            {
                _timer = 0;
                offsetDirection = _baseEulerDirection;
            }

            Direction = Vector3.Reflect(Direction, hit.normal) + offsetDirection; 
            FollowDirection();
        }

        private void FollowDirection()
        {
            if (Direction == Vector3.zero)
                return;
            var rota = Quaternion.LookRotation(Direction, Vector3.back);
            rota.x = 0f;
            rota.y = 0f;
            transform.rotation = rota;
        }

        public void RecallToTarget(float defaultValue = 0f)
        {
            if (defaultValue <= 0)
                defaultValue = GameController.Instance.BallTimeToRecall;
            _collider.enabled = false;
            _state = State.Recall;
            _myTransform.DOMove(GameController.Instance.TargetToRecall, defaultValue).SetEase(Ease.InSine).OnComplete(delegate
            {
                gameObject.SetActive(false);
            });
        }

        public void StopAndDisable()
        {
            _state = State.Recall;
            _collider.enabled = false;
            MyTween.Instance.MyTween_Float(1, delegate
            {
                gameObject.SetActive(false);
            });
        }
    }
}
