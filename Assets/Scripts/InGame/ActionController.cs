﻿using System.Collections;
using DG.Tweening;
using Do.Scripts.Tools.Audio;
using Do.Scripts.Tools.Other;
using Spine.Unity;
using UnityEngine;
using AnimationState = Spine.AnimationState;

namespace InGame
{
    public class ActionController : MonoBehaviour
    {
        public static  ActionController Instance { get; private set; }
        
        [SerializeField] private SkeletonGraphic[] skeletonGraphics;
        private readonly AnimationState[] _animationStates = new AnimationState[4];
        private IEnumerator _routine;
        private Callback _callback;

        private void Awake()
        {
            Instance = this;
        }

        private void OnDestroy()
        {
            Instance = null;
        }

        public void Play_Start(Callback callback = null)
        {
            _callback = callback;
            var ske = skeletonGraphics[0];
            ske.Skeleton.A = 0;
            ske.gameObject.SetActive(true);
            _routine = Anim_Start();
            StartCoroutine(_routine);
        }

        private IEnumerator Anim_Start()
        {
            yield return Yield.EndFrame;
            AudioManager.Instance.Play(SoundType.AnimStart);
            var ske = skeletonGraphics[0];
            if (_animationStates[0] == null)
                _animationStates[0] = ske.AnimationState;
            var animationState = _animationStates[0];
            animationState.SetAnimation(0, "animation", false);
            ske.Skeleton.A = 1;
            var rectTransform = ske.GetComponent<RectTransform>();
            var origin = new Vector2(0, -960);
            rectTransform.anchoredPosition = origin;
            rectTransform.DOAnchorPosY(0, 0.25f).SetEase(Ease.OutSine).OnComplete(delegate
                {
                    rectTransform.DOAnchorPosY(origin.y, 0.25f).SetDelay(0.5f).SetEase(Ease.InSine).OnComplete(delegate
                    {
                        ske.gameObject.SetActive(false);
                        _callback?.Invoke();
                    });
                });
        }

        public void Play_Action(ActionType type)
        {
            var ske = skeletonGraphics[(int) type];
            ske.Skeleton.A = 0;
            ske.gameObject.SetActive(true);
            _routine = Play(type);
            StartCoroutine(_routine);
        }

        private IEnumerator Play(ActionType type)
        {
            yield return Yield.EndFrame;
            AudioManager.Instance.Play(SoundType.TextAction);
            var intType = (int) type; 
            var ske = skeletonGraphics[intType];
            if (_animationStates[intType] == null)
                _animationStates[intType] = ske.AnimationState;
            var animationState = _animationStates[intType];
            animationState.SetAnimation(0, "animation", false);
            ske.Skeleton.A = 1;
            yield return new WaitForSpineAnimationComplete(animationState.GetCurrent(0));
            ske.gameObject.SetActive(false);
        }
    }

    public enum ActionType
    {
        Start,
        Cool,
        NiceShot,
        Amazing
    }
}
