﻿using System.Collections;
using System.Collections.Generic;
using Do.Scripts.Tools.Language;
using Do.Scripts.Tools.Other;
using Spine.Unity;
using UnityEngine;
using AnimationState = Spine.AnimationState;

namespace InGame
{
    public class SpineGraphic : MonoBehaviour
    {
        [SerializeField] private SkeletonGraphic skeletonGraphic;
        [SpineSkin] [SerializeField] private string skinEng, skinVie;
        [SpineAnimation] [SerializeField] private List<string> animStarts, animLoops;
        
        private AnimationState _animationState;

        public void Play(int anim = 0)
        {
            gameObject.SetActive(true);
            skeletonGraphic.initialSkinName = LanguageManager.Instance.Language == Language.Eng ? skinEng : skinVie;
            skeletonGraphic.Initialize(true);
            StartCoroutine(AutoPlay(anim));
            Debug.Log(anim);
        }

        private IEnumerator AutoPlay(int anim)
        {
            skeletonGraphic.Skeleton.A = 0;
            yield return Yield.EndFrame;
            skeletonGraphic.Skeleton.A = 1;
            _animationState = skeletonGraphic.AnimationState;
            _animationState.SetAnimation(0, animStarts[anim], false);
            yield return new WaitForSpineAnimationComplete(_animationState.GetCurrent(0));
            _animationState.SetAnimation(0, animLoops[anim], true);
        }
    }
}
