﻿using System.Collections;
using Do.Scripts.Tools.Audio;
using Spine.Unity;
using UnityEngine;
using AnimationState = Spine.AnimationState;

namespace InGame.Effect
{
    public class EffectSpineAnimation2 : MonoBehaviour
    {
        [SerializeField] private bool playSoundFx;
        [SerializeField] private SoundType soundType;
        [SerializeField] protected SkeletonAnimation ske1, ske2;
        [SerializeField] private AnimationReferenceAsset anim;
        private AnimationState _state1, _state2;
        private IEnumerator _routine;
        public bool IsActive { private set; get; }
        private void OnEnable()
        {
            IsActive = true;
            ske1.Skeleton.A = 0;
            ske2.Skeleton.A = 0;
            if (_state1 == null) 
                return;
            _routine = Animation();
            StartCoroutine(_routine);
        }

        private void Start()
        {
            _state1 = ske1.AnimationState;
            _state2 = ske2.AnimationState;
            _routine = Animation();
            StartCoroutine(_routine);
        }

        private IEnumerator Animation()
        {
            if (playSoundFx)
                AudioManager.Instance.Play(soundType);
            ske1.Skeleton.A = 1;
            ske2.Skeleton.A = 1;
            _state1.SetAnimation(0, anim, false);
            _state2.SetAnimation(0, anim, false);
            yield return new WaitForSpineAnimationComplete(_state1.GetCurrent(0));
            gameObject.SetActive(false);
            IsActive = false;
        }

        public void RebindAnim()
        {
            StopCoroutine(_routine);
            _routine = Animation();
            StartCoroutine(_routine);
        }
    }
}
