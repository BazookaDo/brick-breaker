﻿using System.Collections.Generic;
using Loading;
using Spine.Unity;
using UnityEngine;

namespace InGame.Effect
{
    public class EffectBrickBreaking : EffectSpineAnimation
    {
        [SpineSkin] [SerializeField] private List<string> listSkin;
        protected override void LoadSkin()
        {
            skeletonAnimation.Skeleton.SetSkin(listSkin[(int) RootManager.Instance.CurrentSkinBall]);
        }
    }
}
