﻿using System.Collections;
using Do.Scripts.Tools.Audio;
using Spine.Unity;
using UnityEngine;
using AnimationState = Spine.AnimationState;

namespace InGame.Effect
{
    public class EffectSpineAnimation : MonoBehaviour
    {
        [SerializeField] private bool playSoundFx;
        [SerializeField] private SoundType soundType;
        [SerializeField] protected SkeletonAnimation skeletonAnimation;
        [SpineAnimation] [SerializeField] private string anim;
        private AnimationState _animationState;
        private IEnumerator _routine;
        public bool IsActive { private set; get; }
        private void OnEnable()
        {
            IsActive = true;
            skeletonAnimation.Skeleton.A = 0;
            if (_animationState == null) 
                return;
            _routine = Animation();
            StartCoroutine(_routine);
        }

        private void Start()
        {
            _animationState = skeletonAnimation.AnimationState;
            _routine = Animation();
            StartCoroutine(_routine);
        }

        protected virtual void LoadSkin()
        {
            
        }

        private IEnumerator Animation()
        {
            if (playSoundFx)
                AudioManager.Instance.Play(soundType);
            LoadSkin();
            skeletonAnimation.Skeleton.A = 1;
            _animationState.SetAnimation(0, anim, false);
            yield return new WaitForSpineAnimationComplete(_animationState.GetCurrent(0));
            gameObject.SetActive(false);
            IsActive = false;
        }

        public void RebindAnim()
        {
            StopCoroutine(_routine);
            _routine = Animation();
            StartCoroutine(_routine);
        }
    }
}
