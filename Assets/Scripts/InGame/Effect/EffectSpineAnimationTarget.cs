﻿using System.Collections;
using Do.Scripts.Tools.Audio;
using Spine.Unity;
using UnityEngine;
using AnimationState = Spine.AnimationState;

namespace InGame.Effect
{
    public class EffectSpineAnimationTarget : MonoBehaviour
    {
        [SerializeField] private bool playSoundFx;
        [SerializeField] private SoundType soundType;
        [SerializeField] protected SkeletonAnimation skeletonAnimation;
        [SpineAnimation] [SerializeField] private string anim;
        [SpineBone] [SerializeField] private string boneTarget;
        private AnimationState _animationState;
        private IEnumerator _routine;
        private Vector2 _target;

        public void SetTarget(Vector2 target)
        {
            _target = target;
            var direction = (Vector2) transform.position - target;
            var rota = Quaternion.LookRotation(direction, Vector3.forward);
            rota.x = 0f;
            rota.y = 0f;
            transform.rotation = rota;
        }
        private void OnEnable()
        {
            skeletonAnimation.Skeleton.A = 0;
            if (_animationState == null) 
                return;
            _routine = Animation();
            StartCoroutine(_routine);
        }

        private void Start()
        {
            _animationState = skeletonAnimation.AnimationState;
            _routine = Animation();
            StartCoroutine(_routine);
        }

        private IEnumerator Animation()
        {
            if (playSoundFx)
                AudioManager.Instance.Play(soundType);
            var b = skeletonAnimation.skeleton.FindBone(boneTarget);
            b.Y = Vector2.Distance(_target, transform.position);
            b.Update();
            skeletonAnimation.Skeleton.A = 1;
            _animationState.SetAnimation(0, anim, false);
            yield return new WaitForSpineAnimationComplete(_animationState.GetCurrent(0));
            gameObject.SetActive(false);
        }
    }
}
