﻿using System.Collections;
using Do.Scripts.Tools.Audio;
using Do.Scripts.Tools.Other;
using UnityEngine;

namespace InGame.Effect
{
    public class EffectParticle : MonoBehaviour
    {
        [SerializeField] private float timeDelay;

        [SerializeField] private ParticleSystem particle;

        [SerializeField] private bool playSoundFx;
        [SerializeField] private SoundType soundType;
        private IEnumerator _routine;

        private void OnEnable()
        {
            _routine = Play();
            StartCoroutine(_routine);
            if (playSoundFx)
                AudioManager.Instance.Play(soundType);
        }

        private IEnumerator Play()
        {
            particle.Play();
            yield return Yield.GetTime(timeDelay);
            gameObject.SetActive(false);
        }
    }
}
