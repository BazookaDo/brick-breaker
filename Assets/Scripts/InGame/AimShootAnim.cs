﻿using DG.Tweening;
using Do.Scripts.Tools.Tween;
using UnityEngine;
using UnityEngine.UI;

namespace InGame
{
    public class AimShootAnim : MonoBehaviour
    {
        [SerializeField] private float timeFade, timeLoop;
        [SerializeField] private CanvasGroup normalGroup, aimGroup;
        [SerializeField] private Image ball;
        [SerializeField] private Color colorAim;
        [SerializeField] private RectTransform aimIcon;

        private Tween _tween, _subTween;
        private Vector2 _ballPos;

        private void OnEnable()
        {
            _ballPos = ball.GetComponent<RectTransform>().anchoredPosition;
            Animation();
        }

        private void Animation()
        {
            _tween = MyTween.Instance.DoTween_Float(0.5f, 0f, delegate
            {
                aimIcon.gameObject.SetActive(true);
                _tween = aimIcon.DOScale(new Vector3(2, 2, 2), 0.5f).OnComplete(delegate
                {
                    _subTween = aimIcon.DOScale(Vector3.one, 1f);
                    _tween = aimIcon.DOAnchorPos(_ballPos, 1f).SetEase(Ease.OutSine).OnComplete(delegate
                    {
                        _subTween = normalGroup.DOFade(0, timeFade);
                        aimGroup.gameObject.SetActive(true);
                        _tween = aimGroup.DOFade(1, timeFade).OnComplete(delegate
                        {
                            ball.color = colorAim;
                            normalGroup.gameObject.SetActive(false);
                            _tween = aimGroup.DOFade(0, timeLoop).OnComplete(delegate
                            {
                                _tween = aimGroup.DOFade(1, timeLoop).OnComplete(delegate
                                {
                                    _tween = aimGroup.DOFade(0, timeLoop).OnComplete(delegate
                                    {
                                        _tween = aimGroup.DOFade(1, timeLoop).OnComplete(delegate
                                        {
                                            _tween = aimGroup.DOFade(0, timeLoop).OnComplete(delegate
                                            {
                                                ResetObject();
                                                Animation();
                                            });
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
        }

        private void ResetObject()
        {
            normalGroup.gameObject.SetActive(true);
            normalGroup.alpha = 1;
            aimGroup.gameObject.SetActive(false);
            aimGroup.alpha = 0;
            ball.color = Color.white;
            aimIcon.localScale = Vector3.one;
            aimIcon.anchoredPosition = Vector2.zero;
            aimIcon.gameObject.SetActive(false);
        }

        private void OnDisable()
        {
            _tween.Kill();
            _tween = null;
            _subTween.Kill();
            _subTween = null;
            ResetObject();
        }
    }
}
