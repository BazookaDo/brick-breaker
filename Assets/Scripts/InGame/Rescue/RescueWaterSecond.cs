﻿using DG.Tweening;
using Do.Scripts.Tools.Tween;
using Spine.Unity;
using UnityEngine;

namespace InGame.Rescue
{
    public class RescueWaterSecond : RescueBase
    {
        [Range(1f, 5f)] [SerializeField] private float duration = 2f;
        [Range(0.5f, 2f)] [SerializeField] private float ratioRoom = 1f;
        [SerializeField] private float speedWalk = 1.5f;
        [SerializeField] private Vector2 waterRoot;
        [SerializeField] private Transform bg;
        [SerializeField] private AnimationReferenceAsset swim, idle, die, walk, win;
        [SerializeField] private SkeletonAnimation wife;
        [SerializeField] private AnimationReferenceAsset wifeSwim, wifeIdle, wifeDie, wifeWalk, wifeWin;
        private Transform _wifeTransform;
        private float _time, _height;
        private Vector2 _origin, _middle;
        public override void Init(Transform water2D)
        {
            base.Init(water2D);
            _wifeTransform = wife.transform;
            water2D.position = waterRoot;
            _time = _ratioStep * duration;
            _height = duration / (step) * ratioRoom;
            _gameController.Water_Instance(_time);
            var hit = Physics2D.Linecast(bg.position, new Vector2(0, -20), brickMask);
            var pos = _origin = hit.point;
            var skePos = skeletonTransform.position;
            pos.x = skePos.x;
            skePos = pos;
            skeletonTransform.position = skePos;
            pos.x = _wifeTransform.position.x;
            _wifeTransform.position = pos;
            _middle = pos;
            _middle.x = (pos.x + skePos.x) / 2;
            Play_Animation(skeletonAnimation, idle, true);
            Play_Animation(wife, wifeIdle, true);
            MyTween.Instance.MyTween_Float(0.75f, delegate
            {
                Play_Animation(skeletonAnimation, swim, true);
                Play_Animation(wife, wifeSwim, true);
                skeletonTransform.DOMoveY(pos.y + _height / 3, _time);
                _wifeTransform.DOMoveY(pos.y + _height / 3, _time);
            });
        }
        public override void Check_Step(Callback callback)
        {
            base.Check_Step(callback);
            _currentStep++;
            var pos = skeletonTransform.position;
            skeletonTransform.DOMoveY(pos.y + _height, _time).SetDelay(0.5f);
            _wifeTransform.DOMoveY(pos.y + _height, _time).SetDelay(0.5f);
            _gameController.Water_Instance(_time, delegate
            {
                if (_currentStep >= step)
                {
                    MyTween.Instance.MyTween_Float(0.5f, delegate
                    {
                        Play_Animation(skeletonAnimation, die, false);
                        Play_Animation(wife, wifeDie, false);
                        skeletonTransform.DOMoveY(_origin.y, duration).OnComplete(_gameController.State_Game_Over);
                        _wifeTransform.DOMoveY(_origin.y, duration);
                    });
                }
                else
                {
                    callback?.Invoke();
                }
            });
        }

        public override void State_Win()
        {
            Play_Animation(skeletonAnimation, idle, true);
            Play_Animation(wife, wifeIdle, true);
            var time = 1f * _currentStep;
            var left = _middle.x;
            left -= 0.5f;
            var right = _middle.x;
            right += 0.5f;
            
            skeletonTransform.DOMoveY(_origin.y, time).SetEase(Ease.OutSine).OnComplete(delegate
            {
                _gameController.Water_Remove();
                Play_Animation(skeletonAnimation, walk, true);
                skeletonTransform.DOMoveX(skeletonTransform.position.x > _middle.x ? right : left, speedWalk).SetEase(Ease.InSine).OnComplete(
                    delegate
                    {
                        Play_Animation(skeletonAnimation, win, true);
                        MyTween.Instance.MyTween_Float(1.5f, _gameController.State_Game_Win);
                    });
            });
            _wifeTransform.DOMoveY(_origin.y, time).SetEase(Ease.OutSine).OnComplete(delegate
            {
                Play_Animation(wife, wifeWalk, true);
                _wifeTransform.DOMoveX(_wifeTransform.position.x > _middle.x ? right : left, speedWalk).SetEase(Ease.InSine).OnComplete(
                    delegate
                    {
                        Play_Animation(wife, wifeWin, true);
                    });
            });
        }

        public override void State_Lose()
        {
            var clamp = step - _currentStep;
            var pos = skeletonTransform.position;
            skeletonTransform.DOMoveY(pos.y + _height * clamp, _time * clamp).SetDelay(0.5f);
            _wifeTransform.DOMoveY(pos.y + _height * clamp, _time * clamp).SetDelay(0.5f);
            _gameController.Water_Instance(_time * (clamp + 1), delegate
            {
                MyTween.Instance.MyTween_Float(0.5f, delegate
                {
                    Play_Animation(skeletonAnimation, die, false);
                    Play_Animation(wife, wifeDie, false);
                    skeletonTransform.DOMoveY(_origin.y, (duration - 1) * _time * ratioRoom).OnComplete(_gameController.State_Game_Over);
                    _wifeTransform.DOMoveY(_origin.y, (duration - 1) * _time * ratioRoom);
                });
            });
        }
    }
}
