﻿using System;
using Spine.Unity;
using UnityEngine;

namespace InGame.Rescue
{
    public class CharacterWeaponAttack : MonoBehaviour
    {
        [SerializeField] [SpineSlot] private string slotName;
        [SerializeField] private string skinWeapon;
        [SpineEvent] public string eventActive, eventAttack;
        private SkeletonAnimation _skeletonAnimation;
        public Callback EventActiveWeapon, EventAttack;
        private void Start()
        {
            _skeletonAnimation = GetComponent<SkeletonAnimation>();
            _skeletonAnimation.AnimationState.Event += Active_Weapon;
        }

        private void Active_Weapon(Spine.TrackEntry entry, Spine.Event e)
        {
            var eventName = e.Data.Name;
            if (eventName == eventActive)
            {
                var skeleton = _skeletonAnimation.skeleton;
                skeleton.FindSlot(slotName).Attachment = null;
                skeleton.SetAttachment(slotName, skinWeapon);
                EventActiveWeapon?.Invoke();
                EventActiveWeapon = null;
            }
            else if (eventName == eventAttack)
            {
                EventAttack?.Invoke();
                EventAttack = null;
            }
        }
    }
}
