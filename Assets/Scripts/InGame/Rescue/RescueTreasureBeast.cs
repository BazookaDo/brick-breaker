﻿using DG.Tweening;
using Do.Scripts.Tools.Tween;
using Spine.Unity;
using UnityEngine;

namespace InGame.Rescue
{
    public class RescueTreasureBeast : RescueBase
    {
        [SerializeField] private float durationWin, durationLose;
        [SerializeField] private AnimationReferenceAsset idle, die, move, collect, win;
        [SerializeField] private SkeletonAnimation beast;
        [SerializeField] private AnimationReferenceAsset beastIdle, beastMove, beastAttack;
        [SerializeField] private GameObject fenceLose, fenceWin;
        [SerializeField] private GameObject treasureLock, treasureOpen;
        private float _targetWin, _targetLose;
        public override void Init(Transform water2D)
        {
            base.Init(water2D);
            Play_Animation(skeletonAnimation, idle, true);
            var characterX = skeletonTransform.position.x;
            _targetLose = beast.transform.position.x > characterX ? characterX + 1 : characterX - 1;
            var treasureX = treasureLock.transform.position.x;
            _targetWin = characterX > treasureX ? treasureX + 0.5f : treasureX - 0.5f;
        }
        public override void Check_Step(Callback callback)
        {
            base.Check_Step(callback);
            callback?.Invoke();
        }

        public override void State_Win()
        {
            fenceWin.SetActive(false);
            Play_Animation(skeletonAnimation, move, true);
            var localScale = skeletonTransform.localScale;
            if (skeletonTransform.position.x < _targetWin)
                localScale.x = Mathf.Abs(localScale.x);
            else
                localScale.x = -Mathf.Abs(localScale.x);
            skeletonTransform.localScale = localScale;
            skeletonTransform.DOMoveX(_targetWin, durationWin).OnComplete(delegate
            {
                Play_Animation(skeletonAnimation,collect,false, delegate
                {
                    treasureLock.SetActive(false);
                    treasureOpen.SetActive(true);
                    Play_Animation(skeletonAnimation, win, true);
                    MyTween.Instance.MyTween_Float(2f, _gameController.State_Game_Win);
                });
            });
        }

        public override void State_Lose()
        {
            fenceLose.SetActive(false);
            Play_Animation(beast, beastMove, true);
            beast.transform.DOMoveX(_targetLose, durationLose).SetEase(Ease.InSine).OnComplete(delegate
            {
                Play_Animation(beast, beastAttack, false);
                MyTween.Instance.MyTween_Float(0.5f, delegate
                {
                    MyTween.Instance.MyTween_Float(0.25f, delegate
                    {
                        Play_Animation(beast, beastIdle, true);
                    });
                    Play_Animation(skeletonAnimation, die, false);
                    MyTween.Instance.MyTween_Float(2f, _gameController.State_Game_Over);
                });
            });
        }
    }
}
