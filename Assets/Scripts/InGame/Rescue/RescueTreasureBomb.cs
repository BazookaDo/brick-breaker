﻿using System.Collections.Generic;
using DG.Tweening;
using Do.Scripts.Tools.Audio;
using Do.Scripts.Tools.Pool;
using Do.Scripts.Tools.Tween;
using Spine.Unity;
using UnityEngine;
using UnityEngine.UIElements;

namespace InGame.Rescue
{
    public class RescueTreasureBomb : RescueBase
    {
        [SerializeField] private Vector2 targetWin;
        [SerializeField] private float duration;
        [SerializeField] private AnimationReferenceAsset idle, die, move, collect, win;
        [SerializeField] private SkeletonAnimation bomb;
        [SerializeField] private AnimationReferenceAsset bombAction;
        [SerializeField] private GameObject fenceLose, fenceWin;
        [SerializeField] private GameObject treasureLock, treasureOpen;
        public override void Init(Transform water2D)
        {
            base.Init(water2D);
            Play_Animation(skeletonAnimation, idle, true);
        }
        public override void Check_Step(Callback callback)
        {
            base.Check_Step(callback);
            callback?.Invoke();
        }

        public override void State_Win()
        {
            fenceWin.SetActive(false);
            Play_Animation(skeletonAnimation, move, true);
            skeletonTransform.DOMove(targetWin, duration).OnComplete(delegate
            {
                Play_Animation(skeletonAnimation,collect,false, delegate
                {
                    treasureLock.SetActive(false);
                    treasureOpen.SetActive(true);
                    Play_Animation(skeletonAnimation, win, true);
                    MyTween.Instance.MyTween_Float(2f, _gameController.State_Game_Win);
                });
            });
        }

        public override void State_Lose()
        {
            fenceLose.SetActive(false);
            Play_Animation(bomb, bombAction,false, delegate
            {
                var fx = ObjectPoolManager.Instance.effectMiniBombPools.GetPooledObject();
                fx.transform.position = bomb.transform.position;
                fx.SetActive(true);
                AudioManager.Instance.Play(SoundType.Explosion);
                bomb.gameObject.SetActive(false);
                Play_Animation(skeletonAnimation, die, false);
                MyTween.Instance.MyTween_Float(2f, _gameController.State_Game_Over);
            });
        }
    }
}
