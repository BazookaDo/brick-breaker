﻿using System.Collections.Generic;
using DG.Tweening;
using Do.Scripts.Tools.Tween;
using Spine;
using Spine.Unity;
using UnityEngine;

namespace InGame.Rescue
{
    public class RescueWeapon : RescueBase
    {
        [SerializeField] private bool isMove;
        [SerializeField] private float durationLose, durationWeapon, durationWin;
        [SerializeField] private CharacterWeaponAttack characterWeaponAttack;
        [SerializeField] private AnimationReferenceAsset idle, die, collect, move, attack, win;
        [SerializeField] private SkeletonAnimation beast;
        [SerializeField] private AnimationReferenceAsset beastIdle, beastMove, beastAttack, beastDie;
        [SerializeField] private GameObject save, weapon, fence;
        private float _targetLose, _targetWeapon, _targetWin;
        public override void Init(Transform water2D)
        {
            base.Init(water2D);
            Play_Animation(skeletonAnimation, idle, true);
            Play_Animation(beast, beastIdle, true);
            var characterX = skeletonTransform.position.x;
            var beastX = beast.transform.position.x;
            _targetLose = beastX > characterX ? characterX + 1 : characterX - 1;
            _targetWin = characterX > beastX ? beastX + 0.5f : beastX - 0.5f;
            var pos = weapon.transform.position;
            var hit = Physics2D.Linecast(pos, new Vector2(pos.x, -10), brickMask);
            _targetWeapon = hit.point.y;
            _targetWeapon += 0.15f;
            characterWeaponAttack.EventActiveWeapon = () => { weapon.SetActive(false); };
            characterWeaponAttack.EventAttack = () => { Play_Animation(beast, beastDie, false); };
        }
        public override void Check_Step(Callback callback)
        {
            base.Check_Step(callback);
            callback?.Invoke();
        }

        public override void State_Win()
        {
            save.SetActive(false);
            weapon.transform.DOMoveY(_targetWeapon, durationWeapon).OnComplete(delegate
            {
                Play_Animation(skeletonAnimation, collect, false, delegate
                {
                    fence.SetActive(false); 
                    if (isMove)
                    {
                        Play_Animation(skeletonAnimation, move, true);
                        skeletonTransform.DOMoveX(_targetWin, durationWin).OnComplete(delegate
                        {
                            Play_Animation(skeletonAnimation, attack, false, delegate
                            {
                                Play_Animation(skeletonAnimation, win, true);
                                MyTween.Instance.MyTween_Float(2f, _gameController.State_Game_Win);
                            });
                        });
                    }
                    else
                    {
                        Play_Animation(skeletonAnimation, attack, false, delegate
                        {
                            Play_Animation(skeletonAnimation, win, true);
                            MyTween.Instance.MyTween_Float(2f, _gameController.State_Game_Win);
                        });
                    }
                });
            });
        }

        public override void State_Lose()
        {
            fence.SetActive(false);
            Play_Animation(beast, beastMove, true);
            beast.transform.DOMoveX(_targetLose, durationLose).SetEase(Ease.InSine).OnComplete(delegate
            {
                Play_Animation(beast, beastAttack, false);
                MyTween.Instance.MyTween_Float(0.5f, delegate
                {
                    MyTween.Instance.MyTween_Float(0.25f, delegate
                    {
                        Play_Animation(beast, beastIdle, true);
                    });
                    Play_Animation(skeletonAnimation, die, false);
                    MyTween.Instance.MyTween_Float(2f, _gameController.State_Game_Over);
                });
            });
        }
    }
}
