﻿using System.Collections.Generic;
using DG.Tweening;
using Do.Scripts.Tools.Audio;
using Do.Scripts.Tools.Pool;
using Do.Scripts.Tools.Tween;
using Spine.Unity;
using UnityEngine;

namespace InGame.Rescue
{
    public class RescueBeastBomb : RescueBase
    {
        [SerializeField] private float durationLose;
        [SerializeField] private AnimationReferenceAsset idle, die, win;
        [SerializeField] private SkeletonAnimation beast;
        [SerializeField] private AnimationReferenceAsset beastIdle, beastMove, beastAttack, beastDie;
        [SerializeField] private SkeletonAnimation bomb;
        [SerializeField] private AnimationReferenceAsset actionBomb;
        [SerializeField] private List<GameObject> fences;
        private float _targetLose;
        public override void Init(Transform water2D)
        {
            base.Init(water2D);
            Play_Animation(skeletonAnimation, idle, true);
            Play_Animation(beast, beastIdle, true);
            var characterX = skeletonTransform.position.x;
            _targetLose = beast.transform.position.x > characterX ? characterX + 1 : characterX - 1;
        }
        public override void Check_Step(Callback callback)
        {
            base.Check_Step(callback);
            _currentStep++;
            var fence = fences[fences.Count - 1];
            fence.SetActive(false);
            fences.Remove(fence);
            if (_currentStep >= step)
            {
                Action_Lose();
            }
            else
            {
                callback?.Invoke();
            }
        }

        public override void State_Win()
        {
            Play_Animation(bomb, actionBomb,false, delegate
            {
                var fx = ObjectPoolManager.Instance.effectMiniBombPools.GetPooledObject();
                fx.transform.position = bomb.transform.position;
                fx.SetActive(true);
                AudioManager.Instance.Play(SoundType.Explosion);
                bomb.gameObject.SetActive(false);
                Play_Animation(beast, beastDie, false);
                Play_Animation(skeletonAnimation, win, true);
                MyTween.Instance.MyTween_Float(2f, _gameController.State_Game_Win);
            });
        }

        public override void State_Lose()
        {
            Action_Lose();
        }

        private void Action_Lose()
        {
            Play_Animation(beast, beastMove, true);
            beast.transform.DOMoveX(_targetLose, durationLose).SetEase(Ease.InSine).OnComplete(delegate
            {
                Play_Animation(beast, beastAttack, false);
                MyTween.Instance.MyTween_Float(0.5f, delegate
                {
                    MyTween.Instance.MyTween_Float(0.25f, delegate
                    {
                        Play_Animation(beast, beastIdle, true);
                    });
                    Play_Animation(skeletonAnimation, die, false);
                    MyTween.Instance.MyTween_Float(2f, _gameController.State_Game_Over);
                });
            });
        }
    }
}
