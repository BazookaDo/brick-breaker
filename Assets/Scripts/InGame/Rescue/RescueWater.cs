﻿using DG.Tweening;
using Do.Scripts.Tools.Tween;
using Spine.Unity;
using UnityEngine;

namespace InGame.Rescue
{
    public class RescueWater : RescueBase
    {
        [Range(1f, 5f)] [SerializeField] private float duration = 2f;
        [Range(0.1f, 2f)] [SerializeField] private float ratioRoom = 0.5f;
        [SerializeField] private Vector2 waterRoot;
        [SerializeField] private Transform bg;
        [SerializeField] private AnimationReferenceAsset swim, idle, die, win;
        private float _time, _height;
        private Vector2 _origin;
        public override void Init(Transform water2D)
        {
            base.Init(water2D);
            water2D.position = waterRoot;
            _time = _ratioStep * duration;
            _height = duration / step * ratioRoom;
            _gameController.Water_Instance(_time);
            var hit = Physics2D.Linecast(bg.position, new Vector2(0, -20), brickMask);
            var pos = _origin = hit.point;
            pos.x = _origin.x = skeletonTransform.position.x;
            skeletonTransform.position = pos;
            Play_Animation(skeletonAnimation, idle, true);
            MyTween.Instance.MyTween_Float(0.75f, delegate
            {
                Play_Animation(skeletonAnimation, swim, true);
                skeletonTransform.DOMoveY(pos.y + _height / 3, _time);
            });
        }
        public override void Check_Step(Callback callback)
        {
            base.Check_Step(callback);
            _currentStep++;
            var pos = skeletonTransform.position;
            skeletonTransform.DOMoveY(pos.y + _height, _time).SetDelay(0.5f);
            _gameController.Water_Instance(_time, delegate
            {
                if (_currentStep >= step)
                {
                    MyTween.Instance.MyTween_Float(0.5f, delegate
                    {
                        Play_Animation(skeletonAnimation, die, false);
                        skeletonTransform.DOMoveY(_origin.y, duration).OnComplete(_gameController.State_Game_Over);
                    });
                }
                else
                {
                    callback?.Invoke();
                }
            });
        }

        public override void State_Win()
        {
            Play_Animation(skeletonAnimation, idle, true);
            skeletonTransform.DOMoveY(_origin.y, 1f * _currentStep).SetEase(Ease.OutSine).OnComplete(delegate
            {
                _gameController.Water_Remove();
                Play_Animation(skeletonAnimation, win, true);
                MyTween.Instance.MyTween_Float(2f, _gameController.State_Game_Win);
            });
        }

        public override void State_Lose()
        {
            var clamp = step - _currentStep;
            var pos = skeletonTransform.position;
            skeletonTransform.DOMoveY(pos.y + _height * clamp * 0.75f, _time * clamp).SetDelay(0.5f);
            _gameController.Water_Instance(_time * (clamp + 1), delegate
            {
                MyTween.Instance.MyTween_Float(0.5f, delegate
                {
                    Play_Animation(skeletonAnimation, die, true);
                    skeletonTransform.DOMoveY(_origin.y, (duration - 1) * _time * ratioRoom).OnComplete(_gameController.State_Game_Over);
                });
            });
        }
    }
}
