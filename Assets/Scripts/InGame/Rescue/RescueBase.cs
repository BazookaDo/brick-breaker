﻿using System.Collections;
using System.Collections.Generic;
using Spine.Unity;
using UnityEngine;

namespace InGame.Rescue
{
    public class RescueBase : MonoBehaviour
    {
        protected GameController _gameController;
        [SerializeField] protected LayerMask brickMask;
        protected int _currentStep;
        protected float _ratioStep;
        [SerializeField] protected SkeletonAnimation skeletonAnimation;
        [Range(2, 5)] [SerializeField] protected int step = 2;
        protected Transform skeletonTransform;
        private IEnumerator _routine;

        [SerializeField] private List<ControlDirectionBall> controls;
        public virtual void Init(Transform water2D)
        {
            _gameController = GameController.Instance;
            _currentStep = 1;
            _ratioStep = 1f / step;
            skeletonTransform = skeletonAnimation.transform;
        }
        public virtual void Check_Step(Callback callback)
        {
            foreach (var control in controls)
            {
                control.Reset();
            }
        }

        public virtual void State_Win()
        {
            
        }

        public virtual void State_Lose()
        {
            
        }

        protected void Play_Animation(SkeletonAnimation ske, AnimationReferenceAsset ani, bool loop,
            Callback callback = null)
        {
            if (callback == null)
                ske.AnimationState.SetAnimation(0, ani, loop);
            else
            {
                _routine = Event_Animation(ske, ani, callback);
                StartCoroutine(_routine);
            }
        }

        private IEnumerator Event_Animation(SkeletonAnimation ske, AnimationReferenceAsset ani,
            Callback callback = null)
        {
            var animationState = ske.AnimationState;
            animationState.SetAnimation(0, ani, false);
            yield return new WaitForSpineAnimationComplete(animationState.GetCurrent(0));
            callback?.Invoke();
        }
    }
}
