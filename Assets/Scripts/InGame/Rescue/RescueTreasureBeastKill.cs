﻿using System.Collections.Generic;
using DG.Tweening;
using Do.Scripts.Tools.Tween;
using Spine.Unity;
using UnityEngine;

namespace InGame.Rescue
{
    public class RescueTreasureBeastKill : RescueBase
    {
        [SerializeField] private float durationKill, durationWin, durationLose;
        [SerializeField] private AnimationReferenceAsset idle, die, move, collect, win;
        [SerializeField] private SkeletonAnimation beast;
        [SerializeField] private AnimationReferenceAsset beastIdle, beastMove, beastAttack, beastDie;
        [SerializeField] private GameObject treasureLock, treasureOpen;
        [SerializeField] private GameObject save, kill;
        [SerializeField] private List<GameObject> fences;
        private float _targetWin, _targetLose;
        private Vector2 _targetKill;
        public override void Init(Transform water2D)
        {
            base.Init(water2D);
            Play_Animation(skeletonAnimation, idle, true);
            var characterX = skeletonTransform.position.x;
            _targetLose = beast.transform.position.x > characterX ? characterX + 1 : characterX - 1;
            var treasureX = treasureLock.transform.position.x;
            _targetWin = characterX > treasureX ? treasureX + 0.5f : treasureX - 0.5f;
            var pos = kill.transform.position;
            var hit = Physics2D.Linecast(pos, new Vector2(pos.x, pos.y - 10), brickMask);
            _targetKill = hit.point;
            _targetKill.y += 0.5f;
        }
        public override void Check_Step(Callback callback)
        {
            base.Check_Step(callback);
            _currentStep++;
            var fence = fences[fences.Count - 1];
            fence.SetActive(false);
            fences.Remove(fence);
            if (_currentStep >= step)
            {
                Action_Lose();
            }
            else
            {
                callback?.Invoke();
            }
        }

        public override void State_Win()
        {
            save.SetActive(false);
            Vector2 pos = kill.transform.position;
            var direction = _targetKill - pos;
            direction.Normalize();
            var between = _targetKill - direction * 0.5f;
            var startTime = Vector2.Distance(between, pos) / Vector2.Distance(_targetKill, pos) * durationKill;
            var endTime = durationKill - startTime;
            kill.transform.DOMove(between, startTime).OnComplete(delegate
            {
                Play_Animation(beast, beastDie, false);
                kill.transform.DOMove(_targetKill, endTime).OnComplete(delegate
                {
                    foreach (var fence in fences)
                    {
                        fence.SetActive(false);
                    }
                    Play_Animation(skeletonAnimation, move, true);
                    var localScale = skeletonTransform.localScale;
                    if (skeletonTransform.position.x < _targetWin)
                        localScale.x = Mathf.Abs(localScale.x);
                    else
                        localScale.x = -Mathf.Abs(localScale.x);
                    skeletonTransform.localScale = localScale;
                    skeletonTransform.DOMoveX(_targetWin, durationWin).SetEase(Ease.Linear).OnComplete(delegate
                    {
                        Play_Animation(skeletonAnimation,collect,false, delegate
                        {
                            treasureLock.SetActive(false);
                            treasureOpen.SetActive(true);
                            Play_Animation(skeletonAnimation, win, true);
                            MyTween.Instance.MyTween_Float(2f, _gameController.State_Game_Win);
                        });
                    });
                });
            });
        }

        public override void State_Lose()
        {
            foreach (var fence in fences)
            {
                fence.SetActive(false);
            }
            Action_Lose();
        }

        private void Action_Lose()
        {
            Play_Animation(beast, beastMove, true);
            beast.transform.DOMoveX(_targetLose, durationLose).SetEase(Ease.InSine).OnComplete(delegate
            {
                Play_Animation(beast, beastAttack, false);
                MyTween.Instance.MyTween_Float(0.5f, delegate
                {
                    MyTween.Instance.MyTween_Float(0.25f, delegate
                    {
                        Play_Animation(beast, beastIdle, true);
                    });
                    Play_Animation(skeletonAnimation, die, false);
                    MyTween.Instance.MyTween_Float(2f, _gameController.State_Game_Over);
                });
            });
        }
    }
}
