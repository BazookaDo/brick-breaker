﻿using System.Collections.Generic;
using UnityEngine;

namespace InGame.Rescue
{
    public class ControlDirectionBall : MonoBehaviour
    {
        private const string BallTag = "Ball";
        [SerializeField] private Transform target;
        [SerializeField] private List<Ball> balls;

        private Vector3 _direction;

        private void Awake()
        {
            _direction = target.position - transform.position;
            _direction.Normalize();
            balls = new List<Ball>();
        }

        public void Reset()
        {
            balls.Clear();
        }
        
        #if UNITY_EDITOR
        private void OnDrawGizmos()
        {
            var pos = transform.position;
            var targetPos = target.position;
            Gizmos.DrawSphere(pos, 0.1f);
            Gizmos.DrawLine(pos, targetPos);
            Gizmos.DrawSphere(targetPos, 0.1f);
        }
#endif
        
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (!other.CompareTag(BallTag))
                return;
            var ball = other.GetComponent<Ball>();
            if (balls.Contains(ball))
                return;
            ball.MyTransform.position = transform.position;
            ball.Direction = _direction;
            balls.Add(ball);
        }
    }
}
