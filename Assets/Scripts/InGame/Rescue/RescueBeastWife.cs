﻿using System.Collections.Generic;
using DG.Tweening;
using Do.Scripts.Tools.Tween;
using Spine.Unity;
using UnityEngine;

namespace InGame.Rescue
{
    public class RescueBeastWife : RescueBase
    {
        [SerializeField] private float durationLose, durationWin;
        [SerializeField] private AnimationReferenceAsset idle, die, walk, win;
        [SerializeField] private SkeletonAnimation wife;
        [SerializeField] private AnimationReferenceAsset wifeIdle, wifeCry, wifeWalk, wifeWin;
        [SerializeField] private SkeletonAnimation beast;
        [SerializeField] private AnimationReferenceAsset beastIdle, beastMove, beastAttack;
        [SerializeField] private GameObject fenceLose, fenceWin;
        private float _middle;
        public override void Init(Transform water2D)
        {
            base.Init(water2D);
            Play_Animation(skeletonAnimation, idle, true);
            Play_Animation(wife, wifeIdle, true);
            Play_Animation(beast, beastIdle, true);
            _middle = (skeletonTransform.position.x + wife.transform.position.x) / 2;
        }
        public override void Check_Step(Callback callback)
        {
            base.Check_Step(callback);
            callback?.Invoke();
        }

        public override void State_Win()
        {
            fenceWin.SetActive(false);
            Play_Animation(skeletonAnimation, walk, true);
            Play_Animation(wife, wifeWalk, true);
            var left = _middle - 0.5f;
            var right = _middle + 0.5f;
            var wifeTransform = wife.transform;
            var localScale = wifeTransform.localScale;
            if (wifeTransform.position.x < _middle)
                localScale.x = Mathf.Abs(localScale.x);
            else
                localScale.x = -Mathf.Abs(localScale.x);
            wifeTransform.localScale = localScale;
            wifeTransform.DOMoveX(wifeTransform.position.x < _middle ? left : right, durationWin);
            localScale = skeletonTransform.localScale;
            if (skeletonTransform.position.x < _middle)
                localScale.x = Mathf.Abs(localScale.x);
            else
                localScale.x = -Mathf.Abs(localScale.x);
            skeletonTransform.localScale = localScale;
            skeletonTransform.DOMoveX(skeletonTransform.position.x < _middle ? left : right, durationWin).OnComplete(
                delegate
                {
                    Play_Animation(skeletonAnimation, win, true);
                    Play_Animation(wife, wifeWin, true);
                    MyTween.Instance.MyTween_Float(2f, _gameController.State_Game_Win);
                });
        }

        public override void State_Lose()
        {
            fenceLose.SetActive(false);
            Play_Animation(beast, beastMove, true);
            var beastTransform = beast.transform;
            var target = skeletonTransform.position.x;
            beastTransform.DOMoveX(beastTransform.position.x < target ? target - 0.25f : target + 0.25f, durationLose)
                .SetEase(Ease.InSine).OnComplete(delegate
                {
                    Play_Animation(beast, beastAttack, false);
                    MyTween.Instance.MyTween_Float(0.5f, delegate
                    {
                        MyTween.Instance.MyTween_Float(0.25f, delegate
                        {
                            Play_Animation(beast, beastIdle, true);
                            // Play_Animation(wife, wifeCry, true);
                        });
                        Play_Animation(skeletonAnimation, die, false);
                        MyTween.Instance.MyTween_Float(2f, _gameController.State_Game_Over);
                    });
                });
        }
    }
}
