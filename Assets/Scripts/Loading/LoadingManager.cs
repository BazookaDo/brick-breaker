﻿using System.Collections;
using System.Collections.Generic;
using Do.Scripts.Tools.Language;
using Do.Scripts.Tools.Other;
using Do.Scripts.Tools.Tween;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Loading
{
    public class LoadingManager : MonoBehaviour
    {
        #region Variable
        
        private static LoadingManager instance;
        
        public const string SCENE_LOADING = "LoadingScene";
        public const string SCENE_DATA = "DataScene";
        public const string SCENE_HOME = "HomeScene";
        public const string SCENE_GAME = "GameScene";
        
        public static LoadingManager Instance
        {
            get
            {
                if (instance != null) 
                    return instance;
                var transform = Instantiate(Resources.Load<Transform>("LoadingManager"));
                DontDestroyOnLoad(transform.gameObject);
                instance = transform.GetComponent<LoadingManager>();
                return instance;
            }
        }

        private const float DelayTime = 0.2f;
        private const float Accelerate = 0.5f;
        private const float EndDelayTime = 0.3f;
        [Header("UI")]
        [SerializeField]
        private GameObject root;

        [SerializeField] private Image icon;
        [SerializeField] private Sprite preIcon, nextIcon;
        [SerializeField] private GameObject lets;
        [SerializeField]
        private TextMeshProUGUI titleFill;

        private AsyncOperation _async;
        private bool _loadScene;
        private bool _loadSceneDone, _loadFillDone;
        private bool _isLoadingShow;
        private IEnumerator _routineScene, _routineTitle;
        private Callback _callbackLoadRoutine, _callBackLoadUpdate;
        private readonly List<Callback> _listLoadData = new List<Callback>();
        private readonly List<Callback> _listEndLoad = new List<Callback>();
        private bool _isLoadingScene;

        [SerializeField] private GameObject resource, loadingCamera;

        #endregion

        private void Awake()
        {
            if (instance != null) 
                return;
            instance = this;
            DontDestroyOnLoad(gameObject);
            if (SceneManager.GetActiveScene().name == SCENE_LOADING)
                _isLoadingScene = true;
        }

        private void Start()
        {
            if (LanguageManager.Instance == null)
                Instantiate(resource);
            Invoke(nameof(Init), 1);
        }

        private void Init()
        {
            if (!_isLoadingScene)
            {
                Debug.Log("Loading Auto");
                PlayAnimationOnce();
            }
        }

        private void CheckBanner()
        {
            // set loading fill order banner
        }

        #region LoadSceneStep
        private IEnumerator LoadAsyncScene(string nameScene)
        {
            _loadScene = true;
            _loadSceneDone = false;
            _async = new AsyncOperation();
            _async = SceneManager.LoadSceneAsync(nameScene);
            _async.allowSceneActivation = false;
            while (!_loadSceneDone)
            {
                yield return null;
                if (_async.progress < 0.9f) 
                    continue;
                _loadSceneDone = true;
                if (_loadFillDone)
                {
                    Debug.Log("Loading Async Late");
                    _async.allowSceneActivation = true;
                    _loadScene = false;
                    _callbackLoadRoutine?.Invoke();
                }
                else 
                    Debug.Log("Loading Async Soon");
            }

        }

        private void LoadSceneWithFill(float target, Callback callback)
        {
            _loadFillDone = false;
            var time = target * Accelerate;
            MyTween.Instance.MyTween_Float(time, DelayTime, delegate
            {
                if (_loadScene)
                {
                    _loadFillDone = true;
                    if (_loadSceneDone)
                    {
                        Debug.Log("Loading Scene End Fill");
                        _async.allowSceneActivation = true;
                        _loadScene = false;
                        callback?.Invoke();
                    }
                    else 
                        Debug.Log("None Scene End Fill");
                }
                else
                {
                    Debug.Log("Loading None Load Scene");
                    callback?.Invoke();
                }
            });
        }

        private void LoadDataWithFill(float oldTarget, float newTarget, Callback callback)
        {
            var time = (newTarget - oldTarget) * Accelerate;
            MyTween.Instance.MyTween_Float(time, DelayTime, delegate
            {
                callback?.Invoke();
                CallEventLoadData();
            });
        }

        private void LoadToEndWithFill(float oldTarget, float newTarget, Callback callback)
        {
            var time = (newTarget - oldTarget) * Accelerate;
            MyTween.Instance.MyTween_Float(time, DelayTime, delegate
            {
                callback?.Invoke();
                CallEventLoadData();
            });
        }

        private void LoadEventEndWithFill()
        {
            CallEventLoadData();
            MyTween.Instance.MyTween_Float(EndDelayTime, delegate
            {
                CallEventEndLoading();
                loadingCamera.SetActive(false);
                root.SetActive(false);
                _isLoadingShow = false;
            });
        }

        #endregion
        
        /// <summary>
        /// /////////
        /// </summary>

        #region LoadOnceScene

        public void LoadScene(string nameScene)
        {
            CheckBanner();
            _loadScene = false;
            if (nameScene != "")
            {
                _routineScene = LoadAsyncScene(nameScene);
                StartCoroutine(_routineScene);
            }
            PlayAnimationOnce();
        }

        private void PlayAnimationOnce()
        {
            _isLoadingShow = true;
            _routineTitle = PlayTitleLoading();
            StartCoroutine(_routineTitle);
            
            loadingCamera.SetActive(true);
            root.SetActive(true);
            PreLoadIcon();
            var target = Random.Range(0.2f, 0.3f);
            _callbackLoadRoutine = delegate
            {
                var newTarget = Random.Range(0.8f, 0.9f);
                LoadDataWithFill(target, newTarget, delegate
                {
                    NextLoadIcon();
                    LoadToEndWithFill(newTarget, 1f, LoadEventEndWithFill);
                });
            };
            LoadSceneWithFill(target, _callbackLoadRoutine);
        }
        

        #endregion

        /// <summary>
        /// /////////
        /// </summary>

        #region LoadDoubleScene

        public void LoadScene(string scene1, string scene2)
        {
            CheckBanner();
            _loadScene = false;
            _async = new AsyncOperation();
            if (scene1 != "")
            {
                _routineScene = LoadAsyncScene(scene1);
                StartCoroutine(_routineScene);
            }
            PlayAnimationDouble(scene2);
        }

        private void PlayAnimationDouble(string scene2)
        {
            _isLoadingShow = true;
            _routineTitle = PlayTitleLoading();
            StartCoroutine(_routineTitle);
            
            loadingCamera.SetActive(true);
            root.SetActive(true);
            PreLoadIcon();
            var target = Random.Range(0.1f, 0.15f);
            _callbackLoadRoutine = delegate
            {
                var newTarget = Random.Range(0.4f, 0.45f);
                LoadDataWithFill(target, newTarget, delegate
                {
                    target = Random.Range(0.45f, 0.5f);
                    LoadToEndWithFill(newTarget, target, delegate
                    {
                        if (scene2 != "")
                        {
                            _routineScene = LoadAsyncScene(scene2);
                            StartCoroutine(_routineScene);
                        }
                        target = Random.Range(0.55f, 0.6f);
                        _callbackLoadRoutine = delegate
                        {
                            newTarget = Random.Range(0.9f, 0.95f);
                            LoadDataWithFill(target, newTarget, delegate
                            {
                                NextLoadIcon();
                                target = 1f;
                                LoadToEndWithFill(newTarget, target, LoadEventEndWithFill);
                            });
                        };
                        LoadSceneWithFill(target, _callbackLoadRoutine);
                    });
                });
            };
            LoadSceneWithFill(target, _callbackLoadRoutine);
        }

        #endregion
        
        /// <summary>
        /// /////////
        /// </summary>

        #region CallBackEvent

        public void AddCallBackLoadData(Callback callback)
        {
            _listLoadData.Add(callback);
        }

        public void AddCallBackEndLoading(Callback callback)
        {
            _listEndLoad.Add(callback);
        }

        private void CallEventLoadData()
        {
            if (_listLoadData.Count <= 0) 
                return;
            Debug.Log("Load Data Count : " + _listLoadData.Count);
            foreach (var callback in _listLoadData)
            {
                callback?.Invoke();
                Debug.Log("Load Data Step");
            }
            _listLoadData.Clear();
        }

        private void CallEventEndLoading()
        {
            CallEventLoadData();
            if (_listEndLoad.Count <= 0)
                return;
            Debug.Log("Load End Count : " + _listEndLoad.Count);
            foreach (var callback in _listEndLoad)
            {
                callback?.Invoke();
                Debug.Log("Load End Step");
            }
            _listEndLoad.Clear();
        }

        #endregion

        /// <summary>
        /// /////////
        /// </summary>

        #region Display

        private void PreLoadIcon()
        {
            icon.sprite = preIcon;
            lets.SetActive(false);
        }

        private void NextLoadIcon()
        {
            icon.sprite = nextIcon;
            lets.SetActive(true);
        }
        private IEnumerator PlayTitleLoading()
        {
            var current = 0;
            while (_isLoadingShow)
            {
                if (!titleFill.gameObject.activeSelf)
                    titleFill.gameObject.SetActive(true);
                var content = LanguageManager.Instance.Language == Language.Eng ? "LOADING" : "ĐANG TẢI";
                switch (current)
                {
                    case 0:
                        titleFill.text = content + ".";
                        current++;
                        break;
                    case 1:
                        titleFill.text = content + "..";
                        current++;
                        break;
                    case 2:
                        titleFill.text = content + "...";
                        current = 0;
                        break;
                }

                yield return Yield.GetTime(0.2f);
            }
        }

        #endregion
    }
}
