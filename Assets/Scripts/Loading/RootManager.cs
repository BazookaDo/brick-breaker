﻿using System.Collections.Generic;
using Do.Scripts.Tools.Audio;
using Do.Scripts.Tools.Other;
using Home.Level;
using Home.LevelRescue;
using InGame;
using InGame.Data;
using Other;
using Popup;
using Popup.Building;
using UnityEngine;

namespace Loading
{
    public class RootManager : MonoBehaviour
    {
        public static RootManager Instance { get; private set; }
        [SerializeField] private bool openAllLevel;

        private const string KEY_TUTORIAL = "KEY_TUTORIAL";
        private const string KEY_COIN = "KEY_COIN";
        private const string KEY_STAR = "KEY_STAR";
        private const string KEY_BUILDING = "KEY_BUILDING";
        public const string KEY_SKIN_BALL = "KEY_SKIN_BALL_";
        public const string KEY_SKIN_CHARACTER = "KEY_SKIN_CHARACTER_";
        public const string KEY_SKIN_THEME = "KEY_SKIN_THEME_";
        private const string KEY_ITEM_BOOSTER = "KEY_ITEM_BOOSTER_";
        private const string KEY_RATE = "KEY_RATE";
        private const string KEY_REMOVE_ADS = "KEY_REMOVE_ADS";
        private const string KEY_LEVEL_NORMAL = "KEY_LEVEL_NORMAL_";
        private const string KEY_LEVEL_RESCUE = "KEY_LEVEL_RESCUE_";

        private int _tutorial;
        private int _coin, _star;
        private int _building;
        [SerializeField]
        private  List<int> buildings = new List<int>();
        private SkinBall _skinBall;
        private SkinCharacter _skinCharacter;
        private SkinTheme _skinTheme;
        private readonly List<int> items = new List<int>();
        private bool _isRate, _isRemoveAds;
        private readonly List<LevelData> listLevelData = new List<LevelData>();
        private int _currentLevelOpen;
        private bool _isCanNextLevel;
        private readonly List<LevelRescueData> listLevelRescueData = new List<LevelRescueData>();
        [SerializeField]
        private int _currentLevelRescueOpen, _unlockLevelRescue;

        [SerializeField] private BuildingData buildingData;
        [SerializeField] private List<Sprite> skinBalls, skinThemes;
        [SerializeField] private ObstacleData obstacleLevelNormal, obstacleLevelRescue;
        [SerializeField] private DataRescue dataRescue;

        public bool IsTutorial(int level)
        {
            return level == _tutorial;
        }

        public void PassTutorial()
        {
            _tutorial++;
            MyPref.SetInt(KEY_TUTORIAL, _tutorial);
        }
        
        public int Coin
        {
            get => _coin;
            set
            {
                _coin = value;
                MyPref.SetInt(KEY_COIN, _coin);
            }
        }
        
        public int Star
        {
            get => _star;
            set
            {
                _star = value;
                MyPref.SetInt(KEY_STAR, _star);
            }
        }

        public int Building
        {
            get => _building;
            set
            {
                _building = value;
                MyPref.SetInt(KEY_BUILDING, _building);
            }
        }

        public int GetBuildingItem(int item)
        {
            return buildings[item];
        }

        public void SetBuildingItem(int item, int value)
        {
            buildings[item] = value;
            MyPref.SetInt(KEY_BUILDING + item, value);
            if (value >= buildingData.buildingData[item].listItemData.Count &&
                item < buildings.Count - 1)
            {
                buildings[item + 1] = 0;
                MyPref.SetInt(KEY_BUILDING + (item + 1), 0);
            }
        }

        public void OpenBuildingItem(BuildingItemData buildingItemData)
        {
            buildingItemData.state = BuildingState.Open;
            MyPref.SetInt(KEY_BUILDING + _building + buildingItemData.id, (int) buildingItemData.state);
        }

        public SkinBall CurrentSkinBall
        {
            get => _skinBall;
            set
            {
                _skinBall = value;
                MyPref.SetInt(KEY_SKIN_BALL, (int) _skinBall);
            }
        }

        public Sprite GetCurrentSkinBall => skinBalls[(int) _skinBall];

        public Sprite GetSpriteSkinBall(SkinBall skinBall)
        {
            return skinBalls[(int) skinBall];
        }

        public SkinCharacter CurrentSkinCharacter
        {
            get => _skinCharacter;
            set
            {
                _skinCharacter = value;
                MyPref.SetInt(KEY_SKIN_CHARACTER, (int) _skinCharacter);
            }
        }

        public SkinTheme CurrentSkinTheme
        {
            get => _skinTheme;
            set
            {
                _skinTheme = value;
                MyPref.SetInt(KEY_SKIN_THEME, (int) _skinTheme);
            }
        }

        public Sprite GetCurrentSkinTheme => skinThemes[(int) _skinTheme];

        public Sprite GetSpriteSkinTheme(SkinTheme skinTheme)
        {
            return skinThemes[(int) skinTheme];
        }

        public int GetItem(ItemBooster itemBoosterType)
        {
            return items[(int) itemBoosterType];
        }

        public void SetItem(ItemBooster itemBoosterType, int value)
        {
            items[(int) itemBoosterType] = value;
            MyPref.SetInt(KEY_ITEM_BOOSTER + itemBoosterType, value);
        }

        public bool IsRate
        {
            get => _isRate;
            set
            {
                _isRate = value;
                MyPref.SetBool(KEY_RATE, _isRate);
            }
        }

        public bool IsRemoveAds
        {
            get => _isRemoveAds;
            set
            {
                _isRemoveAds = value;
                MyPref.SetBool(KEY_REMOVE_ADS, _isRemoveAds);
            }
        }

        public List<LevelData> ListLevelData => listLevelData;
        private int MaxLevel => listLevelData.Count;
        public int CurrentLevelOpen => _currentLevelOpen;
        public List<LevelRescueData> ListLevelRescueData => listLevelRescueData;
        public int CurrentLevelRescueOpen => _currentLevelRescueOpen;
        private int MaxLevelRescue => listLevelRescueData.Count;
        
        public bool OpenRescueMode { get; set; }
        
        public bool MoreBall { get; set; }

        private void Awake()
        {
            Instance = this;
            if (openAllLevel)
            {
                if (Star <= 0)
                {
                    Coin = 999999;
                    Star = obstacleLevelNormal.obstacleLevels.Count * 3;
                    MyPref.SetInt(KEY_LEVEL_NORMAL, obstacleLevelNormal.obstacleLevels.Count);
                    MyPref.SetInt(KEY_LEVEL_RESCUE, 2);
                    for (var i = 0; i < dataRescue.listLevelRescueData.Count; i++)
                    {
                        MyPref.SetInt(KEY_LEVEL_RESCUE + i, (int) LevelRescueState.Unlock);
                    }
                }
            }
            _tutorial = MyPref.GetInt(KEY_TUTORIAL);
        }

        private void Start()
        {
            _coin = MyPref.GetInt(KEY_COIN);
            _star = MyPref.GetInt(KEY_STAR);

            _building = MyPref.GetInt(KEY_BUILDING);
            buildings.Add(MyPref.GetInt(KEY_BUILDING + 0));
            for (var i = 1; i < buildingData.buildingData.Count; i++)
            {
                buildings.Add(MyPref.GetInt(KEY_BUILDING + i, -1));
            }
            LoadBuildingData();
            
            _skinBall = (SkinBall) MyPref.GetInt(KEY_SKIN_BALL);
            _skinCharacter = (SkinCharacter) MyPref.GetInt(KEY_SKIN_CHARACTER);
            _skinTheme = (SkinTheme) MyPref.GetInt(KEY_SKIN_THEME);
            
            for (var i = 0; i < 4; i++)
            {
                items.Add(MyPref.GetInt(KEY_ITEM_BOOSTER + (ItemBooster) i, 3));
            }
            _isRate = MyPref.GetBool(KEY_RATE);
            _isRemoveAds = MyPref.GetBool(KEY_REMOVE_ADS);
            
            _currentLevelOpen = MyPref.GetInt(KEY_LEVEL_NORMAL);
            _currentLevelRescueOpen = MyPref.GetInt(KEY_LEVEL_RESCUE);
            SetLevelData();
        }

        private void LoadBuildingData()
        {
            var data = buildingData.buildingData;
            for (var i = 0; i < data.Count; i++)
            {
                var building = data[i];
                var itemData = building.listItemData;
                for (var j = 0; j < itemData.Count; j++)
                {
                    var item = itemData[j];
                    item.id = j;
                    item.state = (BuildingState) MyPref.GetInt(KEY_BUILDING + i + item.id);
                }
            }
            UpdateBuildingData();
        }

        public void UpdateBuildingData()
        {
            var data = buildingData.buildingData;
            foreach (var building in data)
            {
                var itemData = building.listItemData;
                foreach (var item in itemData)
                {
                    if (item.state != BuildingState.Lock)
                        continue;
                    if (item.levelLocks.Count <= 0)
                    {
                        item.state = BuildingState.Unlock;
                        continue;
                    }
                    var open = true;
                    foreach (var levelLock in item.levelLocks)
                    {
                        if(itemData[levelLock].state == BuildingState.Open)
                            continue;
                        open = false;
                        break;
                    }

                    if (open)
                    {
                        item.state = BuildingState.Unlock;
                    }
                }
            }
            
        }

        private void SetLevelData()
        {
            var count = obstacleLevelNormal.obstacleLevels.Count;
            for (var i = 0; i < count; i++)
            {
                var levelData = new LevelData
                {
                    level = i,
                    levelState = i < _currentLevelOpen
                        ? LevelState.Pass
                        : i > _currentLevelOpen
                            ? LevelState.Lock
                            : LevelState.Open,
                    star = MyPref.GetInt(KEY_LEVEL_NORMAL + i)
                };
                listLevelData.Add(levelData);
            }

            if (_currentLevelOpen >= MaxLevel)
                _currentLevelOpen = MaxLevel - 1;

            _unlockLevelRescue = -1;
            listLevelRescueData.AddRange(dataRescue.listLevelRescueData);
            count = listLevelRescueData.Count;
            for (var i = 0; i < count; i++)
            {
                var levelRescueData = listLevelRescueData[i];
                levelRescueData.level = i;
                levelRescueData.levelState = (LevelRescueState) MyPref.GetInt(KEY_LEVEL_RESCUE + i);
                if (_unlockLevelRescue < 0 && levelRescueData.levelState == LevelRescueState.Lock)
                    _unlockLevelRescue = i;
            }

            if (_unlockLevelRescue < 0)
                _unlockLevelRescue = MaxLevelRescue;

            if (_currentLevelRescueOpen >= MaxLevelRescue)
                _currentLevelRescueOpen = MaxLevelRescue - 1;
        }

        public List<ObstacleItem> convertLevel;

        public List<ObstacleItem> Get_ObstacleLevel()
        {
            var obstacleLevel = Utils.GameMode == GameMode.Normal
                ? obstacleLevelNormal.obstacleLevels[Utils.CurrentLevel]
                : obstacleLevelRescue.obstacleLevels[Utils.CurrentLevel];
            convertLevel = new List<ObstacleItem>();
            foreach (var obstacleItem in obstacleLevel.obstacleItems)
            {
                if (obstacleItem.turnDelay > 0)
                    continue;
                convertLevel.Add(obstacleItem);
            }
            return convertLevel;
        }

        public int Gift_Normal()
        {
            var gift = Utils.CurrentLevel >= _currentLevelOpen ? 100 : 10;
            return gift;
        }

        public LevelRescueGift Gift_Rescue()
        {
            var gift = Utils.CurrentLevel >= _currentLevelRescueOpen
                ? listLevelRescueData[Utils.CurrentLevel].gift
                : new LevelRescueGift
                {
                    giftType = LevelRescueGiftType.Coin,
                    giftValue = 10
                };
            return gift;
        }

        public void OpenCurrentNormalLevel(int star)
        {
            var compare = listLevelData[Utils.CurrentLevel].star;
            if (compare < star)
            {
                MyPref.SetInt(KEY_LEVEL_NORMAL + Utils.CurrentLevel, star);
                listLevelData[Utils.CurrentLevel].star = star;
                Star += (star - compare);
                PopupManager.Instance.ShowStar();
            }
            _isCanNextLevel = true;
            if (Utils.CurrentLevel < _currentLevelOpen)
            {
                Utils.CurrentLevel++;
            }
            else
            {
                listLevelData[_currentLevelOpen].levelState = LevelState.Pass;
                _currentLevelOpen++;
                UnlockRescueLevel();
                MyPref.SetInt(KEY_LEVEL_NORMAL, _currentLevelOpen);
                if (_currentLevelOpen >= MaxLevel)
                {
                    _currentLevelOpen = MaxLevel - 1;
                    _isCanNextLevel = false;
                }
                else
                {
                    listLevelData[_currentLevelOpen].levelState = LevelState.Open;
                    Utils.CurrentLevel = _currentLevelOpen;
                }
            }
        }

        private void UnlockRescueLevel()
        {
            if (_unlockLevelRescue >= MaxLevelRescue) 
                return;
            var nextLevelRescue = listLevelRescueData[_unlockLevelRescue];
            if (_currentLevelOpen <= nextLevelRescue.targetOpen)
                return;
            nextLevelRescue.levelState = LevelRescueState.Unlock;
            MyPref.SetInt(KEY_LEVEL_RESCUE + _unlockLevelRescue, (int) LevelRescueState.Unlock);
            _unlockLevelRescue++;
        }

        public void OpenCurrentRescueLevel()
        {
            OpenRescueMode = true;
            if (Utils.CurrentLevel < _currentLevelRescueOpen)
            {
                
            }
            else
            {
                listLevelRescueData[_currentLevelRescueOpen].levelState = LevelRescueState.Pass;
                MyPref.SetInt(KEY_LEVEL_RESCUE + _currentLevelRescueOpen, (int) LevelRescueState.Pass);
                if (_currentLevelRescueOpen >= _unlockLevelRescue)
                    return;
                _currentLevelRescueOpen++;
                MyPref.SetInt(KEY_LEVEL_RESCUE, _currentLevelRescueOpen);
                Utils.CurrentLevel = _currentLevelRescueOpen;
            }
        }

        public bool IsCanNext()
        {
            return _isCanNextLevel;
        }

        public void PlayGame()
        {
            if (PopupManager.Instance.IsBuildingShow)
                PopupManager.Instance.Building_UnActive();
            AudioManager.Instance.StopAll();
            LoadingManager.Instance.LoadScene(LoadingManager.SCENE_GAME);
        }

        public void Home()
        {
            AudioManager.Instance.StopAll();
            LoadingManager.Instance.LoadScene(LoadingManager.SCENE_HOME);
        }
    }
}
