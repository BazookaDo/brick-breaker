﻿using System;
using System.Collections.Generic;
using Spine;
using Spine.Unity;
using UnityEngine;

namespace Other
{
    public class SkeletonSkin : MonoBehaviour
    {
        [SerializeField] private Spine type;
        [SpineSkin] [SerializeField] private List<string> skins;
        private Skeleton _skeleton;

        public void SetSkin(int skin)
        {
            if(_skeleton == null)
                _skeleton = type == Spine.Animation ? GetComponent<SkeletonAnimation>().Skeleton : GetComponent<SkeletonGraphic>().Skeleton;
            _skeleton.SetSkin(skins[skin]);
        }
        
        private enum Spine
        {
            Animation,
            Graphic
        }
    }
}
