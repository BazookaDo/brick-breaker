﻿using InGame;
using UnityEngine;

namespace Other
{
    public class Utils : MonoBehaviour
    {
        public static GameMode GameMode { get; set; }
        
        public static int CurrentLevel { get; set; }
    }
}
